package com.guiddoo.smslite.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.guiddoo.smslite.R;

import org.json.JSONObject;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String collapse_key;
    int id=0;
    String Push_Action;

    int notifyID = 1;
    String CHANNEL_ID = "my_channel_01";// The id of the channel.
    int importance = NotificationManager.IMPORTANCE_HIGH;
    String channelName = "Channel Name";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        //collapse_key = remoteMessage.getCollapseKey();
     // id = Integer.parseInt(remoteMessage.getData().get("Identity"));
     // Push_Action = remoteMessage.getData().get("Action");
      //Log.e("Push_Action----------->", Push_Action);

      /*  Intent intent = new Intent();
        intent.putExtra("noti",remoteMessage.getNotification().getTitle());

        intent.setAction("com.ideologi.notification");
        sendBroadcast(intent);*/

        //Calling method to news push notification

        Intent intent = new Intent();

        try{

              Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                Log.e("JSON_OBJECT", object.toString());


            Log.e(TAG, "Firebase Data-------->: " + remoteMessage.getData());

            Log.e(TAG, "SMSLite Notification-------->: " + remoteMessage.getNotification());

            if (remoteMessage.getNotification() != null) {
                Log.e(TAG, "Message Notification Body------->: " + remoteMessage.getNotification().getBody());
            }

        }catch (Exception e){

        }



       /* NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());*/

        showNotificationMessage(this,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),intent);





       /* if (Push_Action.equalsIgnoreCase("BOOK")) {

            intent = new Intent(getBaseContext(), ActivityHomeScreen.class);
            intent.putExtra("id", remoteMessage.getData().get("Identity"));
            intent.putExtra("Flow", "BOOK");
            showNotificationMessage(this,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),intent);
        }else  if (Push_Action.equalsIgnoreCase("ANNOUNCEMENT")){
            intent = new Intent(getBaseContext(), ActivityHomeScreen.class);
            intent.putExtra("id", remoteMessage.getData().get("Identity"));
            intent.putExtra("Flow", "ANNOUNCEMENT");
            showNotificationMessage(this,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),intent);
        }else if (Push_Action.equalsIgnoreCase("RENEWAL")){
            intent = new Intent(getBaseContext(), ActivityHomeScreen.class);
            intent.putExtra("id", remoteMessage.getData().get("Identity"));
            intent.putExtra("Flow", "RENEWAL");
            showNotificationMessage(this,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),intent);
        }else{
            showNotificationMessage(this,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),intent);
        }*/

    }

    private void showNotificationMessage(Context context, String title, String message,Intent intent) {

        try{
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            int notificationId = 1;
            String channelId = "channel-01";
            String channelName = "SMSLite";
            int importance = NotificationManager.IMPORTANCE_HIGH;



            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
                assert notificationManager != null;
                notificationManager.createNotificationChannel(mChannel);
            }

            // PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    //.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                    //.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                    .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);

            notificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
            notificationManager.cancel(id);
        }catch (Exception e){

        }




    }

}