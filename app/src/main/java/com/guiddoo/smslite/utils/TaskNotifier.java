package com.guiddoo.smslite.utils;


public interface TaskNotifier {
    public  void onSuccess(String message);
    public void onError(String message);
}
