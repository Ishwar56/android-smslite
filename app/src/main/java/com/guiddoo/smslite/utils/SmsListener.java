package com.guiddoo.smslite.utils;

/**
 * Created by ishwar on 19/9/17.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
