package com.guiddoo.smslite.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.guiddoo.smslite.R;


public class CustomResponseDialog {
    private Context mContext;
    private Dialog dialog;
    private ProgressBar progressBar;
    private static String TAG = "LogCustomResponseDialog";

    public CustomResponseDialog(Context mContext) {
        this.mContext = mContext;
        dialog = new Dialog(mContext);
    }


    public void showCustomDialog(/*String message*/){
        try{
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_dialog);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            //DottedProgressBar progressBar = (DottedProgressBar) dialog.findViewById(R.id.progress);
//        TextView txt_message = (TextView) dialog.findViewById(R.id.message);
//        txt_message.setText(message);
           // progressBar.startProgress();
            progressBar = dialog.findViewById(R.id.progress_circular);
            int colorCode = Color.parseColor("#D97D54");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progressBar.setIndeterminateTintList(ColorStateList.valueOf(colorCode));
            }
            dialog.getWindow().setAttributes(lp);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

        }catch (Exception e){

        }

    }

    public void hideCustomeDialog(){
        try{
            dialog.cancel();
        }catch (Exception e){

        }

    }

}
