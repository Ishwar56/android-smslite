package com.guiddoo.smslite.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GPSTracker extends Service implements LocationListener {

    private final Context mContext;
    private Activity activity;

    // flag for GPS status
    boolean isGPSEnabled = false;



    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // pin_add_day
    double latitude; // latitude
    double longitude; // longitude

    // The minimum sorting_dialog_distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private static final int PERMISSION_REQUEST_CODE = 1;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 5 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context, Activity activity) {
        this.mContext = context;
        this.activity = activity;
        getLocation();
    }

    public Location getLocation() {
        try {

            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (!isGPSEnabled && !isNetworkEnabled) {

                //showSettingsAlert(this.activity);

            } else {
                this.canGetLocation = true;

                if (isNetworkEnabled) {

                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) mContext,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);

                        return location;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return location;

    }

    public String getCustomeAddress(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        String ret = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null) {

                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        sb.append(address.getAddressLine(i)).append(",");
                    }
                    sb.append(address.getAddressLine(0)).append(",");

                    ret = sb.toString();
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ret;
    }

    public String getCityName(){
        String cityName = new String();
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        try
        {

            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
               /* for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                sb.append(address.getAddressLine(0)).append(",");
                sb.append(address.getSubLocality()).append(",");
                sb.append(address.getLocality()).append(",");
                sb.append(address.getPostalCode()).append(",");
                sb.append(address.getAdminArea()).append(",");
                sb.append(address.getCountryName());*/
                sb.append(address.getLocality());
                cityName = sb.toString();
            }


        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return cityName;
    }

    public String getState(){
        String State = new String();
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        try
        {

            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
               /* for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                sb.append(address.getAddressLine(0)).append(",");
                sb.append(address.getSubLocality()).append(",");
                sb.append(address.getLocality()).append(",");
                sb.append(address.getPostalCode()).append(",");
                sb.append(address.getAdminArea()).append(",");
                sb.append(address.getCountryName());*/
                sb.append(address.getAdminArea());
                State = sb.toString();
            }


        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return State;
    }

    public String getCountyName(){
        String CountyName = new String();
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        try
        {

            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
               /* for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                sb.append(address.getAddressLine(0)).append(",");
                sb.append(address.getSubLocality()).append(",");
                sb.append(address.getLocality()).append(",");
                sb.append(address.getPostalCode()).append(",");
                sb.append(address.getAdminArea()).append(",");
                sb.append(address.getCountryName());*/
                sb.append(address.getCountryName());
                CountyName = sb.toString();
            }


        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return CountyName;
    }

    public String getPostalCode(){
        String City_postal_Code = new String();
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        try
        {

            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
               /* for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                sb.append(address.getAddressLine(0)).append(",");
                sb.append(address.getSubLocality()).append(",");
                sb.append(address.getLocality()).append(",");
                sb.append(address.getPostalCode()).append(",");
                sb.append(address.getAdminArea()).append(",");
                sb.append(address.getCountryName());*/
                sb.append(address.getPostalCode());
                City_postal_Code = sb.toString();
            }


        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return City_postal_Code;
    }

    public String getFullAddress(){
        String FullAddress = new String();
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        try
        {

            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                sb.append(address.getAddressLine(0)).append(",");
                /*sb.append(address.getSubLocality()).append(",");
                sb.append(address.getLocality()).append(",");
                sb.append(address.getPostalCode()).append(",");
                sb.append(address.getAdminArea()).append(",");
                sb.append(address.getCountryName());*/
                FullAddress = sb.toString();
            }


        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return FullAddress;
    }


    /*Geocoder geocoder = new Geocoder(this, Locale.getDefault());
 List<Address> addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
 String cityName = addresses.get(0).getAddressLine(0);
 String stateName = addresses.get(0).getAddressLine(1);
 String countryName = addresses.get(0).getAddressLine(2);*/

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
            //latitude = 41.0082;//25.2587;
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
            //longitude = 28.9784;//55.3264;
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public  boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show activity_profile_settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */



    public static void  showSettingsAlert(final Activity context){

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(context, 121);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
       /* AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);


        alertDialog.setTitle(R.string.gps_title);
        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage(R.string.gps_text);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
                //context.finish();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();*/
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}