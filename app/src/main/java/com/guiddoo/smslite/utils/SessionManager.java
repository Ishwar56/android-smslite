package com.guiddoo.smslite.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


/**
 * Created by Ishwar on 10-02-2019.
 */

public class SessionManager {

    private static final String PREF_NAME = "Wayrtoo";
    private static final String LOGIN = "isLogin";
    private static final String SWITCH = "isswitch";
    private static final String USER_ID = "isUserIds";
    private static final String AGENT_CODE = "isAgentId";
    private static final String USER_NAME = "isUserName";
    private static final String USER_CITY = "isUserCity";
    private static final String USER_MOBILE = "isUserMobile";
    private static final String USER_EMAIL = "isUserEmail";
    private static final String USER_PHOTO = "isUserPhotos";
    private static final String USER_DEVICE_ID = "isUserDevice";
    private static final String BACK = "isBack";
    private static final String BASE_URL = "isBaseUrl";
    private static final String HASHKEY = "isHashKey";
    private static final String ADDRESS = "isAddtressKey";
    private static final String LATITUDE = "islatKey";
    private static final String LOGITUDE = "islogiKey";
    private static final String LANGUAGE = "islanguage";

    private static final String SORTING_ID = "isSorting";


    //  Book Detail

    private static final String BOOK_ID = "isBook_id";
    private static final String COLLECTION_ID = "isCollection_id";
    private static final String FROM_COLLECTION = "isFrom_collection";
    private static final String FROM_READING = "isFrom_reading";

    private static String TAG = SessionManager.class.getSimpleName();

    Context _context;
    int PRIVATE_MODE = 0;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(LOGIN, isLoggedIn);
        editor.commit();
    }

    public void setBack(boolean isLoggedIn) {
        editor.putBoolean(BACK, isLoggedIn);
        editor.commit();
    }

    public void setPrivacy(boolean isLoggedIn) {
        editor.putBoolean(SWITCH, isLoggedIn);
        editor.commit();
    }

    public boolean setBaseUrl(String key) {
        editor.putString(BASE_URL, key);
        if (editor.commit()) {
            Log.d(TAG, "Authorization key is set!");
            return true;
        }
        return false;
    }

    public boolean setHashKey(String key) {
        editor.putString(HASHKEY, key);
        if (editor.commit()) {
            Log.d(TAG, "HashKey is set!");
            return true;
        }
        return false;
    }

    public boolean setpickUpAddress(String key) {
        editor.putString(ADDRESS, key);
        if (editor.commit()) {
            Log.d(TAG, "HashKey is set!");
            return true;
        }
        return false;
    }
    public boolean setlatitude(String key) {
        editor.putString(LATITUDE, key);
        if (editor.commit()) {
            Log.d(TAG, "HashKey is set!");
            return true;
        }
        return false;
    }
    public boolean setlogitude(String key) {
        editor.putString(LOGITUDE, key);
        if (editor.commit()) {
            Log.d(TAG, "HashKey is set!");
            return true;
        }
        return false;
    }

    public boolean setlanguage(String key) {
        editor.putString(LANGUAGE, key);
        if (editor.commit()) {
            Log.d(TAG, "HashKey is set!");
            return true;
        }
        return false;
    }


    public boolean setUserID(String key) {
        editor.putString(USER_ID, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setAgentCode(String key) {
        editor.putString(AGENT_CODE, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setUserName(String key) {
        editor.putString(USER_NAME, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setUserCity(String key) {
        editor.putString(USER_CITY, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setUserMobile(String key) {
        editor.putString(USER_MOBILE, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setUserEmail(String key) {
        editor.putString(USER_EMAIL, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setUserPhoto(String key) {
        editor.putString(USER_PHOTO, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setUserDeviceID(String key) {
        editor.putString(USER_DEVICE_ID, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setSorting_Position(String key) {
        editor.putString(SORTING_ID, key);
        if (editor.commit()) {
            Log.d(TAG, "SORTING_ID  is set!");
            return true;
        }
        return false;
    }



    ////////////////////////////////////////////////////////////////////////////////////////

    public String getSortingPosition() {
        return pref.getString(SORTING_ID, "");
    }

    public boolean setBookId(String key) {
        editor.putString(BOOK_ID, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public boolean setCollection_ID(String key) {
        editor.putString(COLLECTION_ID, key);
        if (editor.commit()) {
            return true;
        }
        return false;
    }

    public void setFromCollection(boolean isLoggedIn) {
        editor.putBoolean(FROM_COLLECTION, isLoggedIn);
        editor.commit();
    }

    public void setFromReading(boolean isLoggedIn) {
        editor.putBoolean(FROM_READING, isLoggedIn);
        editor.commit();
    }



    public String getBaseUrl() {
        return pref.getString(BASE_URL, null);
    }




    public boolean getLogin() {
        return pref.getBoolean(LOGIN, false);
    }

    public boolean getBack() {
        return pref.getBoolean(BACK, false);
    }

    public boolean getPrivacy() {
        return pref.getBoolean(SWITCH, false);
    }

    public String getUserId() {
        return pref.getString(USER_ID, "");
    }

    public String getAgentCode() {
        return pref.getString(AGENT_CODE, "");
    }

    public String getUserName() {
        return pref.getString(USER_NAME, "Prashant Choudhary");
    }

    public String getUserCity() {
        return pref.getString(USER_CITY, "");
    }

    public String getUserMobile() {
        return pref.getString(USER_MOBILE, "9096168283");
    }

    public String getUserEmail() {
        return pref.getString(USER_EMAIL, "prashant.choudhary@guiddoo.com");
    }

    public String getUserPhoto() {
        return pref.getString(USER_PHOTO, "");
    }


    public String getUserDeviceId() {
        return pref.getString(USER_DEVICE_ID, "");
    }

    public String getBookId() {
        return pref.getString(BOOK_ID, "0");
    }

    public String getCollectionId() {
        return pref.getString(COLLECTION_ID, "0");
    }

    public boolean getFromCollection() {
        return pref.getBoolean(FROM_COLLECTION, false);
    }

    public boolean getFromReading() {
        return pref.getBoolean(FROM_READING, false);
    }

    public String getHashkey() {
        return pref.getString(HASHKEY, "");
    }

    public String getPickupAddress() {
        return pref.getString(ADDRESS, "");
    }
    public String getLatitude() {
        return pref.getString(LATITUDE, "");
    }
    public String getLogitude() {
        return pref.getString(LOGITUDE, "");
    }

    public String getLanguage() {
        return pref.getString(LANGUAGE, "");
    }


}
