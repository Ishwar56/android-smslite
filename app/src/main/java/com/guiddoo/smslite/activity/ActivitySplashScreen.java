package com.guiddoo.smslite.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.services.MyFirebaseInstanceIDService;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.FontsOverride;
import com.guiddoo.smslite.utils.GPSTracker;
import com.guiddoo.smslite.utils.LocaleHelper;
import com.guiddoo.smslite.utils.SessionManager;

import io.fabric.sdk.android.Fabric;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import butterknife.ButterKnife;

public class ActivitySplashScreen extends AppCompatActivity {

    private ProgressBar progressBar;
    private static int SPLASH_TIME_OUT = 1000;
    private Context mContext;
    private View v;
    private SessionManager sessionManager;
    private GPSTracker gpsTracker;
    private LocationManager locationManager;
    private boolean FlagPerFlow = false,FlagPermissionFlow = true;
    private String[] AppPermissions = {Manifest.permission.ACCESS_FINE_LOCATION};
    private String Firebase_deviceId,deviceId,responce,resultObj,message,status_code;
    private CustomResponseDialog customResponseDialog;
    private SQLiteHandler db;
    private FirebaseAnalytics mFirebaseAnalytics;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        customResponseDialog = new CustomResponseDialog(mContext);
        gpsTracker = new GPSTracker(mContext,this);
        db=new SQLiteHandler(mContext);
        v = findViewById(R.id.view);
        progressBar = findViewById(R.id.progressBar);
        int colorCode = Color.parseColor("#FFFFFF");
        progressBar.setIndeterminateTintList(ColorStateList.valueOf(colorCode));

        FontsOverride.setDefaultFont(mContext, "SERIF", "font/open_sans_regular.ttf");
        //sessionManager.setBaseUrl("https://www.guiddooworld.com/smsliteservice/Service1.svc/");
        sessionManager.setBaseUrl("https://smsserviceprod.guiddooworld.com/Service1.svc/");
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        try{
            deviceId = Settings.Secure.getString(mContext.getContentResolver(),Settings.Secure.ANDROID_ID);

            Log.e("deviceId--->",deviceId);

            Firebase_deviceId = FirebaseInstanceId.getInstance().getToken();
            Log.e("Firebase TokenID",  Firebase_deviceId);

            Intent intent1 = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent1);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

            Fabric.with(this, new Crashlytics());

        }catch (Exception e){

        }

       /* if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            gpsTracker.showSettingsAlert(ActivitySplashScreen.this);

        } else {
            if (checkAppPermissions(mContext, AppPermissions)) {
                LoadPages();
            } else if(FlagPermissionFlow){
                ActivityCompat.requestPermissions(this, AppPermissions, 102);
            }
        }*/




    }

    private void LoadPages(){
       // progressBar.setVisibility(View.GONE);

        Log.e("Country name-->",gpsTracker.getCountyName());
        if(gpsTracker.getCountyName().equalsIgnoreCase("China")){
            if(sessionManager.getLanguage().equalsIgnoreCase("")|| sessionManager.getLanguage().equalsIgnoreCase("Cn")){
                sessionManager.setlanguage("Cn");
                LocaleHelper.persist(mContext,"zh");
                setLocale(LocaleHelper.getLanguage(mContext));
            }else{
                sessionManager.setlanguage("En");
                LocaleHelper.persist(mContext,"en");
                setLocale(LocaleHelper.getLanguage(mContext));
            }

        }else{
            if(sessionManager.getLanguage().equalsIgnoreCase("Cn")){
                sessionManager.setlanguage("Cn");
                LocaleHelper.persist(mContext,"zh");
                setLocale(LocaleHelper.getLanguage(mContext));
            }else{
                sessionManager.setlanguage("En");
                LocaleHelper.persist(mContext,"en");
                setLocale(LocaleHelper.getLanguage(mContext));
            }

        }

        Log.e("Country name-->",gpsTracker.getCountyName());

    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        if(sessionManager.getLogin()){
            Intent mainIntent = new Intent(ActivitySplashScreen.this, ActivityHomeScreen.class);
            startActivity(mainIntent);
            finish();
        }else{
            Intent mainIntent = new Intent(ActivitySplashScreen.this, ActivityOTPVerified.class);
            mainIntent.putExtra("Flag", "false");
            startActivity(mainIntent);
            finish();
        }
    }


    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(FlagPermissionFlow){
            if (requestCode == 102 && FlagPermissionFlow) {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FlagPermissionFlow=false;
                    Log.e("Allow------>","0000000");
                    /*if(sessionManager.getLogin()){
                        getMasterRequest();
                    }else{
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                LoadPages();
                            }
                        }, SPLASH_TIME_OUT);

                    }*/
                    LoadPages();
                }else{
                    Log.e("Denai------>","111111");

                }
            }

        }

    }

    public void getMasterRequest() {
        customResponseDialog.showCustomDialog();


        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "users/getmasterdata?hash_key="+sessionManager.getHashkey();
        Log.e("getmasterdata URL--->",url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("getmasterdata Respo--->",jsonString);
                            customResponseDialog.hideCustomeDialog();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("request_status");
                                message = status.getString("message");
                                status_code=status.getString("status_code");
                                if(status.getString("status_code").equals("200")){

                                    db.deleteAllCountries();
                                    db.deleteAllSalesAgents();
                                    db.deleteAllStatus();
                                    db.deleteAllSuppliers();

                                    JSONArray countriesArray = object.getJSONArray("countries");
                                    for(int i=0;countriesArray.length()>i;i++){
                                        JSONObject object1 = countriesArray.getJSONObject(i);
                                        db.setCountriesData(object1.getString("country_id"),object1.getString("country_name"),object1.getString("destinations"),object1.getString("executives"));
                                    }

                                    JSONArray sales_agentsArray = object.getJSONArray("sales_agents");
                                    for(int i=0;sales_agentsArray.length()>i;i++){
                                        JSONObject object1 = sales_agentsArray.getJSONObject(i);
                                        db.setSalesAgentsData(object1.getString("user_code"),object1.getString("user_id"),object1.getString("user_name"));

                                    }

                                    JSONArray statusesArray = object.getJSONArray("statuses");
                                    for(int i=0;statusesArray.length()>i;i++){
                                        JSONObject object1 = statusesArray.getJSONObject(i);
                                        db.setStatusData(object1.getString("status"),object1.getString("detailed_statuses"));
                                    }

                                    JSONArray suppliersArray = object.getJSONArray("suppliers");
                                    for(int i=0;suppliersArray.length()>i;i++){
                                        JSONObject object1 = suppliersArray.getJSONObject(i);
                                        db.setsuppliersData(object1.getString("supplier_id"),object1.getString("supplier_name"));
                                    }

                                }else{
                                    showSnackBar(ActivitySplashScreen.this, message);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }catch (Exception e){
                            showSnackBar(ActivitySplashScreen.this, "Something went wrong.");
                        }

                        if(status_code.equalsIgnoreCase("200")){
                            LoadPages();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(ActivitySplashScreen.this, "Something went wrong.");
                    }
                }
        );

        queue.add(getRequest);


    }

    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }


    @Override
    protected void onResume() {
        super.onResume();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            gpsTracker.showSettingsAlert(ActivitySplashScreen.this);

        } else {
            if (checkAppPermissions(mContext, AppPermissions)) {
                Log.e("Allow------>","0000000");
               /* if(sessionManager.getLogin()){
                    getMasterRequest();
                }else{
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            LoadPages();
                        }
                    }, SPLASH_TIME_OUT);

                }*/
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LoadPages();
                    }
                }, SPLASH_TIME_OUT);

            } else if(FlagPermissionFlow){
                ActivityCompat.requestPermissions(this, AppPermissions, 102);
            }
        }

    }

}
