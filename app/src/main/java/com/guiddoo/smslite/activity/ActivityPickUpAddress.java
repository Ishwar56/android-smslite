package com.guiddoo.smslite.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreabaccega.widget.FormEditText;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.fragment.FragmentAddTravelAgent;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.GPSTracker;
import com.guiddoo.smslite.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPickUpAddress extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.fetAddress)
    FormEditText fetAddress;
    @BindView(R.id.btn_save_address)
    Button btn_save_address;

    @BindView(R.id.btn_check)
    Button btn_check;

    private Context mContext;
    private String responce, resultObj;
    private MapView mapView;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;
    SupportMapFragment mMapView;
    public AlertDialog alertMantra;
    private SessionManager sessionManager;
    Uri imageUri;
    private Bitmap bitmap;
    private static final int PICK_IMAGE = 1;
    private static final int PICK_Camera_IMAGE = 2;
    private boolean flagCammera = false, flagGallery = false;
    private String ORDER_ID;
    private double latitude, longitude,latitudes, longitudes;
    private MarkerOptions markerOptions;
    private IconGenerator iconFactory;
    private GPSTracker gpsTracker;
    private GoogleMap googleMap;
    private LatLng sydney, sydney1;
    private String[] AppPermissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private String Selected_Plan_ID = "1", Base64Image = "", Sfilename = "", Card_Number = "", Coupon_Number = "", Sstate = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up_address);
        mContext = this;
        ButterKnife.bind(this);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        gpsTracker = new GPSTracker(mContext, this);
        gpsTracker.getLocation();

        tv_title.setText("Pick up address");

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_save_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setpickUpAddress(fetAddress.getText().toString());
                onBackPressed();
            }
        });

        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setlatitude(String.valueOf(latitudes));
                sessionManager.setlogitude(String.valueOf(longitudes));
                fetAddress.setText(gpsTracker.getCustomeAddress(latitudes,longitudes));
                //sessionManager.setpickUpAddress(fetAddress.getText().toString());
            }
        });

        mMapView = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        if (mMapView != null) {
            LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null ) {
                mMapView.getMapAsync(this);
                //after this you do whatever you want with the map
            } else{
                Log.e("Eroro inside ","---");
            }
        }else{
            Log.e("Eroro","---");
        }

    }

    @Override
    public void onMapReady(GoogleMap googlemap) {
        try {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            //mMapView.onCreate();
            googleMap = googlemap;
            mMapView.onResume(); // needed to get the toolbar_layout_map to display immediately
            //final LatLng currentPos = new LatLng(latitude, longitude);
            final LatLng currentPos = new LatLng(latitude, longitude);

            googleMap.clear();
            iconFactory = new IconGenerator(this);
            sydney = new LatLng(latitude, longitude);
            //String add = getAddress(latitude, longitude);
            iconFactory.setStyle(IconGenerator.STYLE_GREEN);
            markerOptions = new MarkerOptions().title("My Location").icon(bitmapDescriptorFromVector(this, R.drawable.client_location)).position(sydney).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPos).zoom(16).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.addMarker(markerOptions);


           /* googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                   // Log.e("centerLat---->", String.valueOf(cameraPosition.target.latitude));
                   // Log.e("centerLong---->", String.valueOf(cameraPosition.target.longitude));
                    latitudes=cameraPosition.target.latitude;
                    longitudes=cameraPosition.target.longitude;

                   *//* sessionManager.setlatitude(String.valueOf(cameraPosition.target.latitude));
                    sessionManager.setlogitude(String.valueOf(cameraPosition.target.longitude));
                    fetAddress.setText(gpsTracker.getCustomeAddress(cameraPosition.target.latitude,cameraPosition.target.longitude));*//*

                }
            });*/

            googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {


                    double lat =  googleMap.getCameraPosition().target.latitude;
                    double lng =  googleMap.getCameraPosition().target.longitude;
                    Log.e("centerLat---->", " --------End-----");
                    Log.e("centerLong---->", " -------End---------");

                    sessionManager.setlatitude(String.valueOf(lat));
                    sessionManager.setlogitude(String.valueOf(lng));
                    fetAddress.setText(gpsTracker.getCustomeAddress(lat,lng));


                }
            });



        } catch (NullPointerException e) {
        }
    }
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            mMapView.onPause();
        } catch (Exception e) {
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
