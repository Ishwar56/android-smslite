package com.guiddoo.smslite.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.ui.IconGenerator;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.utils.GPSTracker;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityViewSingleMapDirection extends AppCompatActivity implements LocationListener, OnMapReadyCallback {

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;
    SupportMapFragment mMapView;
    private Context mContext;
    private double latitude, longitude;
    private MarkerOptions markerOptions;
    private IconGenerator iconFactory;
    private GPSTracker gpsTracker;
    private GoogleMap googleMap;
    private LatLng sydney, sydney1;
    private SessionManager sessionManager;
    private String Category_ID;

    private boolean flag=true;
    private ArrayList<LatLng> points; //added
    Polyline line; //added
    private String Lat="0.0",Longi="0.0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_map);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        gpsTracker = new GPSTracker(mContext, ActivityViewSingleMapDirection.this);
        gpsTracker.getLocation();
        Bundle b = getIntent().getExtras();

        tv_title.setText("Map Details");
        points = new ArrayList<LatLng>(); //added

        try {
            Lat = getIntent().getStringExtra("Lat");
            Longi = getIntent().getStringExtra("Log");
        }catch (NullPointerException e){

        }


        mMapView = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mMapView.getMapAsync(this);

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googlemap) {
        try {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            //mMapView.onCreate();
            googleMap = googlemap;
            mMapView.onResume(); // needed to get the toolbar_layout_map to display immediately
            //final LatLng currentPos = new LatLng(latitude, longitude);
            final LatLng currentPos = new LatLng(Double.valueOf(Lat), Double.valueOf(Longi));

            googleMap.clear();
            iconFactory = new IconGenerator(ActivityViewSingleMapDirection.this);
            sydney = new LatLng(latitude, longitude);
            String add = getAddress(latitude, longitude);
            iconFactory.setStyle(IconGenerator.STYLE_GREEN);
            markerOptions = new MarkerOptions().title("My Location").icon(bitmapDescriptorFromVector(ActivityViewSingleMapDirection.this, R.drawable.my_location)).position(sydney).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
            googleMap.addMarker(markerOptions);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPos).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            sydney1 = new LatLng(Double.valueOf(Lat), Double.valueOf(Longi));
            markerOptions = new MarkerOptions().position(sydney1);
            markerOptions.icon(bitmapDescriptorFromVector(ActivityViewSingleMapDirection.this, R.drawable.client_location));
            googleMap.addMarker(markerOptions);

            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                @Override
                public void onInfoWindowClick(Marker arg0) {


                }
            });


        } catch (Exception e) {
        }
    }



    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(ActivityViewSingleMapDirection.this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                // result.append(address.getAddressLine(1)).append("\n");
                result.append(address.getLocality() + " " + address.getCountryName());
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mMapView.onResume();
        } catch (Exception e) {

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mMapView.onPause();
        } catch (Exception e) {
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }



}
