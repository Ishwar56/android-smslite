package com.guiddoo.smslite.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.services.MyFirebaseInstanceIDService;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.FontsOverride;
import com.guiddoo.smslite.utils.GPSTracker;
import com.guiddoo.smslite.utils.SMSReciver;
import com.guiddoo.smslite.utils.SessionManager;
import com.guiddoo.smslite.utils.SmsListener;
import com.guiddoo.smslite.utils.TaskNotifier;
import com.hbb20.CountryCodePicker;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class ActivityOTPVerified extends AppCompatActivity implements View.OnClickListener, TaskNotifier {

    @BindView(R.id.ll_send_OTP)
    LinearLayout ll_send_OTP;

    @BindView(R.id.ll_verify_OTP)
    LinearLayout ll_verify_OTP;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.fetMobile)
    FormEditText fetMobile;

    @BindView(R.id.btn_send_OTP)
    Button btn_send_OTP;

    @BindView(R.id.otp_mobile)
    TextView otp_mobile;

    @BindView(R.id.tv_edit_mobile)
    TextView tv_edit_mobile;

    @BindView(R.id.et_first)
    EditText et_first;

    @BindView(R.id.et_second)
    EditText et_second;

    @BindView(R.id.et_third)
    EditText et_third;

    @BindView(R.id.et_four)
    EditText et_four;

    @BindView(R.id.et_five)
    EditText et_five;

    @BindView(R.id.et_six)
    EditText et_six;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.tv_resend_OTP)
    TextView tv_resend_OTP;

    @BindView(R.id.tv_timer)
    TextView tv_timer;

    @BindView(R.id.tv_skip)
    TextView tv_skip;

    @BindView(R.id.iv_clear)
    ImageView iv_clear;

    @BindView(R.id.btn_verify_OTP)
    Button btn_verify_OTP;
    private GPSTracker gpsTracker;
    private SQLiteHandler db;
    private boolean OTP=false,FlagPermissionFlow = true,FlagLoginOTP=false;
    private String Firebase_deviceId,OTPValue="",deviceId="",message,status_code;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private Context mContext;
    private String[] AppPermissions = {Manifest.permission.RECEIVE_SMS,Manifest.permission.READ_SMS};
    private int Count=1;
    private CustomResponseDialog customResponseDialog;
    private SessionManager sessionManager;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverified);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        db=new SQLiteHandler(mContext);
        customResponseDialog = new CustomResponseDialog(mContext);
        gpsTracker = new GPSTracker(mContext, ActivityOTPVerified.this);
        setUI();
        tv_title.setText(getString(R.string.login));


        FontsOverride.setDefaultFont(mContext, "SERIF", "font/open_sans_regular.ttf");

        FlagLoginOTP=false;

        if (checkAppPermissions(mContext, AppPermissions)) {

        } else if(FlagPermissionFlow){
            ActivityCompat.requestPermissions(this, AppPermissions, 102);
        }

        et_first.addTextChangedListener(new GenericTextWatcher(et_first));
        et_second.addTextChangedListener(new GenericTextWatcher(et_second));
        et_third.addTextChangedListener(new GenericTextWatcher(et_third));
        et_four.addTextChangedListener(new GenericTextWatcher(et_four));
        et_five.addTextChangedListener(new GenericTextWatcher(et_five));
        et_six.addTextChangedListener(new GenericTextWatcher(et_six));

        et_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setVisibility(View.GONE);
            }
        });


       /* tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ActivityOTPVerified.this,ActivityRegistration.class);
                startActivity(intent);

            }
        });*/

        iv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_first.setText("");
                et_second.setText("");
                et_third.setText("");
                et_four.setText("");
                et_five.setText("");
                et_six.setText("");
                et_six.clearFocus();
                et_first.requestFocus();
            }
        });

    }

    /*private void getMobile(){
        fetMobile.setText(sessionManager.getUserMobileNo());

        if(!TextUtils.isEmpty(fetMobile.getText().toString())) {

        } else {

        }
    }*/

    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(FlagPermissionFlow){
            if (requestCode == 102 && FlagPermissionFlow && Count <=5) {
                if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                    FlagPermissionFlow=false;
                }else{
                    Count++;
                    ActivityCompat.requestPermissions(this, AppPermissions, 102);
                }
            }
        }
    }

    @Override
    public void onSuccess(String message) {

    }

    @Override
    public void onError(String message) {

    }


    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.et_first:
                    if(text.length()==1)
                        et_first.clearFocus();
                        et_second.requestFocus();
                    et_first.setSelection(text.length());
                    et_first.setCursorVisible(false);
                    break;
                case R.id.et_second:

                    Log.e("et_second length", String.valueOf(text.length()));
                    if(text.length()==1){
                        et_second.clearFocus();
                        et_third.requestFocus();
                        et_second.setSelection(text.length());
                        et_second.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_second.clearFocus();
                        et_first.requestFocus();
                        et_first.setCursorVisible(true);
                    }

                    break;
                case R.id.et_third:
                    Log.e("et_second et_third", String.valueOf(text.length()));
                    if(text.length()==1){
                        et_third.clearFocus();
                        et_four.requestFocus();
                        et_third.setSelection(text.length());
                        et_third.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_third.clearFocus();
                        et_second.requestFocus();
                        et_second.setCursorVisible(true);
                    }
                    break;
                case R.id.et_four:
                    if(text.length()==1){
                        et_four.clearFocus();
                        et_five.requestFocus();
                        et_four.setSelection(text.length());
                        et_four.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_four.clearFocus();
                        et_third.requestFocus();
                        et_third.setCursorVisible(true);
                    }
                    break;
                case R.id.et_five:
                    if(text.length()==1){
                        et_five.clearFocus();
                        et_six.requestFocus();
                        et_five.setSelection(text.length());
                        et_five.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_five.clearFocus();
                        et_four.requestFocus();
                        et_four.setCursorVisible(true);
                    }
                    break;
                case R.id.et_six:
                    if(text.length()==1){
                        progress.setVisibility(View.GONE);
                        btn_verify_OTP.setVisibility(View.VISIBLE);
                        et_six.setSelection(text.length());
                        et_six.clearFocus();
                    }else if(text.length()==0){
                        et_six.clearFocus();
                        et_five.requestFocus();
                        et_five.setCursorVisible(true);
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try{
            deviceId = Settings.Secure.getString(mContext.getContentResolver(),Settings.Secure.ANDROID_ID);

            Log.e("deviceId--->",deviceId);

            Firebase_deviceId = FirebaseInstanceId.getInstance().getToken();
            Log.e("Firebase TokenID",  Firebase_deviceId);

            Intent intent1 = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent1);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

            Fabric.with(this, new Crashlytics());

            Firebase_deviceId = FirebaseInstanceId.getInstance().getToken();
            Log.e("Firebase TokenID",  Firebase_deviceId);

        }catch (Exception e){

        }


        /*SMSReciver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.e("Text","--------------sms---------> "+messageText);
                // fetOTP.setText(messageText.replaceAll("[^0-9]", ""));`       `````
                try{
                    String OTP = messageText.replaceAll("[^0-9]", "");

                    String first = Character.toString(OTP.charAt(0));
                    String second = Character.toString(OTP.charAt(1));
                    String third = Character.toString(OTP.charAt(2));
                    String four = Character.toString(OTP.charAt(3));
                    String five = Character.toString(OTP.charAt(4));
                    String six = Character.toString(OTP.charAt(5));
                    et_first.setText(first);
                    et_first.setSelection(et_first.getText().toString().length());
                    et_second.setText(second);
                    et_second.setSelection(et_second.getText().toString().length());
                    et_third.setText(third);
                    et_third.setSelection(et_third.getText().toString().length());
                    et_four.setText(four);
                    et_four.setSelection(et_four.getText().toString().length());
                    et_five.setText(five);
                    et_five.setSelection(et_five.getText().toString().length());
                    et_six.setText(six);
                    et_six.setSelection(et_six.getText().toString().length());
                    progress.setVisibility(View.GONE);
                    btn_verify_OTP.setVisibility(View.VISIBLE);

                    if(et_first.getText().length()==1 && et_second.getText().length()==1 && et_third.getText().length()==1 && et_four.getText().length()==1 && et_five.getText().length()==1 && et_six.getText().length()==1){
                        String EnterOTPValue = et_first.getText().toString()+et_second.getText().toString()+et_third.getText().toString()+et_four.getText().toString()+et_five.getText().toString()+et_six.getText().toString();

                        if(OTPValue.equalsIgnoreCase(EnterOTPValue.replace(" ",""))){
                            if (gpsTracker.isNetworkAvailable()) {
                                getMasterRequest();
                            } else {
                                showSnackBar(ActivityOTPVerified.this, "Please check your internet connection .");
                            }
                        }else{
                            showSnackBar(ActivityOTPVerified.this, getString(R.string.invalid_otp));
                        }
                    }else{
                        showSnackBar(ActivityOTPVerified.this, getString(R.string.enter_otp));
                    }

                }catch (Exception e){

                }
            }
        });*/
    }

    @Override
    public void onPause() {
        super.onPause();
       /* SMSReciver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.e("Text","--------------sms---------> "+messageText);
                // fetOTP.setText(messageText.replaceAll("[^0-9]", ""));
                try{
                    String OTP = messageText.replaceAll("[^0-9]", "");
                    String first = Character.toString(OTP.charAt(0));
                    String second = Character.toString(OTP.charAt(1));
                    String third = Character.toString(OTP.charAt(2));
                    String four = Character.toString(OTP.charAt(3));
                    String five = Character.toString(OTP.charAt(4));
                    String six = Character.toString(OTP.charAt(5));
                    et_first.setText(first);
                    et_first.setSelection(et_first.getText().toString().length());
                    et_second.setText(second);
                    et_second.setSelection(et_second.getText().toString().length());
                    et_third.setText(third);
                    et_third.setSelection(et_third.getText().toString().length());
                    et_four.setText(four);
                    et_four.setSelection(et_four.getText().toString().length());
                    et_five.setText(five);
                    et_five.setSelection(et_five.getText().toString().length());
                    et_six.setText(six);
                    et_six.setSelection(et_six.getText().toString().length());
                    progress.setVisibility(View.GONE);
                    btn_verify_OTP.setVisibility(View.VISIBLE);

                    if(et_first.getText().length()==1 && et_second.getText().length()==1 && et_third.getText().length()==1 && et_four.getText().length()==1 && et_five.getText().length()==1 && et_six.getText().length()==1){
                        String EnterOTPValue = et_first.getText().toString()+et_second.getText().toString()+et_third.getText().toString()+et_four.getText().toString()+et_five.getText().toString()+et_six.getText().toString();

                        if(OTPValue.equalsIgnoreCase(EnterOTPValue.replace(" ",""))){
                            if (gpsTracker.isNetworkAvailable()) {
                                getMasterRequest();
                            } else {
                                showSnackBar(ActivityOTPVerified.this, "Please check your internet connection .");
                            }
                        }else{
                            showSnackBar(ActivityOTPVerified.this, getString(R.string.invalid_otp));
                        }
                    }else{
                        showSnackBar(ActivityOTPVerified.this, getString(R.string.enter_otp));
                    }

                }catch (Exception e){

                }

            }
        });*/
    }



    private void setUI() {

        btn_verify_OTP.setOnClickListener(this);
        btn_send_OTP.setOnClickListener(this);
        tv_edit_mobile.setOnClickListener(this);
        tv_resend_OTP.setOnClickListener(this);
    }

    private void setTimer(){
        try{
            new CountDownTimer(30* 1000+1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    int seconds = (int) (millisUntilFinished / 1000);
                    int minutes = seconds / 60;
                    seconds = seconds % 60;
                    // tv_timer.setText("Time remaining: " + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                    tv_timer.setText(getString(R.string.remaining_time)+ String.format("%02d", seconds)+" "+getString(R.string.sec));
                }
                public void onFinish() {
                    tv_timer.setVisibility(View.GONE);
                    tv_resend_OTP.setVisibility(View.VISIBLE);
                }
            }.start();
        }catch (Exception e){
            tv_timer.setVisibility(View.GONE);
            tv_resend_OTP.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.btn_send_OTP:

                if(fetMobile.getText().length()>4){
                    if (gpsTracker.isNetworkAvailable()) {
                        sendOTP();
                    } else {
                        showSnackBar(ActivityOTPVerified.this, getString(R.string.internet_conn));
                    }

                }else{
                    showSnackBar(ActivityOTPVerified.this, getString(R.string.enter_valid_mobile_no));/*""*/
                }
                break;

            case R.id.tv_edit_mobile:

                if(FlagLoginOTP){
                    Intent intent2 = new Intent(mContext, ActivityOTPVerified.class);
                    intent2.putExtra("Flag", "true");
                    startActivity(intent2);
                    finish();
                }else{
                    Intent intent2 = new Intent(mContext, ActivityOTPVerified.class);
                    intent2.putExtra("Flag", "false");
                    startActivity(intent2);
                    finish();
                }
                break;

            case R.id.btn_verify_OTP:

                if(et_first.getText().length()==1 && et_second.getText().length()==1 && et_third.getText().length()==1 && et_four.getText().length()==1 && et_five.getText().length()==1 && et_six.getText().length()==1){
                    String EnterOTPValue = et_first.getText().toString()+et_second.getText().toString()+et_third.getText().toString()+et_four.getText().toString()+et_five.getText().toString()+et_six.getText().toString();

                    if(OTPValue.equalsIgnoreCase(EnterOTPValue.replace(" ",""))){
                        getMasterRequest();
                    }else{
                        showSnackBar(ActivityOTPVerified.this, getString(R.string.invalid_otp));
                    }
                }else{
                    showSnackBar(ActivityOTPVerified.this, getString(R.string.enter_otp));
                }

                break;

            case R.id.tv_resend_OTP:

                et_first.setText("");
                et_second.setText("");
                et_third.setText("");
                et_four.setText("");
                et_five.setText("");
                et_six.setText("");
                et_six.clearFocus();
                et_first.requestFocus();
                btn_verify_OTP.setVisibility(View.GONE);

                if (sessionManager.isNetworkAvailable()) {

                    ll_send_OTP.setVisibility(View.GONE);
                    ll_verify_OTP.setVisibility(View.VISIBLE);
                    progress.setVisibility(View.VISIBLE);
                    otp_mobile.setText(getString(R.string.verify) +" "+ccp.getSelectedCountryCode()+fetMobile.getText().toString());
                    tv_timer.setVisibility(View.VISIBLE);
                    tv_resend_OTP.setVisibility(View.GONE);
                    setTimer();
                    sendOTP();
                } else {
                    showSnackBar(ActivityOTPVerified.this, getString(R.string.internet_conn));
                }

                break;
        }
    }

    public void sendOTP() {
        customResponseDialog.showCustomDialog();
        btn_verify_OTP.setVisibility(View.GONE);
        tv_timer.setVisibility(View.VISIBLE);
        tv_resend_OTP.setVisibility(View.GONE);

        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "users/sendOTPLogin?countryCode="+ccp.getSelectedCountryCode()+"&contactNo="+fetMobile.getText().toString()+"&device_id="+deviceId+"&token_id="+Firebase_deviceId;
        //final String url = sessionManager.getBaseUrl() + "users/sendOTP?contactNo="+ccp.getSelectedCountryCode()+fetMobile.getText().toString()+"&hashkey="+sessionManager.getHashkey();
        Log.e("send OTP URL--->",url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("send OTP Responce--->",jsonString);
                            customResponseDialog.hideCustomeDialog();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("status");
                                OTPValue = status.getString("message");

                                if(status.getString("status_code").equals("200")){
                                    ll_send_OTP.setVisibility(View.GONE);
                                    ll_verify_OTP.setVisibility(View.VISIBLE);
                                    progress.setVisibility(View.VISIBLE);
                                    otp_mobile.setText(getString(R.string.verify) +ccp.getSelectedCountryCode()+fetMobile.getText().toString());
                                    tv_timer.setVisibility(View.VISIBLE);
                                    tv_resend_OTP.setVisibility(View.GONE);
                                    sessionManager.setAgentCode(object.getString("agent_code"));
                                    sessionManager.setUserID(object.getString("agent_id"));
                                    sessionManager.setUserEmail(object.getString("email_id"));
                                    sessionManager.setUserName(object.getString("fullname"));
                                    sessionManager.setUserMobile(object.getString("phone"));
                                    sessionManager.setHashKey(object.getString("hash_key"));
                                    setTimer();
                                }else{
                                    showSnackBar(ActivityOTPVerified.this, OTPValue);
                                    progress.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }catch (Exception e){
                            showSnackBar(ActivityOTPVerified.this, getString(R.string.something_went_wrong));
                            progress.setVisibility(View.VISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(ActivityOTPVerified.this, getString(R.string.something_went_wrong));
                        progress.setVisibility(View.VISIBLE);
                    }
                }
        );

        queue.add(getRequest);


    }

    public void VerifyOTP() {
        customResponseDialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "users/verifyOTP/"+sessionManager.getHashkey();
        Log.e("Verify OTP URL--->",url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            customResponseDialog.hideCustomeDialog();
                            String jsonString = response.toString();
                            String sms;
                            Log.e("send OTP Responce--->",jsonString);

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                sms = object.getString("Status_message");

                                if(object.getString("status_code").equals("200")){
                                    showSnackBar(ActivityOTPVerified.this, sms);
                                   /* Intent intent = new Intent(ActivityOTPVerified.this, ActivityHomeScreen.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    sessionManager.setSelectFragmentPosition("0");
                                    finish();
                                    Bungee.zoom(mContext);
                                    Toast.makeText(mContext, sms, Toast.LENGTH_LONG).show();*/

                                }else{
                                    showSnackBar(ActivityOTPVerified.this, sms);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }catch (Exception e){
                            showSnackBar(ActivityOTPVerified.this, "Something went wrong.");
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(ActivityOTPVerified.this, "Something went wrong.");
                    }
                }
        );

        queue.add(getRequest);
    }

    public void getMasterRequest() {
        customResponseDialog = new CustomResponseDialog(mContext);
        customResponseDialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "users/getmasterdata?hash_key="+sessionManager.getHashkey();
        Log.e("getmasterdata URL--->",url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("getmasterdata Respo--->",jsonString);


                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("request_status");
                                message = status.getString("message");
                                status_code=status.getString("status_code");
                                if(status.getString("status_code").equals("200")){

                                    db.deleteAllCountries();
                                    db.deleteAllSalesAgents();
                                    db.deleteAllStatus();
                                    db.deleteAllSuppliers();

                                    JSONArray countriesArray = object.getJSONArray("countries");
                                    for(int i=0;countriesArray.length()>i;i++){
                                        JSONObject object1 = countriesArray.getJSONObject(i);
                                        db.setCountriesData(object1.getString("country_id"),object1.getString("country_name"),object1.getString("destinations"),object1.getString("executives"));

                                    }

                                    JSONArray sales_agentsArray = object.getJSONArray("sales_agents");
                                    for(int i=0;sales_agentsArray.length()>i;i++){
                                        JSONObject object1 = sales_agentsArray.getJSONObject(i);
                                        db.setSalesAgentsData(object1.getString("user_code"),object1.getString("user_id"),object1.getString("user_name"));

                                    }

                                    JSONArray statusesArray = object.getJSONArray("statuses");
                                    for(int i=0;statusesArray.length()>i;i++){
                                        JSONObject object1 = statusesArray.getJSONObject(i);
                                        db.setStatusData(object1.getString("status"),object1.getString("detailed_statuses"));
                                    }

                                    JSONArray suppliersArray = object.getJSONArray("suppliers");
                                    for(int i=0;suppliersArray.length()>i;i++){
                                        JSONObject object1 = suppliersArray.getJSONObject(i);
                                        db.setsuppliersData(object1.getString("supplier_id"),object1.getString("supplier_name"));
                                    }

                                }else{
                                    showSnackBar(ActivityOTPVerified.this, message);
                                    customResponseDialog.hideCustomeDialog();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                customResponseDialog.hideCustomeDialog();
                            }

                        }catch (Exception e){
                            showSnackBar(ActivityOTPVerified.this, getString(R.string.something_went_wrong));
                            customResponseDialog.hideCustomeDialog();
                        }

                        if(status_code.equalsIgnoreCase("200")){
                            customResponseDialog.hideCustomeDialog();
                            Toast.makeText(mContext, getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                            sessionManager.setLogin(true);
                            Intent mainIntent = new Intent(ActivityOTPVerified.this, ActivityHomeScreen.class);
                            startActivity(mainIntent);
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(ActivityOTPVerified.this, getString(R.string.something_went_wrong));
                    }
                }
        );

        queue.add(getRequest);


    }

    /*public void getRequestLogin() {

        sessionManager.setSignInMethod("Custom");
        JsonParser jsonParser = new JsonParser();
        JSONObject customerDetails = new JSONObject();
        try {
            customerDetails.put("Password", "");
            customerDetails.put("appVersion", AppConstants.VERSION_CODE);
            customerDetails.put("contactNo", fetMobile.getText().toString());
            customerDetails.put("deviceId", deviceId);
            customerDetails.put("dob", "");
            customerDetails.put("emailId", "");
            customerDetails.put("firstname", "");
            customerDetails.put("lastname", "");
            customerDetails.put("gcmId", "");
            customerDetails.put("gender", "");
            customerDetails.put("latitude", gpsTracker.getLatitude());
            customerDetails.put("longitude", gpsTracker.getLongitude());
            customerDetails.put("loginFrom", 4);
            customerDetails.put("modelNo", "");
            customerDetails.put("packageName", AppConstants.PACKAGE_NAME);
            customerDetails.put("platform", 1);
            customerDetails.put("profileImage", "");
            customerDetails.put("region", "");
            customerDetails.put("traceId", "");
            customerDetails.put("hashkey", "");
            customerDetails.put("isLogin", true);
            customerDetails.put("userId", 0);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonParser.getJsonLoginRequest(mContext, this, customerDetails, "users/authentication");

    }

    @Override
    public void onSuccess(String message) {
        Log.e("", "Registration Responce--->" + message);

        try {
            JSONObject jsonObject = new JSONObject(message);

            JSONObject jsonStatus = jsonObject.getJSONObject("status");
            String msg = jsonStatus.getString("Status_message");

            if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                sessionManager.setCartCount(jsonData.getString("cart_count"));
                sessionManager.setUserFirstName(jsonData.getString("firstname"));
                sessionManager.setUserLastName(jsonData.getString("lastname"));
                sessionManager.setEmailId(jsonData.getString("emailId"));
                sessionManager.setHashKey(jsonData.getString("hashkey"));
                sessionManager.setUserGender(jsonData.getString("gender"));
                sessionManager.setUserName(jsonData.getString("firstname") + " " + jsonData.getString("lastname"));
                sessionManager.setUserID(jsonData.getString("userId"));
                sessionManager.setLogedIn(true);
                sessionManager.setCountryCode(jsonData.getString("countryCode"));
                sessionManager.setUserMobile(jsonData.getString("contactNo"));
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();

                if (sessionManager.getWishListID() != 0) {
                    if (sessionManager.getWishList().equalsIgnoreCase("")) {
                        sessionManager.setWishList(String.valueOf(sessionManager.getWishListID()) + ",");
                    } else {
                        String old_Value = sessionManager.getWishList();
                        old_Value = old_Value.concat(String.valueOf(sessionManager.getWishListID()) + ",");
                        sessionManager.setWishList(old_Value);
                    }
                }

                sessionManager.setWishListID(0);
                Log.e("SELECTED WISH", sessionManager.getWishList());
                Intent intent = new Intent(ActivityOTPVerified.this, ActivityHomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                sessionManager.setSelectFragmentPosition("0");
                finish();
                Bungee.zoom(mContext);

            } else {
                showSnackBar(ActivityOTPVerified.this, msg);
            }

        } catch (JSONException e) {
            showSnackBar(ActivityOTPVerified.this, "Something went wrong. Please check your internet connection..");
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String message) {
        showSnackBar(ActivityOTPVerified.this, "Something went wrong. Please check your internet connection..");
    }*/

    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
