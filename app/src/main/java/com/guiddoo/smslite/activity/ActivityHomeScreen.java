package com.guiddoo.smslite.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.fragment.FragmentAddQuery;
import com.guiddoo.smslite.fragment.FragmentAddTravelAgent;
import com.guiddoo.smslite.fragment.FragmentMeeting;
import com.guiddoo.smslite.fragment.FragmentMeetingFollowUp;
import com.guiddoo.smslite.fragment.FragmentPromotion;
import com.guiddoo.smslite.fragment.FragmentQueries;
import com.guiddoo.smslite.fragment.FragmentSettings;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.FontsOverride;
import com.guiddoo.smslite.utils.GPSTracker;
import com.guiddoo.smslite.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
  Created by Ishwar on 20/12/17.
 */
public class ActivityHomeScreen extends AppCompatActivity {



    @BindView(R.id.fragment_container)
    LinearLayout fragment_container;

    @BindView(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;

    @BindView(R.id.add_itinerary)
    FloatingActionButton floatingActionButton;

    @BindView(R.id.fabSave)
    LinearLayout fabSave;

    @BindView(R.id.fabQuery)
    LinearLayout fabQuery;

    @BindView(R.id.fabFollowup)
    LinearLayout fabFollowup;

    @BindView(R.id.layoutFabSave)
    LinearLayout layoutFabSave;


    private LocationManager locationManager;
    private GPSTracker gpsTracker;
    private boolean FlagPerFlow = false,FlagPermissionFlow = true;
    private String[] AppPermissions = {Manifest.permission.ACCESS_FINE_LOCATION};
    private Context mContext;
    private String Status,msg,responce,resultObj;
    private boolean Backflag = false,FlagAPICall=true;
    private boolean FlagProfile = false, FlagAllData = false;
    private int Count = 0;
    private CustomResponseDialog customResponseDialog;
    private String ResponceData,deviceId;
    boolean doubleBackToExitPressedOnce = false,fabExpanded = false;;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{

            setContentView(R.layout.activity_home_screen);
            mContext = this;
            ButterKnife.bind(this);
            customResponseDialog = new CustomResponseDialog(mContext);
            gpsTracker = new GPSTracker(mContext,this);
            sessionManager = new SessionManager(mContext);
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/open_sans_regular.ttf");
            deviceId = Settings.Secure.getString(mContext.getContentResolver(),Settings.Secure.ANDROID_ID);
            Show();



            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                if (fabExpanded == true){
                    closeSubMenusFab();
                } else {
                    openSubMenusFab();
                }
                }
            });

            fabQuery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeSubMenusFab();
                    changeFragment(R.id.fragment_container, new FragmentAddQuery(),true);
                }
            });

            fabFollowup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeSubMenusFab();
                    FragmentMeetingFollowUp bookReview = new FragmentMeetingFollowUp();
                    Bundle bundle=new Bundle();
                    bundle.putString("ID", "");
                    bookReview.setArguments(bundle);
                    changeFragment(R.id.fragment_container, bookReview,true);
                }
            });

            fabSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        gpsTracker.showSettingsAlert(ActivityHomeScreen.this);

                    } else {
                        closeSubMenusFab();
                       // changeFragment(R.id.fragment_container, new FragmentAddTravelAgent(),true);
                        FragmentAddTravelAgent bookReview = new FragmentAddTravelAgent();
                        Bundle bundle=new Bundle();
                        bundle.putString("Flow", "Regular");
                        bookReview.setArguments(bundle);
                        changeFragment(R.id.fragment_container, bookReview,true);
                    }
                }
            });


        }catch (Exception e){

        }




    }

    private void Show(){
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.meeting, R.color.color_tab);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.queries, R.color.color_tab);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.promotion, R.color.color_tab);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.profile, R.color.color_tab);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("",  R.drawable.circle_white, R.color.colorWhite);

// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item5);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.colorWhite));


        bottomNavigation.setAccentColor(getResources().getColor(R.color.colorOrangeRust));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.colorBackgroundFossisl));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //bottomNavigation.setCurrentItem(2);
        bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);
        //bottomNavigation.manageFloatingActionButtonBehavior(fabSave);

        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setBehaviorTranslationEnabled(true);

        changeFragment(R.id.fragment_container, new FragmentMeeting(),true);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (position == 0) {
                    closeSubMenusFab();
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    changeFragment(R.id.fragment_container, new FragmentMeeting(),true);
                } else if (position == 1) {
                    closeSubMenusFab();
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    changeFragment(R.id.fragment_container, new FragmentQueries(),false);
                } else if (position == 3) {
                    closeSubMenusFab();
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    changeFragment(R.id.fragment_container, new FragmentPromotion(),false);
                } else if (position == 4) {
                    closeSubMenusFab();
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    changeFragment(R.id.fragment_container, new FragmentSettings(),false);
                }
                return true;
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.restoreBottomNavigation(false);
        closeSubMenusFab();

    }


    //closes FAB submenus
    public void closeSubMenusFab(){
        fragment_container.setEnabled(true);
        fragment_container.setClickable(true);
        layoutFabSave.setVisibility(View.INVISIBLE);
        floatingActionButton.setImageResource(R.drawable.cross_squres);
        floatingActionButton.setRotation(45);
        bottomNavigation.restoreBottomNavigation(false);
        fabExpanded = false;
    }

    //Opens FAB submenus
    private void openSubMenusFab(){

        //Change settings icon to 'X' icon
        layoutFabSave.setVisibility(View.VISIBLE);
        fragment_container.setEnabled(false);
        fragment_container.setClickable(false);
        floatingActionButton.setImageResource(R.drawable.cross_squres);
        floatingActionButton.setRotation(180);
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.restoreBottomNavigation(false);
        fabExpanded = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.restoreBottomNavigation(false);
        closeSubMenusFab();
    }

    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message,4000 ).show();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }
    }

}
