package com.guiddoo.smslite.model;

import java.io.Serializable;

/**
 * Created by Admin on 8/30/2016.
 */
public class QueryListModel implements Serializable {

    private String amount;
    private String booking_code;
    private String confirmation_guarantee;
    private String currency;
    private String destination;
    private String detailed_status;
    private String executive_name;
    private String markup;
    private String query_id;
    private String status;
    private String supplier;
    private String travel_agent;
    private String travel_agent_phone;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBooking_code() {
        return booking_code;
    }

    public void setBooking_code(String booking_code) {
        this.booking_code = booking_code;
    }

    public String getConfirmation_guarantee() {
        return confirmation_guarantee;
    }

    public void setConfirmation_guarantee(String confirmation_guarantee) {
        this.confirmation_guarantee = confirmation_guarantee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDetailed_status() {
        return detailed_status;
    }

    public void setDetailed_status(String detailed_status) {
        this.detailed_status = detailed_status;
    }

    public String getExecutive_name() {
        return executive_name;
    }

    public void setExecutive_name(String executive_name) {
        this.executive_name = executive_name;
    }

    public String getMarkup() {
        return markup;
    }

    public void setMarkup(String markup) {
        this.markup = markup;
    }

    public String getQuery_id() {
        return query_id;
    }

    public void setQuery_id(String query_id) {
        this.query_id = query_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getTravel_agent() {
        return travel_agent;
    }

    public void setTravel_agent(String travel_agent) {
        this.travel_agent = travel_agent;
    }

    public String getTravel_agent_phone() {
        return travel_agent_phone;
    }

    public void setTravel_agent_phone(String travel_agent_phone) {
        this.travel_agent_phone = travel_agent_phone;
    }
}
