package com.guiddoo.smslite.model;

import java.io.Serializable;

public class ModelOCStatusList implements Serializable {

    private String detailed_status_id;
    private String detailed_status;

    public String getDetailed_status_id() {
        return detailed_status_id;
    }

    public void setDetailed_status_id(String detailed_status_id) {
        this.detailed_status_id = detailed_status_id;
    }

    public String getDetailed_status() {
        return detailed_status;
    }

    public void setDetailed_status(String detailed_status) {
        this.detailed_status = detailed_status;
    }
}
