package com.guiddoo.smslite.model;

import java.io.Serializable;

/**
 * Created by Admin on 8/30/2016.
 */
public class ModelPromotion implements Serializable {


    private String active_status;
    private String created_datetime;
    private String destination_id;
    private String destination_name;
    private String display_order;
    private String image_url;
    private String validity_end_date;
    private String validity_start_date;

    public String getActive_status() {
        return active_status;
    }

    public void setActive_status(String active_status) {
        this.active_status = active_status;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getDestination_id() {
        return destination_id;
    }

    public void setDestination_id(String destination_id) {
        this.destination_id = destination_id;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }

    public String getDisplay_order() {
        return display_order;
    }

    public void setDisplay_order(String display_order) {
        this.display_order = display_order;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getValidity_end_date() {
        return validity_end_date;
    }

    public void setValidity_end_date(String validity_end_date) {
        this.validity_end_date = validity_end_date;
    }

    public String getValidity_start_date() {
        return validity_start_date;
    }

    public void setValidity_start_date(String validity_start_date) {
        this.validity_start_date = validity_start_date;
    }
}
