package com.guiddoo.smslite.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelCountriesList implements Serializable{


    private String country_name;
    private String country_id;
    private ArrayList<ModelDestinationsList> modelDestinationsList;
    private ArrayList<ModelExecutivesList> modelExecutivesLists;


    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public ArrayList<ModelDestinationsList> getModelDestinationsList() {
        return modelDestinationsList;
    }

    public void setModelDestinationsList(ArrayList<ModelDestinationsList> modelDestinationsList) {
        this.modelDestinationsList = modelDestinationsList;
    }

    public ArrayList<ModelExecutivesList> getModelExecutivesLists() {
        return modelExecutivesLists;
    }

    public void setModelExecutivesLists(ArrayList<ModelExecutivesList> modelExecutivesLists) {
        this.modelExecutivesLists = modelExecutivesLists;
    }

}

