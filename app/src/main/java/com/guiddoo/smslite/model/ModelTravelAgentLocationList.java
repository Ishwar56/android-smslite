package com.guiddoo.smslite.model;

import java.io.Serializable;

import ir.mirrajabi.searchdialog.core.Searchable;

public class ModelTravelAgentLocationList implements Serializable , Searchable {

    private String travel_id;
    private String travel_name;
    private String comp_name;
    private String call_status;
    private String Location;

    public String getTravel_id() {
        return travel_id;
    }

    public void setTravel_id(String travel_id) {
        this.travel_id = travel_id;
    }

    public String getTravel_name() {
        return travel_name;
    }

    public void setTravel_name(String travel_name) {
        this.travel_name = travel_name;
    }

    public String getComp_name() {
        return comp_name;
    }

    public void setComp_name(String comp_name) {
        this.comp_name = comp_name;
    }

    public String getCall_status() {
        return call_status;
    }

    public void setCall_status(String call_status) {
        this.call_status = call_status;
    }

    @Override
    public String getTitle() {
        return Location;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }
}
