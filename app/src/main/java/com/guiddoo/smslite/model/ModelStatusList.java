package com.guiddoo.smslite.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ishwar on 23/02/2019.
 */
public class ModelStatusList implements Serializable {


    private String Status;
    private ArrayList<ModelOCStatusList> modelOCStatusLists;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


    public ArrayList<ModelOCStatusList> getModelOCStatusLists() {
        return modelOCStatusLists;
    }

    public void setModelOCStatusLists(ArrayList<ModelOCStatusList> modelOCStatusLists) {
        this.modelOCStatusLists = modelOCStatusLists;
    }
}

