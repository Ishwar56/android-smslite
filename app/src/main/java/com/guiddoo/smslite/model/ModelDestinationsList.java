package com.guiddoo.smslite.model;

import java.io.Serializable;

public class ModelDestinationsList implements Serializable {

    private String destination_id;
    private String destination_name;

    public String getDestination_id() {
        return destination_id;
    }

    public void setDestination_id(String destination_id) {
        this.destination_id = destination_id;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }
}
