package com.guiddoo.smslite.model;

import java.io.Serializable;

/**
 * Created by Admin on 8/30/2019.
 */
public class QueryFollowupListModel implements Serializable {

    private String confirmation_guarantee;
    private String date;
    private String executive_name;
    private String query;
    private String remarks;
    private String sales_amount;
    private String time;
    private String type;

    public String getConfirmation_guarantee() {
        return confirmation_guarantee;
    }

    public void setConfirmation_guarantee(String confirmation_guarantee) {
        this.confirmation_guarantee = confirmation_guarantee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExecutive_name() {
        return executive_name;
    }

    public void setExecutive_name(String executive_name) {
        this.executive_name = executive_name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSales_amount() {
        return sales_amount;
    }

    public void setSales_amount(String sales_amount) {
        this.sales_amount = sales_amount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
