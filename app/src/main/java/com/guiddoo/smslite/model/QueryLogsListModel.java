package com.guiddoo.smslite.model;

import java.io.Serializable;

/**
 * Created by Admin on 8/30/2016.
 */
public class QueryLogsListModel implements Serializable {


    private String created_by;
    private String log_title;

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getLog_title() {
        return log_title;
    }

    public void setLog_title(String log_title) {
        this.log_title = log_title;
    }
}
