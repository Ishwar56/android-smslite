package com.guiddoo.smslite.model;

import java.io.Serializable;

public class AreaModel implements Serializable {
    public String Area_ID;
    public String Title;

    public String getArea_ID() {
        return Area_ID;
    }

    public void setArea_ID(String area_ID) {
        Area_ID = area_ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
