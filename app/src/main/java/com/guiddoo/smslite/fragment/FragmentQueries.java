package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.adapter.AdapterCountry;
import com.guiddoo.smslite.adapter.AdapterDestination;
import com.guiddoo.smslite.adapter.AdapterQueryList;
import com.guiddoo.smslite.adapter.AdapterStatus;
import com.guiddoo.smslite.adapter.AdapterStatusDetails;
import com.guiddoo.smslite.model.ModelCountriesList;
import com.guiddoo.smslite.model.ModelDestinationsList;
import com.guiddoo.smslite.model.ModelOCStatusList;
import com.guiddoo.smslite.model.ModelStatusList;
import com.guiddoo.smslite.model.QueryListModel;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentQueries extends Fragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_filter)
    ImageView iv_filter;

    @BindView(R.id.rv_query)
    RecyclerView rv_query;

    @BindView(R.id.tv_query_follow_up)
    TextView tv_query_follow_up;

    @BindView(R.id.tv_no_record)
    TextView tv_no_record;

    @BindView(R.id.tv_no_of_record)
    TextView tv_no_of_record;

    SearchView mSearchView;
    MenuItem mSearch;


    private Context mContext;
    private String responce, resultObj, message, searchText;
    private CustomResponseDialog customResponseDialog;
    public static int sizeofList;
    private SessionManager sessionManager;

    private boolean doubleBackToExitPressedOnce = false, filterFlag = false, Flag_Status = true, Flag_Detail_Status = false, Flag_City = false;
    ;
    private ArrayList<QueryListModel> followeUpArrayList;
    private ArrayList<QueryListModel> queryOpenArrayList = new ArrayList<>();
    private ArrayList<QueryListModel> queryClosedArrayList = new ArrayList<>();
    private QueryListModel modelQuery;
    private AdapterQueryList adapterQueryList;
    private SQLiteHandler db;
    private BottomSheetDialog dialog;
    private String sp_spinner_Country_id, sp_spinner_City_id, sp_spinner_Country_Name, sp_spinner_City_Name, sp_spinner_Status_value, sp_spinner_details_Status_ID, sp_spinner_details_Status_value;

    ArrayList<ModelStatusList> arrayModelStatusList = new ArrayList<>();
    ArrayList<ModelOCStatusList> arrayModelStatusDetailsList = new ArrayList<>();
    ArrayList<ModelCountriesList> arrayCountriesList = new ArrayList<>();
    ArrayList<ModelDestinationsList> arrayDestinationList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_query, container, false);
        mContext = getActivity();
        db = new SQLiteHandler(mContext);
        ButterKnife.bind(this, view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        dialog = new BottomSheetDialog(mContext);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getString(R.string.tab_2));
        //tv_title.setText("Queries");
        setHasOptionsMenu(true);
        sessionManager.setSorting_Position("");


        tv_query_follow_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentQueryFollowUp bookReview = new FragmentQueryFollowUp();
                Bundle bundle = new Bundle();
                //bundle.putString("Book_ID", liabraryList.get(position).getBookId());
                // bundle.putString("Book_Name", liabraryList.get(position).getBookName());
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview, true);
            }
        });

        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (filterFlag) {

                    dialog.setContentView(R.layout.query_filter_dialog);

                    dialog.getWindow().setDimAmount(0.0f);

                    dialog.show();

                    final Spinner sp_spinner_Status = (Spinner) dialog.findViewById(R.id.sp_spinner_Status);
                    final Spinner sp_spinner_detailed_Status = (Spinner) dialog.findViewById(R.id.sp_spinner_detailed_Status);
                    final Spinner sp_spinner_Country = (Spinner) dialog.findViewById(R.id.sp_spinner_Country);
                    final Spinner sp_spinner_City = (Spinner) dialog.findViewById(R.id.sp_spinner_City);

                    final CheckBox cb_status = (CheckBox) dialog.findViewById(R.id.cb_status);
                    final CheckBox cb_status_details = (CheckBox) dialog.findViewById(R.id.cb_status_details);
                    final CheckBox cb_country = (CheckBox) dialog.findViewById(R.id.cb_country);
                    final CheckBox cb_city = (CheckBox) dialog.findViewById(R.id.cb_city);

                   /* Flag_Status = false;
                    Flag_Detail_Status = false;
                    Flag_City = false;*/

                    if(Flag_Status){
                        cb_status.setChecked(true);
                    }else {
                        cb_status.setChecked(false);
                    }
                    if(Flag_Detail_Status ){
                        cb_status_details.setChecked(true);
                    }
                    if(Flag_City){
                        cb_country.setChecked(true);
                        cb_city.setChecked(true);
                    }

                    cb_country.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                     @Override
                       public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                         if(isChecked){
                             cb_city.setChecked(true);
                             Flag_City = true;
                         }else{
                             cb_city.setChecked(false);
                             Flag_City = false;
                         }
                        }
                      }
                    );

                    cb_city.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                     @Override
                     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                         if(isChecked){
                             cb_country.setChecked(true);
                             Flag_City = true;
                         }else{
                             cb_country.setChecked(false);
                             Flag_City = false;
                         }

                     }
                    }
                    );


                    LinearLayout ll_apply_filter = (LinearLayout) dialog.findViewById(R.id.ll_apply_filter);
                    LinearLayout ll_clear_filter = (LinearLayout) dialog.findViewById(R.id.ll_clear_filter);


                    try {

                        arrayModelStatusList = new ArrayList<>();
                        arrayModelStatusDetailsList = new ArrayList<>();
                        arrayCountriesList = new ArrayList<>();
                        arrayDestinationList = new ArrayList<>();


                        arrayModelStatusList = db.getStatusArray();
                        if (arrayModelStatusList.size() > 0 && arrayModelStatusList != null) {
                            AdapterStatus adapterstate = new AdapterStatus(getActivity(), R.layout.spiner_area_list, R.id.title, arrayModelStatusList);
                            sp_spinner_Status.setAdapter(adapterstate);
                        }

                        sp_spinner_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (arrayModelStatusList.size() > 0 && arrayModelStatusList != null) {
                                    sp_spinner_Status_value = arrayModelStatusList.get(position).getStatus();
                                    ArrayList<ModelOCStatusList> arrayModelStatusDetailsList2 = new ArrayList<>();
                                    arrayModelStatusDetailsList2 = db.getStatusDetailsArray(sp_spinner_Status_value);
                                    arrayModelStatusDetailsList = arrayModelStatusDetailsList2;
                                    try {
                                        if (arrayModelStatusDetailsList.size() > 0 && arrayModelStatusDetailsList != null) {
                                            AdapterStatusDetails adapterstate = new AdapterStatusDetails(getActivity(), R.layout.spiner_area_list, R.id.title, arrayModelStatusDetailsList);
                                            sp_spinner_detailed_Status.setAdapter(adapterstate);
                                        }
                                    } catch (NullPointerException e) {
                                        showSnackBar(getActivity(), mContext.getString(R.string.data_loading));
                                    }

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                        sp_spinner_detailed_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (arrayModelStatusDetailsList.size() > 0 && arrayModelStatusDetailsList != null) {
                                    sp_spinner_details_Status_value = arrayModelStatusDetailsList.get(position).getDetailed_status();
                                    sp_spinner_details_Status_ID = arrayModelStatusDetailsList.get(position).getDetailed_status_id();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                        arrayCountriesList = db.getCountriesArray();
                        if (arrayCountriesList.size() > 0 && arrayCountriesList != null) {
                            AdapterCountry adapterstate = new AdapterCountry(getActivity(), R.layout.spiner_area_list, R.id.title, arrayCountriesList);
                            sp_spinner_Country.setAdapter(adapterstate);
                        }

                        sp_spinner_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                sp_spinner_Country_id = arrayCountriesList.get(position).getCountry_id();
                                sp_spinner_Country_Name = arrayCountriesList.get(position).getCountry_name();


                               /* if (arrayCountriesList.size() > 0 && arrayCountriesList != null) {
                                    arrayDestinationList = db.getCountriesDestinationArray(arrayCountriesList.get(position).getCountry_id());
                                    if (arrayDestinationList.size() > 0 && arrayDestinationList != null) {
                                        AdapterDestination adapterstate = new AdapterDestination(getActivity(), R.layout.spiner_area_list, R.id.title, arrayDestinationList);
                                        sp_spinner_City.setAdapter(adapterstate);
                                    }

                                }*/
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                       /* sp_spinner_City.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (arrayDestinationList.size() > 0 && arrayDestinationList != null) {
                                    *//*sp_spinner_City_id = arrayDestinationList.get(position).getDestination_id();
                                    sp_spinner_City_Name = arrayDestinationList.get(position).getDestination_name();*//*
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });*/

                    } catch (NullPointerException e) {
                        dialog.dismiss();
                        showSnackBar(getActivity(), mContext.getString(R.string.data_loading));

                    }


                    ll_apply_filter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            try {
                                if (cb_status.isChecked()) {
                                    Flag_Status = true;
                                } else {
                                    Flag_Status = false;
                                }

                                if (cb_status_details.isChecked()) {
                                    Flag_Detail_Status = true;

                                } else {
                                    Flag_Detail_Status = false;
                                }

                                if (cb_country.isChecked()) {
                                    Flag_City = true;
                                } else {
                                    Flag_City = false;
                                }

                                if (cb_city.isChecked()) {
                                    Flag_City = true;
                                } else {
                                    Flag_City = false;
                                }
                            } catch (Exception e) {

                            }

                            try{
                                queryClosedArrayList = db.getQueryStatusArray(sp_spinner_Status_value, sp_spinner_details_Status_value, sp_spinner_Country_Name, Flag_Status, Flag_Detail_Status, Flag_City);
                                if (queryClosedArrayList.size() > 0 && queryClosedArrayList!=null) {
                                    tv_no_record.setVisibility(View.GONE);
                                    rv_query.setVisibility(View.VISIBLE);
                                    tv_no_of_record.setText(getText(R.string.total_quries) + " : " + queryClosedArrayList.size());
                                    GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    adapterQueryList = new AdapterQueryList(mContext, queryClosedArrayList, getActivity(), fragmentManager, FragmentQueries.this);
                                    rv_query.setLayoutManager(manager);
                                    rv_query.setAdapter(adapterQueryList);
                                } else {
                                    tv_no_of_record.setText(getText(R.string.total_quries) + " : 0");
                                    showSnackBar(getActivity(), getString(R.string.record_not_found));
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    rv_query.setVisibility(View.GONE);
                                }

                            }catch (Exception e){
                                tv_no_of_record.setText(getText(R.string.total_quries) + " : 0");
                                showSnackBar(getActivity(), getString(R.string.record_not_found));
                                tv_no_record.setVisibility(View.VISIBLE);
                                rv_query.setVisibility(View.GONE);
                            }

                            dialog.dismiss();



                        }
                    });


                    ll_clear_filter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Flag_Status = false;
                            Flag_Detail_Status = false;
                            Flag_City = false;
                            if (followeUpArrayList.size() > 0) {
                                tv_no_record.setVisibility(View.GONE);
                                rv_query.setVisibility(View.VISIBLE);
                                tv_no_of_record.setText(getText(R.string.total_quries) + " : " + followeUpArrayList.size());
                                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                adapterQueryList = new AdapterQueryList(mContext, followeUpArrayList, getActivity(), fragmentManager, FragmentQueries.this);
                                rv_query.setLayoutManager(manager);
                                rv_query.setAdapter(adapterQueryList);
                            } else {
                                showSnackBar(getActivity(), getString(R.string.record_not_found));
                                tv_no_record.setVisibility(View.VISIBLE);
                                rv_query.setVisibility(View.GONE);
                            }
                            dialog.dismiss();

                        }
                    });


                } else {

                    showSnackBar(getActivity(), mContext.getString(R.string.data_loading));
                }


            }
        });

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menus, MenuInflater inflater) {
        try {
            inflater.inflate(R.menu.search_menu, menus);
            mSearch = menus.findItem(R.id.action_search);
            mSearchView = (SearchView) mSearch.getActionView();
            mSearchView.setPadding(0, 8, 0, 8);
            mSearchView.setMaxWidth(Integer.MAX_VALUE);
            View searchplate = (View) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchplate.setBackgroundResource(R.drawable.custombg_white_border);
            //searchplate.setPadding(0,5,0,5);

        } catch (Exception e) {

        }

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                tv_no_of_record.setText(getText(R.string.total_quries) + " : " + sizeofList);
                searchText = s;
                adapterQueryList.getFilter().filter(searchText);
                // tv_no_of_record.setText(getText(R.string.total_quries)+" : "+sizeofList);
                return true;
            }

        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tv_no_of_record.setText(getText(R.string.total_quries) + " : " + sizeofList);
                return false;
            }
        });

        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                tv_no_of_record.setText(getText(R.string.total_quries) + " : " + sizeofList);
            }
        });


        super.onCreateOptionsMenu(menus, inflater);
    }


    public void getQueryRequest() {
        customResponseDialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = sessionManager.getBaseUrl() + "users/getqueries?hash_key=" + sessionManager.getHashkey();
        //final String url = sessionManager.getBaseUrl() + "users/sendOTP?contactNo="+ccp.getSelectedCountryCode()+fetMobile.getText().toString()+"&hashkey="+sessionManager.getHashkey();
        Log.e("getqueries URL--->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("getqueries Respon--->", jsonString);
                            customResponseDialog.hideCustomeDialog();
                            db.deleteAllQueryData();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("status");
                                message = status.getString("message");

                                if (status.getString("status_code").equals("200")) {
                                    tv_no_record.setVisibility(View.GONE);
                                    rv_query.setVisibility(View.VISIBLE);

                                    try {
                                        JSONArray travel_agents = object.getJSONArray("queries");
                                        followeUpArrayList = new ArrayList<>();
                                        for (int i = 0; i < travel_agents.length(); i++) {
                                            JSONObject jsonObject1 = travel_agents.getJSONObject(i);
                                            modelQuery = new QueryListModel();

                                            modelQuery.setAmount(jsonObject1.getString("amount"));
                                            modelQuery.setBooking_code(jsonObject1.getString("booking_code"));
                                            modelQuery.setConfirmation_guarantee(jsonObject1.getString("confirmation_guarantee"));
                                            modelQuery.setCurrency(jsonObject1.getString("currency"));
                                            modelQuery.setDestination(jsonObject1.getString("destination"));
                                            modelQuery.setDetailed_status(jsonObject1.getString("detailed_status"));
                                            modelQuery.setExecutive_name(jsonObject1.getString("executive_name"));
                                            modelQuery.setMarkup(jsonObject1.getString("markup"));
                                            modelQuery.setQuery_id(jsonObject1.getString("query_id"));
                                            modelQuery.setStatus(jsonObject1.getString("status"));
                                            modelQuery.setSupplier(jsonObject1.getString("supplier"));
                                            modelQuery.setTravel_agent(jsonObject1.getString("travel_agent"));
                                            modelQuery.setTravel_agent_phone(jsonObject1.getString("travel_agent_phone"));
                                            followeUpArrayList.add(modelQuery);

                                        }

                                        new LoadData().execute();

                                    } catch (NullPointerException e) {
                                        showSnackBar(getActivity(), getString(R.string.record_not_found));
                                    }


                                    if (followeUpArrayList.size() > 0) {
                                        tv_no_of_record.setText(getText(R.string.total_quries) + " : " + followeUpArrayList.size());
                                        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        adapterQueryList = new AdapterQueryList(mContext, followeUpArrayList, getActivity(), fragmentManager, FragmentQueries.this);
                                        rv_query.setLayoutManager(manager);
                                        rv_query.setAdapter(adapterQueryList);
                                    } else {
                                        showSnackBar(getActivity(), getString(R.string.record_not_found));
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        rv_query.setVisibility(View.GONE);
                                    }


                                } else {
                                    showSnackBar(getActivity(), message);
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    rv_query.setVisibility(View.GONE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                            tv_no_record.setVisibility(View.VISIBLE);
                            rv_query.setVisibility(View.GONE);

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                        tv_no_record.setVisibility(View.VISIBLE);
                        rv_query.setVisibility(View.GONE);
                    }
                }
        );

        queue.add(getRequest);


    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (int i = 0; followeUpArrayList.size() > i; i++) {
                    db.setQueryData(followeUpArrayList.get(i).getQuery_id(), followeUpArrayList.get(i).getAmount(), followeUpArrayList.get(i).getBooking_code(),
                            followeUpArrayList.get(i).getConfirmation_guarantee(), followeUpArrayList.get(i).getCurrency(), followeUpArrayList.get(i).getDestination(),
                            followeUpArrayList.get(i).getDetailed_status(), followeUpArrayList.get(i).getExecutive_name(), followeUpArrayList.get(i).getMarkup(),
                            followeUpArrayList.get(i).getStatus(), followeUpArrayList.get(i).getSupplier(), followeUpArrayList.get(i).getTravel_agent(), followeUpArrayList.get(i).getTravel_agent_phone());
                }

                //AsyncTask.execute(runnable);
            } catch (SecurityException | NullPointerException e) {
                try {
                    for (int i = 0; followeUpArrayList.size() > i; i++) {
                        db.setQueryData(followeUpArrayList.get(i).getQuery_id(), followeUpArrayList.get(i).getAmount(), followeUpArrayList.get(i).getBooking_code(),
                                followeUpArrayList.get(i).getConfirmation_guarantee(), followeUpArrayList.get(i).getCurrency(), followeUpArrayList.get(i).getDestination(),
                                followeUpArrayList.get(i).getDetailed_status(), followeUpArrayList.get(i).getExecutive_name(), followeUpArrayList.get(i).getMarkup(),
                                followeUpArrayList.get(i).getStatus(), followeUpArrayList.get(i).getSupplier(), followeUpArrayList.get(i).getTravel_agent(), followeUpArrayList.get(i).getTravel_agent_phone());
                    }

                } catch (SecurityException | NullPointerException e1) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("@@@@@@@@@@@@@@", "Inside onPostExecute--->");
            filterFlag = true;
            iv_filter.setVisibility(View.VISIBLE);
        }
    }

    public static void setListCounts(int size) {
        FragmentQueries queries = new FragmentQueries();
        queries.setListCountss(size);
    }

    public void setListCountss(int size) {
        tv_no_of_record.setText(getText(R.string.total_quries) + " : " + size);
    }


    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

    }


    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }


    @Override
    public void onResume() {
        super.onResume();
        getQueryRequest();
        iv_filter.setVisibility(View.INVISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);

                    return true;
                }
                return false;
            }
        });
    }


}
