package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.utils.CustomResponseDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentQueryFollowUp extends Fragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;


    @BindView(R.id.fetAddQuery)
    FormEditText fetAddQuery;
    @BindView(R.id.fetAmount)
    FormEditText fetAmount;
    @BindView(R.id.fetMarkUp)
    FormEditText fetMarkUp;
    @BindView(R.id.fetRemark)
    FormEditText fetRemark;

    @BindView(R.id.tv_Submit)
    TextView tv_Submit;


    private Context mContext;
    private String responce,resultObj;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_query_follow_up, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        customResponseDialog = new CustomResponseDialog(mContext);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getText(R.string.followup_details));

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {fetAmount,fetMarkUp,fetRemark,fetAddQuery};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }

                if (allValid) {
                    Toast.makeText(mContext, "Query submitted successfully", Toast.LENGTH_SHORT).show();
                    getFragmentManager().popBackStack();
                }

            }
        });


        return view;
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }

    }



    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }


}
