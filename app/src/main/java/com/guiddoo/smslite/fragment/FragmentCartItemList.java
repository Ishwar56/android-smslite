package com.guiddoo.smslite.fragment;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.model.ModelPromotion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;



public class FragmentCartItemList extends Fragment {

    private static final String ARG_PRODUCTS = "CartListDetailModels";
    private static ArrayList<ModelPromotion> activityList;
    int position = 0;
    TextView tv_start_date,tv_end_date,tv_destination;
    private String[] AppPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    RatingBar rb_rating;
    ImageView iv_image,iv_share;
    ProgressBar progressBar;
    CardView cv_discount;
    private boolean FlagPerFlow = false,FlagPermissionFlow = true;
    LinearLayout ll_content_click;
    private Context mContext;
    private  Uri bmpUri;
    //RecyclerView prodList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.adapter_cart_activity, container, false);
        mContext = getActivity();
       // prodList = (RecyclerView) view.findViewById(R.id.rv_list);
        tv_start_date = (TextView) view.findViewById(R.id.tv_start_date);
        tv_end_date = (TextView) view.findViewById(R.id.tv_end_date);
        tv_destination = (TextView) view.findViewById(R.id.tv_destination);
        iv_image = (ImageView) view.findViewById(R.id.iv_image);
        iv_share = (ImageView) view.findViewById(R.id.iv_share);

        if (getArguments() != null) {
            activityList = (ArrayList<ModelPromotion>) getArguments().getSerializable(ARG_PRODUCTS);
            this.position = getArguments().getInt("KEY_POSITION");
        }
       /* RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        prodList.setLayoutManager(mLayoutManager);
        productAdapter= new CartBookingListAdapter(getActivity(), allProducts,getActivity(),false);
        prodList.setAdapter(productAdapter);*/
        tv_start_date.setText(activityList.get(position).getValidity_start_date());
        tv_end_date.setText(activityList.get(position).getValidity_end_date());
        tv_destination.setText(activityList.get(position).getDestination_name());

        try{
            Glide.with(getActivity())
                    .load(activityList.get(position).getImage_url())
                    .placeholder(R.drawable.logo_loader)
                    .error(R.drawable.logo_loader)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_image);
        }catch (Exception e){
            Glide.with(getActivity())
                    .load(R.drawable.logo_loader)
                    .placeholder(R.drawable.logo_loader)
                    .error(R.drawable.logo_loader)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_image);
        }


        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkAppPermissions(mContext, AppPermissions)) {
                    Log.e("Allow------>","0000000");
                    ShareImage();


                } else if(FlagPermissionFlow){
                    ActivityCompat.requestPermissions(getActivity(), AppPermissions, 102);
                }


            }

        });

        return view;
    }


    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(FlagPermissionFlow){
            if (requestCode == 102 && FlagPermissionFlow) {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FlagPermissionFlow=false;
                    Log.e("Allow------>","0000000");
                    ShareImage();
                }else{
                    Log.e("Denai------>","111111");

                }
            }

        }

    }

    private void ShareImage(){

        iv_image.buildDrawingCache();
        Bitmap bm=iv_image.getDrawingCache();

        OutputStream fOut = null;
        Uri outputFileUri;

        try {
            File root = new File(Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator);
            root.mkdirs();
            File sdImageMainDirectory = new File(root, activityList.get(position).getDisplay_order()+".jpg");
            outputFileUri = Uri.fromFile(sdImageMainDirectory);
            fOut = new FileOutputStream(sdImageMainDirectory);
        } catch (Exception e) {
            Toast.makeText(mContext, "Error occured. Please try again later.", Toast.LENGTH_SHORT).show();
        }

        try {
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
        }


        PackageManager pm=getActivity().getPackageManager();


        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("image/*");
            waIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator+ activityList.get(position).getDisplay_order()+".jpg"));
            Log.e("Path-->",Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator+ activityList.get(position).getDisplay_order()+".jpg");
            waIntent.putExtra(Intent.EXTRA_TEXT, "Start Date:"+tv_start_date.getText().toString() + "\n " +"End Date:"+ tv_end_date.getText()+"\n"+"Destination:"+ tv_destination.getText()+"\n");
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(mContext, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();

            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/html");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your promotion details are here");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Please find the details attached....");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator+ activityList.get(position).getDisplay_order()+".jpg"));
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Start Date : "+tv_start_date.getText().toString() + "\n " +"End Date : "+ tv_end_date.getText()+"\n"+"Destination : "+ tv_destination.getText()+"\n");
            startActivity(emailIntent);
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                getActivity().finish();
                Log.i("Finished Data", "");
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(mContext, "No way you can share Promotion,enjoy alone", Toast.LENGTH_SHORT).show();
            }
        }
    }



    public static FragmentCartItemList newInstance(ArrayList<ModelPromotion> products, int position) {
        FragmentCartItemList fragment = new FragmentCartItemList();
        Bundle args = new Bundle();
        args.putSerializable("CartListDetailModels", (ArrayList<ModelPromotion>) products);
        args.putInt("KEY_POSITION",position);
        fragment.setArguments(args);
        return fragment;
    }

}
