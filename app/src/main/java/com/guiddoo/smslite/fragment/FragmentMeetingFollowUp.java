package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.adapter.AdapterCurrency;
import com.guiddoo.smslite.datamodel.JsonParser;
import com.guiddoo.smslite.model.ModelTravelAgentList;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;
import com.guiddoo.smslite.utils.TaskNotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class FragmentMeetingFollowUp extends Fragment implements TaskNotifier {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.fetRemark)
    FormEditText fetRemark;

    @BindView(R.id.sp_spinner_travel_Agent)
    Spinner sp_spinner_travel_Agent;
    @BindView(R.id.ll_travel_Agent_Name)
    LinearLayout ll_travel_Agent_Name;
    @BindView(R.id.tv_travel_Agent_Name)
    TextView tv_travel_Agent_Name;

    @BindView(R.id.fetDate)
    FormEditText fetDate;

    @BindView(R.id.fetTime)
    FormEditText fetTime;

    @BindView(R.id.sp_spinner_status_call)
    Spinner sp_spinner_status_call;

    @BindView(R.id.tv_Submit)
    TextView tv_Submit;

    private SessionManager sessionManager;

    private Context mContext;
    private String responce, resultObj, time, sp_spinner_status_call_value, sp_spinner_travel_Agent_id;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;
    private Calendar myFromCalendar;
    private DatePickerDialog.OnDateSetListener Fromdate;
    private ArrayList<String> arrayCurrencyList = new ArrayList<>();
    private ArrayList<ModelTravelAgentList> arrayTravelAgentsList = new ArrayList<>();
    private SQLiteHandler db;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meeting_followup, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        db = new SQLiteHandler(mContext);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getText(R.string.meeting_followup));


        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        arrayCurrencyList.add("Cold");
        arrayCurrencyList.add("Warm");
        arrayCurrencyList.add("Hot");

        if (arrayCurrencyList.size() > 0 && arrayCurrencyList != null) {
            AdapterCurrency adapterstate = new AdapterCurrency(getActivity(), R.layout.spiner_area_list, R.id.title, arrayCurrencyList);
            sp_spinner_status_call.setAdapter(adapterstate);
        }

        sp_spinner_status_call.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (arrayCurrencyList.size() > 0 && arrayCurrencyList != null) {
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_status_call_value = arrayCurrencyList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        try {
            arrayTravelAgentsList = db.getTravelArray();
            if (arrayTravelAgentsList.size() > 0 && arrayTravelAgentsList != null) {
                //AdapterTravelAgent adapterstate = new AdapterTravelAgent(getActivity(), R.layout.spiner_area_list, R.id.title, arrayTravelAgentsList);
                //sp_spinner_travel_Agent.setAdapter(adapterstate);
                tv_travel_Agent_Name.setText(arrayTravelAgentsList.get(0).getComp_name()+" ("+arrayTravelAgentsList.get(0).getTravel_name()+")");
            }
        } catch (NullPointerException | SecurityException e) {

        }


        try {
            if (arrayTravelAgentsList.size() > 0 && arrayTravelAgentsList != null) {
                sp_spinner_travel_Agent_id = getArguments().getString("ID");
                selectSpinnerValue(sp_spinner_travel_Agent, sp_spinner_travel_Agent_id);
            }
            //fetAgent.setText(getArguments().getString("Name"));

        } catch (NullPointerException e) {

        }

        /*sp_spinner_travel_Agent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (arrayTravelAgentsList.size() > 0 && arrayTravelAgentsList != null) {
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_travel_Agent_id = arrayTravelAgentsList.get(position).getTravel_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        ll_travel_Agent_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Travel Agent...",
                        "Search Travel Agent...?", null, arrayTravelAgentsList,
                        new SearchResultListener<ModelTravelAgentList>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelTravelAgentList item, int position
                            ) {
                                //sp_spinner_travel_Agent_id=arrayTravelAgentsList.get(position).getTravel_id();
                                sp_spinner_travel_Agent_id = item.getTravel_id();
                                tv_travel_Agent_Name.setText(item.getComp_name()+" ("+item.getTravel_name()+")");
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });







        myFromCalendar = Calendar.getInstance();



        fetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*new DatePickerDialog(mContext, Fromdate, myFromCalendar
                        .get(Calendar.YEAR), myFromCalendar.get(Calendar.MONTH),
                        myFromCalendar.get(Calendar.DAY_OF_MONTH)).show();*/

                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate, myFromCalendar
                        .get(Calendar.YEAR), myFromCalendar.get(Calendar.MONTH),
                        myFromCalendar.get(Calendar.DAY_OF_MONTH));
                mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDate.show();


            }
        });

        Fromdate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis() - 1000);
                myFromCalendar.set(Calendar.YEAR, year);
                myFromCalendar.set(Calendar.MONTH, monthOfYear);
                myFromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateFromDate();
            }
        };


        fetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String period;
                        int hour = 0;
                        String minute;
                        if (selectedMinute >= 0 && selectedMinute < 10)
                            minute = "0" + selectedMinute;
                        else
                            minute = String.valueOf(selectedMinute);
                        if (selectedHour >= 0 && selectedHour < 12)
                            period = "AM";
                        else
                            period = "PM";
                        if (period.equals("AM")) {
                            if (selectedHour == 0)
                                hour = 12;
                            else if (selectedHour >= 1 && selectedHour < 12)
                                hour = selectedHour;
                        } else if (period.equals("PM")) {
                            if (selectedHour == 12)
                                hour = 12;
                            else if (selectedHour >= 13 && selectedHour <= 23)
                                hour = selectedHour - 12;
                        }
                        String time = hour + ":" + minute + " " + period;
                        fetTime.setText(time);
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });


        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {fetDate, fetTime, fetRemark};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }

                if (allValid) {
                    getRequestAddTravelAgent();
                }

            }
        });


        return view;
    }

    private void selectSpinnerValue(Spinner spinner, String myString) {
        /*int index = 0;
        Log.e("spinner getCount", String.valueOf(spinner.getCount()));
        for (int i = 0; i < spinner.getCount(); i++) {
            Log.e("travel id", arrayTravelAgentsList.get(i).getTravel_id());
            if (arrayTravelAgentsList.get(i).getTravel_id().equalsIgnoreCase(myString)) {
                spinner.setSelection(i);
                break;
            }
        }*/
        for (int i = 0; i < arrayTravelAgentsList.size(); i++) {
            Log.e("travel id", arrayTravelAgentsList.get(i).getTravel_id());
            if (arrayTravelAgentsList.get(i).getTravel_id().equalsIgnoreCase(myString)) {
                tv_travel_Agent_Name.setText(arrayTravelAgentsList.get(i).getComp_name()+" ("+arrayTravelAgentsList.get(i).getTravel_name()+")");
               // spinner.setSelection(i);
                break;
            }
        }

    }

    public void getRequestAddTravelAgent() {

        JsonParser jsonParser = new JsonParser();
        JSONObject customerDetails = new JSONObject();
        try {

            customerDetails.put("call_status", sp_spinner_status_call_value);
            customerDetails.put("next_meeting_date", fetDate.getText().toString());
            customerDetails.put("next_meeting_time", fetTime.getText().toString());
            customerDetails.put("remarks", fetRemark.getText().toString());
            customerDetails.put("sales_agent_id", sessionManager.getUserId());
            customerDetails.put("travel_agent_id", sp_spinner_travel_Agent_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonParser.getJsonLoginRequest(mContext, this, customerDetails, "users/meetings");

    }

    @Override
    public void onSuccess(String message) {
        Log.e("", "Add meetings Responce--->" + message);

        try {
            JSONObject jsonObject = new JSONObject(message);
//{"Success":true,"message":"success","status_code":200}
            //JSONObject jsonStatus = jsonObject.getJSONObject("status");
            String msg = jsonObject.getString("message");

            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();


            } else {
                showSnackBar(getActivity(), msg);
            }

        } catch (JSONException e) {
            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String message) {
        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
    }

    private void updateFromDate() {
        // String myFormat = "dd/MM/yy";
        String myFormat = "yyyy-MM-dd";
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        fetDate.setText(sdf.format(myFromCalendar.getTime()).replace("/", ""));
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

    }


    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }

    @Override
    public void onResume() {
        super.onResume();

        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }


}
