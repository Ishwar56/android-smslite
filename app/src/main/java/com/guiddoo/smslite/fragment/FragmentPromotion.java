package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.DepthPageTransformer;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.model.ModelPromotion;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class FragmentPromotion extends Fragment /*implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener */ {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.slider)
    SliderLayout sliderLayout;

    @BindView(R.id.tv_no_record)
    TextView tv_no_record;

    @BindView(R.id.viewpager)
    AutoScrollViewPager viewPager;

   /* @BindView(R.id.tv_start_date)
    TextView tv_start_date;

    @BindView(R.id.tv_end_date)
    TextView tv_end_date;*/

    private Context mContext;
    private String responce, resultObj, message, searchText;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;
    private SessionManager sessionManager;
    private ArrayList<ModelPromotion> ImagesArrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promotion, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        //tv_title.setText("Promotions");
        tv_title.setText(getString(R.string.tab_3));

        getPromotionRequest();

        return view;
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

    }

    public void getPromotionRequest() {
        customResponseDialog.showCustomDialog();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = sessionManager.getBaseUrl() + "users/getpromotions?hash_key=" + sessionManager.getHashkey();
        //final String url = sessionManager.getBaseUrl() + "users/sendOTP?contactNo="+ccp.getSelectedCountryCode()+fetMobile.getText().toString()+"&hashkey="+sessionManager.getHashkey();
        Log.e("getpromotions URL--->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("getpromotions Rep--->", jsonString);
                            customResponseDialog.hideCustomeDialog();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("status");
                                message = status.getString("message");
                                ImagesArrayList = new ArrayList<>();
                                if (status.getString("status_code").equals("200")) {


                                    try {
                                        JSONArray travel_agents = object.getJSONArray("promotional_images");
                                        for (int i = 0; i < travel_agents.length(); i++) {
                                            JSONObject jsonObject1 = travel_agents.getJSONObject(i);
                                            ModelPromotion modelQuery = new ModelPromotion();

                                            modelQuery.setImage_url(jsonObject1.getString("image_url").replace("\\", ""));
                                            // Log.e("URL--->",jsonObject1.getString("image_url").replace("\\",""));
                                            modelQuery.setValidity_start_date(jsonObject1.getString("validity_start_date"));
                                            modelQuery.setValidity_end_date(jsonObject1.getString("validity_end_date"));
                                            modelQuery.setDestination_name(jsonObject1.getString("destination_name"));
                                            modelQuery.setDisplay_order(jsonObject1.getString("display_order"));

                                            ImagesArrayList.add(modelQuery);
                                        }

                                    } catch (NullPointerException e) {
                                        //showSnackBar(getActivity(), getString(R.string.record_not_found));
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        sliderLayout.setVisibility(View.GONE);
                                    }

                                    if (ImagesArrayList.size() > 0) {
                                        tv_no_record.setVisibility(View.GONE);
                                        sliderLayout.setVisibility(View.VISIBLE);


                                        for (int i = 0; i < ImagesArrayList.size(); i++) {
                                            final int finalI = i;
                                            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
                                            defaultSliderView .image(ImagesArrayList.get(i).getImage_url())
                                                    .error(R.drawable.logo_loader)
                                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                                        @Override
                                                        public void onSliderClick(BaseSliderView slider) {
                                                            //Toast.makeText(mContext, ImagesArrayList.get(finalI).getDisplay_order(), Toast.LENGTH_SHORT).show();
                                                            FragmentZoomPromotion bookReview = new FragmentZoomPromotion();
                                                            Bundle bundle=new Bundle();
                                                            bundle.putString("imageURL", slider.getUrl());
                                                            bundle.putString("Display_order", ImagesArrayList.get(finalI).getDisplay_order());
                                                            bookReview.setArguments(bundle);
                                                            changeFragment(R.id.fragment_container, bookReview,true);
                                                        }
                                                    });
                                            sliderLayout.addSlider(defaultSliderView);


                                           /* DefaultSliderView sliderView = new DefaultSliderView(mContext);
                                            final int finalI = i;
                                            sliderView.image(ImagesArrayList.get(i).getImage_url())
                                                    .empty(R.drawable.logo_loader)
                                                    .error(R.drawable.logo_loader)
                                                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                                @Override
                                                public void onSliderClick(BaseSliderView slider) {
                                                    Toast.makeText(mContext, ImagesArrayList.get(finalI).getDisplay_order(), Toast.LENGTH_SHORT).show();
                                                    FragmentZoomPromotion bookReview = new FragmentZoomPromotion();
                                                    Bundle bundle=new Bundle();
                                                    bundle.putString("imageURL", slider.getUrl());
                                                    bundle.putString("Display_order", ImagesArrayList.get(finalI).getDisplay_order());
                                                    bookReview.setArguments(bundle);
                                                    changeFragment(R.id.fragment_container, bookReview,true);
                                                }
                                            });
                                            sliderLayout.addSlider(sliderView);*/
                                        }
                                        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                        sliderLayout.setCustomAnimation(new DescriptionAnimation());
                                        sliderLayout.getPagerIndicator().setVisibility(View.INVISIBLE);
                                        sliderLayout.setDuration(8000);
                                        sliderLayout.movePrevPosition(false);

                                       /* sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                        sliderLayout.setCustomAnimation(new DescriptionAnimation());
                                        sliderLayout.setDuration(8000);*/


                                       /* viewPager.startAutoScroll(4000);
                                        viewPager.setInterval(3000);
                                        viewPager.setAdapter(new PagerAdapter(getActivity().getSupportFragmentManager()));
                                        viewPager.setCycle(true);
                                        viewPager.setBorderAnimation(true);*/
                                        //viewPager.setPageTransformer(true,  new DepthPageTransformer());//DepthPageTransformers //DrawFromBackTransformer


                                    } else {
                                        showSnackBar(getActivity(), message);
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        sliderLayout.setVisibility(View.GONE);
                                    }


                                } else {
                                    showSnackBar(getActivity(), message);
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    sliderLayout.setVisibility(View.GONE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                            tv_no_record.setVisibility(View.VISIBLE);
                            sliderLayout.setVisibility(View.GONE);

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                        tv_no_record.setVisibility(View.VISIBLE);
                        sliderLayout.setVisibility(View.GONE);
                    }
                }
        );

        queue.add(getRequest);


    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        // tab titles
        // private String[] tabTitles = new String[]{"Request", "Fly Mates", "Connection"};

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // overriding getPageTitle()
        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public Fragment getItem(int position) {
            return new FragmentCartItemList().newInstance(ImagesArrayList, position);
        }

        @Override
        public int getCount() {
            return ImagesArrayList.size();
        }

    }


    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);

                    return true;
                }
                return false;
            }
        });
    }


   /* @Override
    public void onSliderClick(BaseSliderView slider) {

        Log.e("Click","--------------->");

        Toast.makeText(mContext,slider.getBundle().get("extra").toString(),Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.e("onPageScrolled","--------------->");
    }

    @Override
    public void onPageSelected(int position) {
        Log.e("onPageSelected","--------------->");
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.e("onPageScrollStateChanged","--------------->");
    }*/
}
