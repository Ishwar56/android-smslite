package com.guiddoo.smslite.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.adapter.AdapterQueryFollowUPList;
import com.guiddoo.smslite.model.QueryFollowupListModel;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.senab.photoview.PhotoViewAttacher;

public class FragmentZoomPromotion extends Fragment{
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.image)
    ImageView iv_image;

    @BindView(R.id.iv_share)
    ImageView iv_share;


    private Context mContext;
    private String[] AppPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private String imageURL, Display_order;
    private GridLayoutManager manager;
    private FragmentManager fragmentManager;
    private boolean FlagPerFlow = false, FlagPermissionFlow = true;
    private PhotoViewAttacher pAttacher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zoom_promotion, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getText(R.string.tab_3));

        imageURL = getArguments().getString("imageURL");
        Display_order = getArguments().getString("Display_order");

        manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
        fragmentManager = getActivity().getSupportFragmentManager();

        pAttacher = new PhotoViewAttacher(iv_image);
        pAttacher.update();


        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        try {
            Glide.with(getActivity())
                    .load(imageURL)
                    .placeholder(R.drawable.logo_loader)
                    .error(R.drawable.logo_loader)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_image);
        } catch (Exception e) {
            Glide.with(getActivity())
                    .load(R.drawable.logo_loader)
                    .placeholder(R.drawable.logo_loader)
                    .error(R.drawable.logo_loader)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_image);
        }


        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Glide.with(getActivity())
                            .load(imageURL)
                            .placeholder(R.drawable.logo_loader)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_image);
                } catch (Exception e) {
                    Glide.with(getActivity())
                            .load(R.drawable.logo_loader)
                            .placeholder(R.drawable.logo_loader)
                            .error(R.drawable.logo_loader)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_image);
                }

                if (checkAppPermissions(mContext, AppPermissions)) {
                    Log.e("Allow------>", "0000000");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pAttacher = new PhotoViewAttacher(iv_image);
                            pAttacher.update();
                            ShareImage();
                        }
                    }, 2000);


                } else if (FlagPermissionFlow) {
                    ActivityCompat.requestPermissions(getActivity(), AppPermissions, 102);
                }

            }

        });


        return view;
    }



    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (FlagPermissionFlow) {
            if (requestCode == 102 && FlagPermissionFlow) {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FlagPermissionFlow = false;
                    Log.e("Allow------>", "0000000");
                    ShareImage();
                } else {
                    Log.e("Denai------>", "111111");

                }
            }

        }

    }

    private void ShareImage() {

        iv_image.buildDrawingCache();
        Bitmap bm = iv_image.getDrawingCache();

        OutputStream fOut = null;
        Uri outputFileUri;

        try {
            File root = new File(Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator);
            root.mkdirs();
            File sdImageMainDirectory = new File(root, Display_order + ".jpg");
            outputFileUri = Uri.fromFile(sdImageMainDirectory);
            fOut = new FileOutputStream(sdImageMainDirectory);
        } catch (Exception e) {
            Toast.makeText(mContext, "Error occured. Please try again later.", Toast.LENGTH_SHORT).show();
        }

        try {
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
        }


        PackageManager pm = getActivity().getPackageManager();


        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("image/*");
            waIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator + Display_order + ".jpg"));
            Log.e("Path-->", Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator + Display_order + ".jpg");
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(mContext, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();

            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/html");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your promotion details are here");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Please find the details attached....");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "SMSLite" + File.separator + Display_order + ".jpg"));
            startActivity(emailIntent);
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                getActivity().finish();
                Log.i("Finished Data", "");
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(mContext, "No way you can share Promotion,enjoy alone", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

    }


    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }


}
