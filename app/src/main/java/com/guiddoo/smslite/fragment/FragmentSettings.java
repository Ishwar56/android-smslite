package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.activity.ActivityOTPVerified;
import com.guiddoo.smslite.application.excursion;
import com.guiddoo.smslite.utils.FontsOverride;
import com.guiddoo.smslite.utils.LocaleHelper;
import com.guiddoo.smslite.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FragmentSettings extends Fragment {

    @BindView(R.id.ll_login)
    LinearLayout ll_login;

    @BindView(R.id.iv_login)
    ImageView iv_login;

    @BindView(R.id.ll_not_login_header)
    LinearLayout ll_not_login_header;

    @BindView(R.id.ll_login_header)
    LinearLayout ll_login_header;

    @BindView(R.id.iv_profile_photo)
    CircleImageView iv_profile_photo;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_mob)
    TextView tv_mob;

    @BindView(R.id.tv_email)
    TextView tv_email;

    @BindView(R.id.tv_log_out)
    TextView tv_log_out;

    @BindView(R.id.ll_log_out)
    LinearLayout ll_log_out;

    @BindView(R.id.ll_Rate_us)
    LinearLayout ll_Rate_us;

    @BindView(R.id.ll_Contact_Us)
    LinearLayout ll_Contact_Us;

    @BindView(R.id.ll_About_Us)
    LinearLayout ll_About_Us;

    @BindView(R.id.ll_Share_App)
    LinearLayout ll_Share_App;


    private Context mContext;
    boolean doubleBackToExitPressedOnce = false,FlagLanguage=false;
    private SessionManager sessionManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        mContext = getContext();
        sessionManager = new SessionManager(mContext);

        FontsOverride.setDefaultFont(mContext, "SERIF", "font/open_sans_regular.ttf");

        tv_name.setText(sessionManager.getUserName());
        tv_email.setText(sessionManager.getUserEmail());
        tv_mob.setText(sessionManager.getUserMobile());
        Log.e("Photo Url --->", sessionManager.getUserPhoto());
        if (sessionManager.getUserPhoto().equals("")) {
            iv_profile_photo.setImageResource(R.drawable.user_white);
        } else {
            Picasso.with(mContext).load(sessionManager.getUserPhoto()).error(R.drawable.user_white).into(iv_profile_photo);
        }

        Log.e("Selected language",sessionManager.getLanguage());


        ll_About_Us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Change_Language();

            }
        });




        ll_log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!sessionManager.getLogin()) {
                    Intent intent = new Intent(getActivity(), ActivityOTPVerified.class);
                    intent.putExtra("Flag", "false");
                    startActivity(intent);
                    getActivity().finish();
                } else {

                    final Dialog dialogMsg = new Dialog(getActivity());
                    dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogMsg.setContentView(R.layout.dialog_login);
                    dialogMsg.setCancelable(false);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialogMsg.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogMsg.getWindow().setAttributes(lp);
                    dialogMsg.show();

                    TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
                    TextView tv_title = (TextView) dialogMsg.findViewById(R.id.tv_title);
                    TextView tv_msg = (TextView) dialogMsg.findViewById(R.id.tv_msg);
                    TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);

                    tv_title.setText(getString(R.string.logout));
                    tv_msg.setText(R.string.logout_text);
                    btn_text.setText(getString(R.string.logout));


                    btn_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.cancel();
                            sessionManager.setLogin(false);
                            //sessionManager.setUserID("0");
                            sessionManager.setUserMobile("");
                            Intent intent = new Intent(getActivity(), ActivityOTPVerified.class);
                            intent.putExtra("Flag", "false");
                            startActivity(intent);
                            getActivity().finish();

                        }
                    });

                    cardViewCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.cancel();
                        }
                    });
                }
            }
        });

        ll_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityOTPVerified.class);
                intent.putExtra("Flag", "false");
                startActivity(intent);
            }
        });

        ll_Rate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.isNetworkAvailable()) {
                    String url = "https://play.google.com/store/apps/details?id=com.guiddoo.smslite";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } else {
                    showSnackBar(getActivity(), "Please check your internet connection.");
                }

            }
        });


        ll_Contact_Us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.isNetworkAvailable()) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", "query@guiddooworld.com", null));
                    //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Query for Booking Id: " + bookingId);
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                } else {
                    showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                }

            }
        });


        ll_Share_App.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.isNetworkAvailable()) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "SMSLite Application");
                        String sAux = "\nLet me recommend you SMSLite Application\n\n";
                        sAux = sAux + "Android link : https://play.google.com/store/apps/details?id=com.guiddoo.smslite \n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                } else {
                    showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                }
            }
        });


        return view;
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(getActivity(), ActivityHomeScreen.class);
        startActivity(refresh);
        getActivity().finish();
    }

    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }


    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (sessionManager.getLogin()) {
            tv_log_out.setText(getString(R.string.logout));
            iv_login.setImageResource(R.drawable.logout);
            ll_login_header.setVisibility(View.VISIBLE);
            ll_not_login_header.setVisibility(View.GONE);
        } else {
            tv_log_out.setText(getString(R.string.login));
            iv_login.setImageResource(R.drawable.login);
            ll_not_login_header.setVisibility(View.VISIBLE);
            ll_login_header.setVisibility(View.GONE);
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);

                    return true;
                }
                return false;
            }
        });
    }


    private void Change_Language() {
        try{

            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.custom_dialog_change_language);

            LinearLayout ll_english = (LinearLayout) dialog.findViewById(R.id.ll_english);
            LinearLayout ll_chinese = (LinearLayout) dialog.findViewById(R.id.ll_chinese);
            final ImageView iv_eng1 = (ImageView)dialog.findViewById(R.id.iv_eng1);
            final ImageView iv_eng2 = (ImageView)dialog.findViewById(R.id.iv_eng2);

            final ImageView iv_ch1 = (ImageView)dialog.findViewById(R.id.iv_ch1);
            final ImageView iv_ch2 = (ImageView)dialog.findViewById(R.id.iv_ch2);

            final TextView tv_eng = (TextView) dialog.findViewById(R.id.tv_eng);
            final TextView tv_ch = (TextView) dialog.findViewById(R.id.tv_ch);
            TextView tv_change_language = (TextView) dialog.findViewById(R.id.tv_change_language);


            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();


            if(sessionManager.getLanguage().equalsIgnoreCase("En")){
                //tv_language.setText("EN");
                tv_eng.setTextColor(Color.parseColor("#324563"));
                tv_ch.setTextColor(Color.parseColor("#A5B8DC"));
                iv_eng1.setImageResource(R.drawable.hotel_intersection_1);
                iv_eng2.setImageResource(R.drawable.hotel_intersection_1);
                iv_ch1.setImageResource(R.drawable.hotel_intersection_brown);
                iv_ch2.setImageResource(R.drawable.hotel_intersection_brown);
            }else{
               // tv_language.setText("CN");
                tv_eng.setTextColor(Color.parseColor("#A5B8DC"));
                tv_ch.setTextColor(Color.parseColor("#324563"));
                iv_eng1.setImageResource(R.drawable.hotel_intersection_brown);
                iv_eng2.setImageResource(R.drawable.hotel_intersection_brown);
                iv_ch1.setImageResource(R.drawable.hotel_intersection_1);
                iv_ch2.setImageResource(R.drawable.hotel_intersection_1);
            }


            ll_english.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FlagLanguage=false;
                    tv_eng.setTextColor(Color.parseColor("#324563"));
                    tv_ch.setTextColor(Color.parseColor("#A5B8DC"));
                    iv_eng1.setImageResource(R.drawable.hotel_intersection_1);
                    iv_eng2.setImageResource(R.drawable.hotel_intersection_1);
                    iv_ch1.setImageResource(R.drawable.hotel_intersection_brown);
                    iv_ch2.setImageResource(R.drawable.hotel_intersection_brown);
                }
            });

            ll_chinese.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FlagLanguage=true;
                    tv_eng.setTextColor(Color.parseColor("#A5B8DC"));
                    tv_ch.setTextColor(Color.parseColor("#324563"));
                    iv_eng1.setImageResource(R.drawable.hotel_intersection_brown);
                    iv_eng2.setImageResource(R.drawable.hotel_intersection_brown);
                    iv_ch1.setImageResource(R.drawable.hotel_intersection_1);
                    iv_ch2.setImageResource(R.drawable.hotel_intersection_1);
                }
            });

            tv_change_language.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(!FlagLanguage){
                        //tv_language.setText("EN");
                        sessionManager.setlanguage("En");
                        LocaleHelper.persist(mContext,"en");
                        setLocale(LocaleHelper.getLanguage(mContext));
                    }else{
                        //tv_language.setText("CN");
                        sessionManager.setlanguage("Cn");
                        LocaleHelper.persist(mContext,"zh");
                        setLocale(LocaleHelper.getLanguage(mContext));
                    }

                }
            });



        }catch (NullPointerException |SecurityException e){

        }


    }

    public void Login() {
        final Dialog dialogMsg = new Dialog(getActivity());
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_login);
        dialogMsg.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView tv_title = (TextView) dialogMsg.findViewById(R.id.tv_title);
        TextView tv_msg = (TextView) dialogMsg.findViewById(R.id.tv_msg);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);

        tv_title.setText(getString(R.string.login));
        tv_msg.setText(R.string.login_text);
        btn_text.setText(getString(R.string.login));


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
                sessionManager.setLogin(false);
                Intent intent = new Intent(getActivity(), ActivityOTPVerified.class);
                intent.putExtra("Flag", "false");
                startActivity(intent);

            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });
    }


}
