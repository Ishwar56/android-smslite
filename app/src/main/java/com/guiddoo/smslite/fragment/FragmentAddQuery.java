package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.adapter.AdapterCountry;
import com.guiddoo.smslite.adapter.AdapterCurrency;
import com.guiddoo.smslite.adapter.AdapterDestination;
import com.guiddoo.smslite.adapter.AdapterExecutive;
import com.guiddoo.smslite.adapter.AdapterStatus;
import com.guiddoo.smslite.adapter.AdapterStatusDetails;
import com.guiddoo.smslite.datamodel.JsonParser;
import com.guiddoo.smslite.model.ModelCountriesList;
import com.guiddoo.smslite.model.ModelDestinationsList;
import com.guiddoo.smslite.model.ModelExecutivesList;
import com.guiddoo.smslite.model.ModelOCStatusList;
import com.guiddoo.smslite.model.ModelSalesAgentsList;
import com.guiddoo.smslite.model.ModelStatusList;
import com.guiddoo.smslite.model.ModelSupplierList;
import com.guiddoo.smslite.model.ModelTravelAgentList;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;
import com.guiddoo.smslite.utils.TaskNotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class FragmentAddQuery extends Fragment implements TaskNotifier {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.tv_query_code)
    FormEditText tv_query_code;

    @BindView(R.id.fetQueryDate)
    FormEditText fetQueryDate;
    @BindView(R.id.fetBookingTravelDate)
    FormEditText fetBookingTravelDate;
    @BindView(R.id.fetNoOfPack)
    FormEditText fetNoOfPack;
    @BindView(R.id.fetAmount)
    FormEditText fetAmount;
    @BindView(R.id.fetMarkUp)
    FormEditText fetMarkUp;
    @BindView(R.id.fetRemark)
    FormEditText fetRemark;

    @BindView(R.id.fetAddQuery)
    FormEditText fetAddQuery;

    @BindView(R.id.tv_Submit)
    TextView tv_Submit;

    @BindView(R.id.sp_spinner_Country)
    Spinner sp_spinner_Country;
    @BindView(R.id.sp_spinner_City)
    Spinner sp_spinner_City;

    @BindView(R.id.sp_spinner_supplier_Name)
    Spinner sp_spinner_supplier_Name;
    @BindView(R.id.ll_supplier_Dialog)
    LinearLayout ll_supplier_Dialog;
    @BindView(R.id.tv_supplier_Name)
    TextView tv_supplier_Name;
    @BindView(R.id.sp_spinner_travel_Agent)
    Spinner sp_spinner_travel_Agent;

    @BindView(R.id.ll_travel_Agent_Name)
    LinearLayout ll_travel_Agent_Name;
    @BindView(R.id.tv_travel_Agent_Name)
    TextView tv_travel_Agent_Name;

    @BindView(R.id.sp_spinner_sales_Agent)
    Spinner sp_spinner_sales_Agent;
    @BindView(R.id.ll_sales_Agent_Name)
    LinearLayout ll_sales_Agent_Name;
    @BindView(R.id.tv_sales_Agent_Name)
    TextView tv_sales_Agent_Name;

    @BindView(R.id.sp_spinner_Executive)
    Spinner sp_spinner_Executive;
    @BindView(R.id.sp_spinner_Currency)
    Spinner sp_spinner_Currency;
    @BindView(R.id.sp_spinner_Status)
    Spinner sp_spinner_Status;
    @BindView(R.id.sp_spinner_detailed_Status)
    Spinner sp_spinner_detailed_Status;

    private Calendar myQueryCalendar,myFromCalendar;
    private DatePickerDialog.OnDateSetListener Fromdate;

    private Context mContext;
    private SessionManager sessionManager;
    private String Booking_ID,sp_spinner_Country_id,sp_spinner_City_id,sp_spinner_supplier_id,sp_spinner_sales_Agent_id,sp_spinner_travel_Agent_id,sp_spinner_Executive_id,sp_spinner_Executive_Code="",sp_spinner_Executive_name,
            HTML_QURY,Travel_Date,Current_Date,Pakcs,sp_spinner_Currency_value,sp_spinner_Status_value,sp_spinner_details_Status_ID,sp_spinner_details_Status_value,responce,resultObj;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;
    private SQLiteHandler db;

    private ArrayList<ModelCountriesList> arrayCountriesList= new ArrayList<>();
    private ArrayList<ModelDestinationsList> arrayDestinationList= new ArrayList<>();
    private ArrayList<ModelSupplierList> arraySupplierList= new ArrayList<>();
    private ArrayList<ModelSalesAgentsList> arraySalesAgentsList= new ArrayList<>();
    private ArrayList<ModelTravelAgentList> arrayTravelAgentsList= new ArrayList<>();
    private ArrayList<ModelExecutivesList> arrayExecutivesList= new ArrayList<>();
    private ArrayList<String>arrayCurrencyList =new ArrayList<>();
    private ArrayList<ModelStatusList> arrayModelStatusList= new ArrayList<>();
    private ArrayList<ModelOCStatusList> arrayModelStatusDetailsList= new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_query, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        db=new SQLiteHandler(mContext);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getText(R.string.add_query));
        Pakcs=fetNoOfPack.getText().toString();
        myFromCalendar = Calendar.getInstance();
        myQueryCalendar = Calendar.getInstance();
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        currentDate();

        fetBookingTravelDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* new DatePickerDialog(mContext, Fromdate, myFromCalendar
                        .get(Calendar.YEAR), myFromCalendar.get(Calendar.MONTH),
                        myFromCalendar.get(Calendar.DAY_OF_MONTH)).show();*/
                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate, myFromCalendar
                        .get(Calendar.YEAR), myFromCalendar.get(Calendar.MONTH),
                        myFromCalendar.get(Calendar.DAY_OF_MONTH));
                mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDate.show();

            }
        });

        Fromdate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myFromCalendar.set(Calendar.YEAR, year);
                myFromCalendar.set(Calendar.MONTH, monthOfYear);
                myFromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateFromDate();
            }

        };


        fetNoOfPack.addTextChangedListener(new GenericTextWatcher(fetNoOfPack));



        arrayCountriesList = db.getCountriesArray();
        if(arrayCountriesList.size()>0&&arrayCountriesList!=null){
            AdapterCountry adapterstate = new AdapterCountry(getActivity(), R.layout.spiner_area_list, R.id.title, arrayCountriesList);
            sp_spinner_Country.setAdapter(adapterstate);
        }

        sp_spinner_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getActivity(),  arrayCountriesList.get(position).getCountry_id(), Toast.LENGTH_SHORT).show();
                sp_spinner_Country_id=arrayCountriesList.get(position).getCountry_id();
                if(arrayCountriesList.size()>0&&arrayCountriesList!=null){
                    arrayDestinationList=db.getCountriesDestinationArray(arrayCountriesList.get(position).getCountry_id());
                    if(arrayDestinationList.size()>0&&arrayDestinationList!=null){
                        AdapterDestination adapterstate = new AdapterDestination(getActivity(), R.layout.spiner_area_list, R.id.title, arrayDestinationList);
                        sp_spinner_City.setAdapter(adapterstate);
                    }

                    arrayExecutivesList = db.getCountriesExecutiveArray(arrayCountriesList.get(position).getCountry_id());
                    if(arrayExecutivesList.size()>0&&arrayExecutivesList!=null){
                        AdapterExecutive adapterstate = new AdapterExecutive(getActivity(), R.layout.spiner_area_list, R.id.title, arrayExecutivesList);
                        sp_spinner_Executive.setAdapter(adapterstate);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_spinner_City.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayDestinationList.size()>0&&arrayDestinationList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_City_id=arrayDestinationList.get(position).getDestination_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arraySupplierList=db.getsuppliersArray();
        if(arraySupplierList.size()>0&&arraySupplierList!=null){
           // AdapterSupplier adapterstate = new AdapterSupplier(getActivity(), R.layout.spiner_area_list, R.id.title, arraySupplierList);
           // sp_spinner_supplier_Name.setAdapter(adapterstate);
            tv_supplier_Name.setText(arraySupplierList.get(0).getSupplier_name());
            sp_spinner_supplier_id=arraySupplierList.get(0).getSupplier_id();
        }

        ll_supplier_Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Supplier Name...",
                        "Search Supplier Name...?", null, arraySupplierList,
                        new SearchResultListener<ModelSupplierList>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelSupplierList item, int position
                            ) {
                                //sp_spinner_supplier_id=arraySupplierList.get(position).getSupplier_id();
                                sp_spinner_supplier_id=item.getSupplier_id();
                                tv_supplier_Name.setText(item.getSupplier_name());
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });




        arraySalesAgentsList=db.getSalesAgentsArray();
        try{
            if(arraySalesAgentsList.size()>0&& arraySalesAgentsList!=null){
                for(int i = 0; i < arraySalesAgentsList.size(); i++){
                    if(arraySalesAgentsList.get(i).getUser_id().equalsIgnoreCase(sessionManager.getUserId())){
                        tv_sales_Agent_Name.setText(arraySalesAgentsList.get(i).getUser_name());
                        sp_spinner_sales_Agent_id=arraySalesAgentsList.get(i).getUser_id();
                        //spinner.setSelection(i);
                        break;
                    }else{
                        tv_sales_Agent_Name.setText(arraySalesAgentsList.get(0).getUser_name());
                        sp_spinner_sales_Agent_id=arraySalesAgentsList.get(0).getUser_id();
                    }
                }
            }


        }catch (NullPointerException e){

        }

        ll_sales_Agent_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Sales Agent",
                        "Search Sales Agent...?", null, arraySalesAgentsList,
                        new SearchResultListener<ModelSalesAgentsList>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelSalesAgentsList item, int position
                            ) {
                                //sp_spinner_sales_Agent_id=arraySalesAgentsList.get(position).getUser_id();
                                sp_spinner_sales_Agent_id=item.getUser_id();
                                tv_sales_Agent_Name.setText(item.getUser_name());
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });




        arrayTravelAgentsList=db.getTravelArray();
        if(arrayTravelAgentsList.size()>0&& arrayTravelAgentsList!=null){
            tv_travel_Agent_Name.setText(arrayTravelAgentsList.get(0).getComp_name()+" ("+arrayTravelAgentsList.get(0).getTravel_name()+")");
            sp_spinner_travel_Agent_id=arrayTravelAgentsList.get(0).getTravel_id();
        }

        ll_travel_Agent_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Travel Agent...",
                        "Search Travel Agent...?", null, arrayTravelAgentsList,
                        new SearchResultListener<ModelTravelAgentList>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelTravelAgentList item, int position
                            ) {
                                //sp_spinner_travel_Agent_id=arrayTravelAgentsList.get(position).getTravel_id();
                                sp_spinner_travel_Agent_id = item.getTravel_id();
                                tv_travel_Agent_Name.setText(item.getComp_name()+" ("+item.getTravel_name()+")");
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });



        sp_spinner_Executive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayExecutivesList.size()>0&&arrayExecutivesList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_Executive_id=arrayExecutivesList.get(position).getUser_id();
                    sp_spinner_Executive_name=arrayExecutivesList.get(position).getUser_name();
                    sp_spinner_Executive_Code=arrayExecutivesList.get(position).getUser_code();
                    int no_of_packs= Integer.parseInt(fetNoOfPack.getText().toString());

                    updateBookCode();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayCurrencyList.add("USD");
        arrayCurrencyList.add("SGD");
        arrayCurrencyList.add("AED");
        arrayCurrencyList.add("INR");
        arrayCurrencyList.add("HKD");
        arrayCurrencyList.add("CNY");
        arrayCurrencyList.add("EGP");
        arrayCurrencyList.add("EUR");
        arrayCurrencyList.add("MYR");
        arrayCurrencyList.add("THB");
        arrayCurrencyList.add("TRY");
        if(arrayCurrencyList.size()>0&&arrayCurrencyList!=null){
            AdapterCurrency adapterstate = new AdapterCurrency(getActivity(), R.layout.spiner_area_list, R.id.title, arrayCurrencyList);
            sp_spinner_Currency.setAdapter(adapterstate);
        }

        sp_spinner_Currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayCurrencyList.size()>0&&arrayCurrencyList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_Currency_value=arrayCurrencyList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayModelStatusList=db.getStatusArray();
        if(arrayModelStatusList.size()>0&&arrayModelStatusList!=null){
            AdapterStatus adapterstate = new AdapterStatus(getActivity(), R.layout.spiner_area_list, R.id.title, arrayModelStatusList);
            sp_spinner_Status.setAdapter(adapterstate);
        }

        sp_spinner_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayModelStatusList.size()>0&&arrayModelStatusList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_Status_value=arrayModelStatusList.get(position).getStatus();
                    arrayModelStatusDetailsList = db.getStatusDetailsArray(sp_spinner_Status_value);
                    if(arrayModelStatusDetailsList.size()>0&&arrayModelStatusDetailsList!=null){
                        AdapterStatusDetails adapterstate = new AdapterStatusDetails(getActivity(), R.layout.spiner_area_list, R.id.title, arrayModelStatusDetailsList);
                        sp_spinner_detailed_Status.setAdapter(adapterstate);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        sp_spinner_detailed_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayModelStatusDetailsList.size()>0&&arrayModelStatusDetailsList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_details_Status_value=arrayModelStatusDetailsList.get(position).getDetailed_status();
                    sp_spinner_details_Status_ID=arrayModelStatusDetailsList.get(position).getDetailed_status_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {fetQueryDate,fetBookingTravelDate,fetNoOfPack,fetAmount,fetMarkUp,fetRemark,fetAddQuery};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }

                if (allValid) {

                    try{
                        if(fetNoOfPack.length()==1){
                            Pakcs="0"+fetNoOfPack.getText().toString();
                        }else{
                            Pakcs=fetNoOfPack.getText().toString();
                        }

                    }catch (NullPointerException|NumberFormatException e){
                        Pakcs="00";
                    }

                    Booking_ID = sessionManager.getAgentCode()+sp_spinner_Executive_Code+"-"+fetQueryDate.getText().toString()+"-"+fetBookingTravelDate.getText().toString()+"-"+Pakcs;
                    Log.e("Booking_ID--->",Booking_ID);

                    SpannableStringBuilder text = (SpannableStringBuilder) fetAddQuery.getText();
                    HTML_QURY = Html.toHtml(text);
                    Log.e("HTML_QURY--->",HTML_QURY);

                    if(fetAmount.getText().toString().equalsIgnoreCase("0")){
                        showSnackBar(getActivity(),getString(R.string.check_amt));
                    }else{
                        getRequestAddQuery();
                    }

                }

            }
        });


        return view;
    }





    private void updateFromDate() {
        String myFormat = "dd/MM/yy";
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        fetBookingTravelDate.setText(sdf.format(myFromCalendar.getTime()).replace("/",""));
       updateBookCode();

        String myFormats = "yyyy-MM-dd";
        final SimpleDateFormat sdfs = new SimpleDateFormat(myFormats, Locale.US);

        Travel_Date = sdfs.format(myQueryCalendar.getTime()).replace("/","");
    }

    private void currentDate() {
        String myFormat = "dd/MM/yy";
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        fetQueryDate.setText(sdf.format(myQueryCalendar.getTime()).replace("/",""));
        fetBookingTravelDate.setText(sdf.format(myQueryCalendar.getTime()).replace("/",""));

       try{
          updateBookCode();

       }catch (NumberFormatException|NullPointerException e){

       }




        String myFormats = "yyyy-MM-dd";
        final SimpleDateFormat sdfs = new SimpleDateFormat(myFormats, Locale.US);

        Current_Date = sdfs.format(myQueryCalendar.getTime()).replace("/","");
        Travel_Date = sdfs.format(myQueryCalendar.getTime()).replace("/","");


    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }

    }

    public void getRequestAddQuery() {

        JsonParser jsonParser = new JsonParser();
        JSONObject customerDetails = new JSONObject();
        try {
            customerDetails.put("amount", new BigDecimal(fetAmount.getText().toString()));
            customerDetails.put("booking_code", Booking_ID);
            customerDetails.put("country_id", sp_spinner_Country_id);
            customerDetails.put("currency", sp_spinner_Currency_value);
            customerDetails.put("destination_id", sp_spinner_City_id);
            customerDetails.put("detailed_status", sp_spinner_details_Status_ID);
            customerDetails.put("executive_id", sp_spinner_Executive_id);
            customerDetails.put("executive_name", sp_spinner_Executive_name);
            customerDetails.put("detailed_status_name", sp_spinner_details_Status_value);
            customerDetails.put("markup", new BigDecimal(fetMarkUp.getText().toString()));
            customerDetails.put("query", HTML_QURY);
            customerDetails.put("query_date", Current_Date);
            customerDetails.put("remarks", fetRemark.getText().toString());
            customerDetails.put("sales_agent_id", sp_spinner_sales_Agent_id);
            customerDetails.put("status", sp_spinner_Status_value);
            customerDetails.put("supplier_id", sp_spinner_supplier_id);
            customerDetails.put("travel_agent_id", sp_spinner_travel_Agent_id);
            customerDetails.put("travel_date", Travel_Date);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonParser.getJsonLoginRequest(mContext, this, customerDetails, "users/addquery");

    }

    @Override
    public void onSuccess(String message) {
        Log.e("", "Add Query Responce--->" + message);

        try {
            JSONObject jsonObject = new JSONObject(message);
//{"Success":true,"message":"success","status_code":200}
            //JSONObject jsonStatus = jsonObject.getJSONObject("status");
            String msg = jsonObject.getString("message");

            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();


            } else {
                showSnackBar(getActivity(), msg);
            }

        } catch (JSONException e) {
            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String message) {
        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
    }



    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }

    @Override
    public void onResume() {
        super.onResume();

        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }


    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

            updateBookCode();

        }
    }

    public void updateBookCode(){
        try{
            if(fetNoOfPack.length()>0){
                if(fetNoOfPack.length()==1){
                    Pakcs="0"+fetNoOfPack.getText().toString();
                }else{
                    Pakcs=fetNoOfPack.getText().toString();
                }
                tv_query_code.setText(sessionManager.getAgentCode()+"-"+fetQueryDate.getText().toString()+"-"+fetBookingTravelDate.getText().toString()+"-"+Pakcs);
            }
        }catch (NumberFormatException|NullPointerException e){
            tv_query_code.setText(sessionManager.getAgentCode()+"-"+fetQueryDate.getText().toString()+"-"+fetBookingTravelDate.getText().toString()+"-"+Pakcs);
        }

    }




}
