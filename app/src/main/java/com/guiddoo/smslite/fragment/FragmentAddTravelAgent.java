package com.guiddoo.smslite.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.activity.ActivityPickUpAddress;
import com.guiddoo.smslite.activity.ActivityViewSingleMapDirection;
import com.guiddoo.smslite.datamodel.JsonParser;
import com.guiddoo.smslite.model.ModelPartnerTravelAgentList;
import com.guiddoo.smslite.utils.Base64;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.GPSTracker;
import com.guiddoo.smslite.utils.ImageFilePath;
import com.guiddoo.smslite.utils.SessionManager;
import com.guiddoo.smslite.utils.TaskNotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FragmentAddTravelAgent extends Fragment implements  OnMapReadyCallback , TaskNotifier {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_edit)
    ImageView tv_edit;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.fetCompany_Name)
    FormEditText fetCompany_Name;

    @BindView(R.id.fetPersone_Name)
    FormEditText fetPersone_Name;

    @BindView(R.id.fetPersone_Email)
    FormEditText fetPersone_Email;

    @BindView(R.id.fetMobile)
    FormEditText fetMobile;

    @BindView(R.id.fetLocation)
    FormEditText fetLocation;

    @BindView(R.id.fetAddress)
    FormEditText fetAddress;

    @BindView(R.id.fetRemark)
    FormEditText fetRemark;

    @BindView(R.id.tv_Upload)
    ImageView uploadImage;
    @BindView(R.id.tv_file_name)
    TextView fileName;
    @BindView(R.id.iv_close)
    ImageView close;
    @BindView(R.id.ll_attachment_path)
    LinearLayout ll_attachment_path;

    @BindView(R.id.ll_map)
    LinearLayout ll_map;

    @BindView(R.id.rg_address)
    RadioGroup rg_address;

    @BindView(R.id.rb_current_address)
    RadioButton rb_current_address;

    @BindView(R.id.rb_map_address)
    RadioButton rb_map_address;

    @BindView(R.id.tv_Submit)
    TextView tv_Submit;

    @BindView(R.id.tv_Update)
    TextView tv_Update;

    private Context mContext;
    private String responce, resultObj;
    private MapView mapView;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;
    SupportMapFragment mMapView;
    public AlertDialog alertMantra;
    Uri imageUri;
    private Bitmap bitmap;
    private static final int PICK_IMAGE = 1;
    private static final int PICK_Camera_IMAGE = 2;
    private boolean flagCammera = false, flagGallery = false;
    private String ORDER_ID;
    private double latitude, longitude;
    private MarkerOptions markerOptions;
    private IconGenerator iconFactory;
    private GPSTracker gpsTracker;
    private GoogleMap googleMap;
    private LatLng sydney, sydney1;
    private SessionManager sessionManager;
    private String[] AppPermissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private String Selected_Plan_ID = "1", travel_Agent_id="",Base64Image = "", Sfilename = "", lati = "", longi = "", Sstate = "",Flow="";
    private ArrayList<ModelPartnerTravelAgentList> liabraryList;
    private int position;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_partner, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        gpsTracker = new GPSTracker(mContext, getActivity());
        gpsTracker.getLocation();
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        tv_title.setText(getText(R.string.add_travel_agent));
        try {
            Flow = getArguments().getString("Flow");
            if(Flow.equalsIgnoreCase("Info")){
                tv_title.setText(getText(R.string.travel_agent_info));
                position = Integer.parseInt(getArguments().getString("Position"));
                liabraryList = (ArrayList<ModelPartnerTravelAgentList>)getArguments().getSerializable("arraylist");
                Log.e("Array Size", String.valueOf(liabraryList.size()));

                 travel_Agent_id = liabraryList.get(position).getTravel_Agent_id();
                 fetAddress.setText(liabraryList.get(position).getAddress());
                 fetCompany_Name.setText(liabraryList.get(position).getCompany_name());
                 fetPersone_Name.setText(liabraryList.get(position).getContact_person());
                 fetPersone_Email.setText(liabraryList.get(position).getEmail_id());
                 sessionManager.setlatitude(liabraryList.get(position).getLatitude());
                 sessionManager.setlogitude(liabraryList.get(position).getLongitude());
                 fetLocation.setText(liabraryList.get(position).getLocation());
                 fetMobile.setText(liabraryList.get(position).getPhone());
                 fetRemark.setText(liabraryList.get(position).getRemarks());

                tv_Submit.setVisibility(View.GONE);
                tv_Update.setVisibility(View.VISIBLE);
                tv_edit.setVisibility(View.VISIBLE);

                fetAddress.setEnabled(false);
                fetCompany_Name.setEnabled(false);
                fetPersone_Name.setEnabled(false);
                fetPersone_Email.setEnabled(false);
                fetLocation.setEnabled(false);
                fetMobile.setEnabled(false);
                fetRemark.setEnabled(false);
                rb_map_address.setChecked(true);
                rg_address.setEnabled(false);
                rb_map_address.setEnabled(false);
                rb_current_address.setEnabled(false);
                rb_current_address.setEnabled(false);




            }else{

                gpsTracker = new GPSTracker(mContext, getActivity());
                gpsTracker.getLocation();
                tv_title.setText(getText(R.string.add_travel_agent));
                rb_current_address.setChecked(true);
                ll_map.setVisibility(View.VISIBLE);
                tv_Submit.setVisibility(View.VISIBLE);
                tv_Update.setVisibility(View.GONE);
                tv_edit.setVisibility(View.INVISIBLE);
                fetAddress.setText("");
                try {
                    fetAddress.setText(gpsTracker.getFullAddress());
                    sessionManager.setlatitude(String.valueOf(gpsTracker.getLatitude()));
                    sessionManager.setlogitude(String.valueOf(gpsTracker.getLongitude()));
                    fetLocation.setText(gpsTracker.getCityName());
                } catch (NullPointerException e1) {
                }

            }

        }catch (NullPointerException e){

            tv_title.setText(getText(R.string.add_travel_agent));

        }


        sessionManager.setpickUpAddress(fetAddress.getText().toString());


        mMapView = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mMapView != null) {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null ) {
                mMapView.getMapAsync(FragmentAddTravelAgent.this);
                //after this you do whatever you want with the map
            } else{
                Log.e("Eroro inside ","---");
            }
        }else{
            Log.e("Eroro","---");
        }

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 fetAddress.setEnabled(true);
                fetCompany_Name.setEnabled(true);
                fetPersone_Name.setEnabled(true);
                fetPersone_Email.setEnabled(true);
                fetLocation.setEnabled(true);
                fetMobile.setEnabled(true);
                fetRemark.setEnabled(true);
                rg_address.setEnabled(true);
                rb_map_address.setEnabled(true);
                rb_current_address.setEnabled(true);
                rb_current_address.setEnabled(true);
            }
        });



        rg_address.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId == R.id.rb_current_address) {
                    ll_map.setVisibility(View.VISIBLE);
                    fetAddress.setText("");
                    try {
                        fetAddress.setText(gpsTracker.getFullAddress());
                        sessionManager.setlatitude(String.valueOf(gpsTracker.getLatitude()));
                        sessionManager.setlogitude(String.valueOf(gpsTracker.getLongitude()));
                    } catch (NullPointerException e) {
                    }

                } else if(checkedId == R.id.rb_map_address) {
                    ll_map.setVisibility(View.GONE);
                    fetAddress.setText("");
                    Intent intent = new Intent(getActivity(), ActivityPickUpAddress.class);
                    startActivity(intent);
                }
            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAppPermissions(mContext, AppPermissions)) {
                    alertBoxSelectPhoto(getActivity());
                } else {
                    ActivityCompat.requestPermissions(getActivity(), AppPermissions, 102);
                }

            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sfilename = "";
                fileName.setText("");
                ll_attachment_path.setVisibility(View.GONE);
            }
        });


        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {fetCompany_Name, fetPersone_Name, fetPersone_Email, fetMobile, fetLocation, fetRemark};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }

                if (allValid) {
                    getRequestAddTravelAgent();

                }

            }
        });

        tv_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {fetCompany_Name, fetPersone_Name, fetPersone_Email, fetMobile, fetLocation, fetRemark};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }

                if (allValid) {
                    updateTravelAgent();
                    //showSnackBar(getActivity(),"Work in progress");

                }

            }
        });


        return view;
    }


    public void alertBoxSelectPhoto(Activity context) {

        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.alert_select_image, null);
        final LinearLayout photo = (LinearLayout) promptsView.findViewById(R.id.alert_take_photo);
        final LinearLayout gallery = (LinearLayout) promptsView.findViewById(R.id.take_from_gallery);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);


        alertDialog.setView(promptsView);


        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fileName = "new-photo-name.jpg";
                //create parameters for Intent with filename
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, fileName);
                values.put(MediaStore.Images.Media.DESCRIPTION, "Image captured by camera");
                //imageUri is the current activity attribute, define and save it for later usage (also in onSaveInstanceState)
                imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                //create new Intent
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                startActivityForResult(intent, PICK_Camera_IMAGE);
                alertMantra.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
 /*Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, PICK_IMAGE);*/
                try {
                    Intent gintent = new Intent();
                    gintent.setType("image/*");
                    gintent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    gintent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(gintent, "Select Picture"),
                            PICK_IMAGE);
                } catch (Exception e) {
                   /* Toast.makeText(getApplicationContext(),
                            e.getMessage(),
                            Toast.LENGTH_LONG).show();*/
                    Log.e("Upload image error---->", e.getMessage(), e);

                }
                alertMantra.dismiss();
            }
        });
        alertMantra = alertDialog.create();
        alertMantra.show();


    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public void decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        bitmap = BitmapFactory.decodeFile(filePath, o2);

        BitmapFactory.Options bfo;
        ByteArrayOutputStream bao;
        bfo = new BitmapFactory.Options();
        bfo.inSampleSize = 2;
        bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        Base64Image = Base64.encodeBytes(ba);
        Log.e("Base64Image", Base64Image);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri selectedImageUri = null;
        String filePath = null;
        switch (requestCode) {

            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    selectedImageUri = data.getData();
                    flagGallery = true;
                }
                break;
            case PICK_Camera_IMAGE:
                if (resultCode == RESULT_OK) {
                    //use imageUri here to access the image
                    selectedImageUri = imageUri;
                    flagCammera = true;
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getActivity(), "Picture was not taken", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Picture was not taken", Toast.LENGTH_SHORT).show();
                }


                break;
        }

        if (selectedImageUri != null) {
            try {

                String filemanagerstring = selectedImageUri.getPath();
                String selectedImagePath = getPath(selectedImageUri);

                if (selectedImagePath != null) {
                    filePath = selectedImagePath;
                } else if (filemanagerstring != null) {
                    filePath = filemanagerstring;
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Unknown path",
                            Toast.LENGTH_LONG).show();
                    Log.e("Bitmap", "Unknown path");
                }
                Log.e("File Path---->", filePath);
                Sfilename = filePath.substring(filePath.lastIndexOf("/") + 1);

                ll_attachment_path.setVisibility(View.VISIBLE);
                fileName.setText(Sfilename);
                if (flagCammera) {
                    decodeFile(filePath);
                    flagCammera = false;
                }
                if (flagGallery) {
                    processGalleryPickedImage(data);
                    flagGallery = false;
                }

            } catch (Exception e) {
                Toast.makeText(getActivity().getApplicationContext(), "Internal error",
                        Toast.LENGTH_LONG).show();
                Log.e("Internal error--->", e.getMessage(), e);
            }
        }

    }

    private void processGalleryPickedImage(Intent intent) {
        if (intent != null) {
            String selectedImagePath = ImageFilePath.getPath(getActivity(), intent.getData());
            Log.e("Gallary Path-->", selectedImagePath);
            fileName.setText(selectedImagePath.substring(selectedImagePath.lastIndexOf("/") + 1));
            try {
                Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath);

                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                byte[] ba = bao.toByteArray();
                Base64Image = android.util.Base64.encodeToString(ba, android.util.Base64.DEFAULT);
                Log.e("Base64Image", Base64Image);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showSnackBar(getActivity(), "Error occurred in selecting image, try again");
            fileName.setText("");
        }
    }

    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 102) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                alertBoxSelectPhoto(getActivity());
            }
        }

    }


    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

    }

    public void getRequestAddTravelAgent() {

        JsonParser jsonParser = new JsonParser();
        JSONObject customerDetails = new JSONObject();
        try {
            customerDetails.put("address", fetAddress.getText().toString());
            customerDetails.put("business", "");
            customerDetails.put("call_status", "");
            customerDetails.put("company_name", fetCompany_Name.getText().toString());
            customerDetails.put("contact_person", fetPersone_Name.getText().toString());
            customerDetails.put("email_id", fetPersone_Email.getText().toString());
            customerDetails.put("follow_up", "");
            customerDetails.put("latitude", sessionManager.getLatitude());
            customerDetails.put("longitude", sessionManager.getLogitude());
            customerDetails.put("location", fetLocation.getText().toString());
            customerDetails.put("phone", fetMobile.getText().toString());
            customerDetails.put("remarks", fetRemark.getText().toString());
            customerDetails.put("travel_agent_id", "0");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonParser.getJsonLoginRequest(mContext, this, customerDetails, "users/addtravelagent");

    }

    public void updateTravelAgent() {

        JsonParser jsonParser = new JsonParser();
        JSONObject customerDetails = new JSONObject();
        try {

            customerDetails.put("address", fetAddress.getText().toString());
            customerDetails.put("business", "");
            customerDetails.put("call_status", "");
            customerDetails.put("company_name", fetCompany_Name.getText().toString());
            customerDetails.put("contact_person", fetPersone_Name.getText().toString());
            customerDetails.put("email_id", fetPersone_Email.getText().toString());
            customerDetails.put("follow_up", "");
            customerDetails.put("latitude", Double.parseDouble(sessionManager.getLatitude()));
            customerDetails.put("longitude", Double.parseDouble(sessionManager.getLogitude()));
            customerDetails.put("location", fetLocation.getText().toString());
            customerDetails.put("phone", fetMobile.getText().toString());
            customerDetails.put("remarks", fetRemark.getText().toString());
            customerDetails.put("travel_Agent_id", Integer.parseInt(travel_Agent_id));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonParser.getJsonLoginRequest(mContext, this, customerDetails, "users/updateTravelAgent");

    }

    @Override
    public void onSuccess(String message) {
        Log.e("", "Add Query Responce--->" + message);

        try {
            JSONObject jsonObject = new JSONObject(message);
//{"Success":true,"message":"success","status_code":200}
            //JSONObject jsonStatus = jsonObject.getJSONObject("status");
            String msg = jsonObject.getString("message");

            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
            } else {
                showSnackBar(getActivity(), msg);
            }

        } catch (JSONException e) {
            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String message) {
        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
    }



    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mMapView.onResume();
        } catch (Exception e) {

        }

        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        fetAddress.setText(sessionManager.getPickupAddress());

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }



    @Override
    public void onMapReady(GoogleMap googlemap) {
        try {
            if(Flow.equalsIgnoreCase("Info")){
                latitude = Double.parseDouble(sessionManager.getLatitude());
                longitude = Double.parseDouble(sessionManager.getLogitude());
            }else{
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
            }

            Log.e("Main Lat---->", String.valueOf(latitude));

            Log.e("Main Long---->", String.valueOf(longitude));

            //mMapView.onCreate();
            googleMap = googlemap;
            mMapView.onResume(); // needed to get the toolbar_layout_map to display immediately
            //final LatLng currentPos = new LatLng(latitude, longitude);

            googleMap.clear();
            iconFactory = new IconGenerator(getActivity());
            sydney = new LatLng(latitude, longitude);
            //String add = getAddress(latitude, longitude);
            iconFactory.setStyle(IconGenerator.STYLE_GREEN);
            markerOptions = new MarkerOptions().title(getString(R.string.my_location)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.client_location)).position(sydney).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.addMarker(markerOptions);


            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    Log.e("centerLat---->", String.valueOf(cameraPosition.target.latitude));

                    Log.e("centerLong---->", String.valueOf(cameraPosition.target.longitude));
                }
            });

            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                @Override
                public void onInfoWindowClick(Marker arg0) {


                }
            });


        } catch (NullPointerException e) {
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            mMapView.onPause();
        } catch (Exception e) {
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    /*@Override
    public void onCameraChange(CameraPosition cameraPosition) {
        mMapView.addMarker( new MarkerOptions()
                .position( position.target )
                .title( position.toString() )
        );
    }*/
}
