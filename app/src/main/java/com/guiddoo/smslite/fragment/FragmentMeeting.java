package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.adapter.AdapterCurrency;
import com.guiddoo.smslite.adapter.AdapterFollowUPList;
import com.guiddoo.smslite.adapter.AdapterYourPartnerList;
import com.guiddoo.smslite.model.FollowUPListModel;
import com.guiddoo.smslite.model.ModelOCStatusList;
import com.guiddoo.smslite.model.ModelPartnerTravelAgentList;
import com.guiddoo.smslite.model.ModelStatusList;
import com.guiddoo.smslite.model.ModelTravelAgentList;
import com.guiddoo.smslite.model.ModelTravelAgentLocationList;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class FragmentMeeting extends Fragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_filter)
    ImageView iv_filter;

    @BindView(R.id.ll_follow_ups)
    LinearLayout ll_follow_ups;

    @BindView(R.id.tv_follow_up)
    TextView tv_follow_up;

    @BindView(R.id.vv_followup)
    View vv_followup;

    @BindView(R.id.rv_follow_ups)
    RecyclerView rv_follow_ups;

    @BindView(R.id.ll_your_partner)
    LinearLayout ll_your_partner;

    @BindView(R.id.tv_your_partner)
    TextView tv_your_partner;

    @BindView(R.id.vv_partner)
    View vv_partner;

    @BindView(R.id.tv_no_record)
    TextView tv_no_record;

    @BindView(R.id.rv_your_partner)
    RecyclerView rv_your_partner;

    private Context mContext;
    private String responce, resultObj, message, searchText;
    private CustomResponseDialog customResponseDialog;
    private SessionManager sessionManager;
    private boolean doubleBackToExitPressedOnce = false, FlagFilter = false, Flag_agent_Name = true, Flag_agent_Location = false, Flag_Meeting = false;
    private ArrayList<FollowUPListModel> followeUpArrayList = new ArrayList<>();
    private ArrayList<ModelPartnerTravelAgentList> yourPatnerArrayList = new ArrayList<>();
    private ArrayList<ModelPartnerTravelAgentList> filteryourPatnerArrayList = new ArrayList<>();
    private ModelPartnerTravelAgentList partnerTravelAgentmodel;
    private FollowUPListModel followUPListModel;
    private AdapterFollowUPList adapterFollowUpList;
    private AdapterYourPartnerList adapterYourPatnerList;
    private SQLiteHandler db;
    private boolean FlagTabSelect = true, FlagFollowupSelect = true, FlagPartnerSelect = true, filterFlag = false;
    SearchView mSearchView;
    MenuItem mSearch;
    public static int sizeofFollowupList, sizeofPartnerupList;
    private Typeface typeBold, typeMedium;
    private String time, sp_spinner_status_call_value, sp_spinner_travel_Agent_id, sp_spinner_comp_name, sp_spinner_locations, sp_spinner_exicutive_name;
    private BottomSheetDialog dialog;
    private ArrayList<String> arrayCurrencyList = new ArrayList<>();
    private ArrayList<ModelTravelAgentList> arrayTravelAgentsList = new ArrayList<>();
    private ArrayList<ModelTravelAgentLocationList> arrayTravelLocation = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meeting, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        db = new SQLiteHandler(mContext);
        dialog = new BottomSheetDialog(mContext);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getString(R.string.tab_1));
        setHasOptionsMenu(true);
        // tv_about_us.setText("Meetings");

        typeBold = ResourcesCompat.getFont(mContext, R.font.roboto_bold);
        typeMedium = ResourcesCompat.getFont(mContext, R.font.avenir_semibold);
        iv_filter.setVisibility(View.INVISIBLE);

        ll_follow_ups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
                }catch (Exception e){
                    Log.e("Inside exception","Bottom bar Up");
                }

                if (FlagFollowupSelect) {
                    getFollowupsRequest();
                    FlagFollowupSelect = false;
                } else {

                }

                iv_filter.setVisibility(View.INVISIBLE);


                if (followeUpArrayList.size() > 0 && followeUpArrayList != null) {
                    tv_follow_up.setText(mContext.getText(R.string.ta_follow_ups) + "(" + followeUpArrayList.size() + ")");
                } else {
                    tv_follow_up.setText(mContext.getText(R.string.ta_follow_ups) + "(0)");
                }

                ll_follow_ups.setBackgroundResource(R.drawable.custom_tab_one_fill);
                ll_your_partner.setBackgroundResource(R.drawable.custom_tab_sec_blank);

                vv_followup.setBackgroundColor(Color.parseColor("#D97D54"));
                vv_partner.setBackgroundColor(Color.parseColor("#E3E7E8"));

                tv_follow_up.setTextColor(Color.parseColor("#324755"));
                tv_your_partner.setTextColor(Color.parseColor("#929292"));

                tv_follow_up.setTypeface(typeBold);
                tv_your_partner.setTypeface(typeMedium);

                rv_follow_ups.setVisibility(View.VISIBLE);
                rv_your_partner.setVisibility(View.GONE);
                FlagTabSelect = true;


            }
        });

        ll_your_partner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
                }catch (Exception e){
                    Log.e("Inside exception","Bottom bar Up");
                }

                if (FlagPartnerSelect) {
                    iv_filter.setVisibility(View.INVISIBLE);
                    getPartnersRequest();
                    FlagPartnerSelect = false;
                } else {
                    iv_filter.setVisibility(View.VISIBLE);
                }


                if (FlagFilter) {
                    callPartnarList(filteryourPatnerArrayList);
                    if (filteryourPatnerArrayList.size() > 0 && filteryourPatnerArrayList != null) {
                        tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(" + filteryourPatnerArrayList.size() + ")");
                    } else {
                        tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(0)");
                    }
                } else {

                    if (yourPatnerArrayList.size() > 0 && yourPatnerArrayList != null) {
                        callPartnarList(yourPatnerArrayList);
                        tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(" + yourPatnerArrayList.size() + ")");
                    } else {
                        tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(0)");
                    }
                }


                ll_follow_ups.setBackgroundResource(R.drawable.custom_tab_sec_blank);
                ll_your_partner.setBackgroundResource(R.drawable.custom_tab_one_fill);

                vv_followup.setBackgroundColor(Color.parseColor("#E3E7E8"));
                vv_partner.setBackgroundColor(Color.parseColor("#D97D54"));

                tv_follow_up.setTextColor(Color.parseColor("#929292"));
                tv_your_partner.setTextColor(Color.parseColor("#324755"));

                tv_follow_up.setTypeface(typeMedium);
                tv_your_partner.setTypeface(typeBold);

                rv_follow_ups.setVisibility(View.GONE);
                rv_your_partner.setVisibility(View.VISIBLE);
                FlagTabSelect = false;


            }
        });

        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (filterFlag) {
                    dialog.setContentView(R.layout.partnar_filter_dialog);

                    dialog.getWindow().setDimAmount(0.0f);

                    dialog.show();


                    final Spinner sp_spinner_travel_Agent = (Spinner) dialog.findViewById(R.id.sp_spinner_travel_Agent);
                    final LinearLayout ll_travel_Agent_Name = (LinearLayout) dialog.findViewById(R.id.ll_travel_Agent_Name);
                    final Spinner sp_spinner_status_call = (Spinner) dialog.findViewById(R.id.sp_spinner_status_call);
                    final TextView tv_travel_Agent_Name = (TextView) dialog.findViewById(R.id.tv_travel_Agent_Name);

                    final LinearLayout ll_travel_Location = (LinearLayout) dialog.findViewById(R.id.ll_travel_Location);
                    final TextView tv_travel_Location = (TextView) dialog.findViewById(R.id.tv_travel_Location);

                    final CheckBox cb_agent_Name = (CheckBox) dialog.findViewById(R.id.cb_agent_Name);
                    final CheckBox cb_agent_Location = (CheckBox) dialog.findViewById(R.id.cb_agent_Location);
                    final CheckBox cb_agent_meet = (CheckBox) dialog.findViewById(R.id.cb_agent_meet);

                    LinearLayout ll_apply_filter = (LinearLayout) dialog.findViewById(R.id.ll_apply_filter);
                    LinearLayout ll_clear_filter = (LinearLayout) dialog.findViewById(R.id.ll_clear_filter);


                    try {

                        if (Flag_agent_Name) {
                            cb_agent_Name.setChecked(true);
                        } else {
                            cb_agent_Name.setChecked(false);
                        }
                        if (Flag_agent_Location) {
                            cb_agent_Location.setChecked(true);
                        } else {
                            cb_agent_Location.setChecked(false);
                        }
                        if (Flag_Meeting) {
                            cb_agent_meet.setChecked(true);
                        } else {
                            cb_agent_meet.setChecked(false);
                        }

                        cb_agent_Name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                                     @Override
                                                                     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                                                         if (isChecked) {
                                                                             cb_agent_Name.setChecked(true);
                                                                             Flag_agent_Name = true;
                                                                         } else {
                                                                             cb_agent_Name.setChecked(false);
                                                                             Flag_agent_Name = false;
                                                                         }
                                                                     }
                                                                 }
                        );

                        cb_agent_Location.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                                         @Override
                                                                         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                                             if (isChecked) {
                                                                                 cb_agent_Location.setChecked(true);
                                                                                 Flag_agent_Location = true;
                                                                             } else {
                                                                                 cb_agent_Location.setChecked(false);
                                                                                 Flag_agent_Location = false;
                                                                             }

                                                                         }
                                                                     }
                        );

                        cb_agent_meet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                                     @Override
                                                                     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                                         if (isChecked) {
                                                                             cb_agent_meet.setChecked(true);
                                                                             Flag_Meeting = true;
                                                                         } else {
                                                                             cb_agent_meet.setChecked(false);
                                                                             Flag_Meeting = false;
                                                                         }

                                                                     }
                                                                 }
                        );


                        arrayTravelAgentsList = new ArrayList<>();
                        arrayTravelLocation = new ArrayList<>();
                        arrayCurrencyList = new ArrayList<>();
                        arrayCurrencyList.add("Cold");
                        arrayCurrencyList.add("Warm");
                        arrayCurrencyList.add("Hot");

                        if (arrayCurrencyList.size() > 0 && arrayCurrencyList != null) {
                            AdapterCurrency adapterstate = new AdapterCurrency(getActivity(), R.layout.spiner_area_list, R.id.title, arrayCurrencyList);
                            sp_spinner_status_call.setAdapter(adapterstate);
                        }

                        sp_spinner_status_call.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (arrayCurrencyList.size() > 0 && arrayCurrencyList != null) {
                                    sp_spinner_status_call_value = arrayCurrencyList.get(position);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        try {
                            arrayTravelAgentsList = db.getTravelArray();
                            if (arrayTravelAgentsList.size() > 0 && arrayTravelAgentsList != null) {
                                tv_travel_Agent_Name.setText(arrayTravelAgentsList.get(0).getComp_name() + " (" + arrayTravelAgentsList.get(0).getTravel_name() + ")");
                                sp_spinner_comp_name = arrayTravelAgentsList.get(0).getComp_name();
                                sp_spinner_exicutive_name = arrayTravelAgentsList.get(0).getTravel_name();
                            }
                        } catch (NullPointerException | SecurityException e) {
                            Log.e("----------->", "Exception 1 -->");
                        }


                        ll_travel_Agent_Name.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                try {
                                    SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Travel Agent...",
                                            "Search Travel Agent...?", null, arrayTravelAgentsList,
                                            new SearchResultListener<ModelTravelAgentList>() {
                                                @Override
                                                public void onSelected(
                                                        BaseSearchDialogCompat dialog,
                                                        ModelTravelAgentList item, int position
                                                ) {
                                                    sp_spinner_travel_Agent_id = item.getTravel_id();
                                                    sp_spinner_comp_name = item.getComp_name();
                                                    sp_spinner_exicutive_name = item.getTravel_name();
                                                    tv_travel_Agent_Name.setText(item.getComp_name() + " (" + item.getTravel_name() + ")");
                                                    dialog.dismiss();
                                                }
                                            }
                                    );

                                    dialog.show();

                                } catch (NullPointerException e) {

                                }


                            }
                        });

                        try {
                            arrayTravelLocation = db.getTravelLocation();
                            if (arrayTravelLocation.size() > 0 && arrayTravelLocation != null) {
                                tv_travel_Location.setText(arrayTravelLocation.get(0).getLocation());
                                sp_spinner_locations = arrayTravelLocation.get(0).getLocation();
                            }
                        } catch (NullPointerException | SecurityException e) {
                            Log.e("----------->", "Exception 1 -->");
                        }

                        ll_travel_Location.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                try {
                                    SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Location...",
                                            "Search Location...?", null, arrayTravelLocation,
                                            new SearchResultListener<ModelTravelAgentLocationList>() {
                                                @Override
                                                public void onSelected(
                                                        BaseSearchDialogCompat dialog,
                                                        ModelTravelAgentLocationList item, int position
                                                ) {
                                                    sp_spinner_locations = item.getLocation();
                                                    tv_travel_Location.setText(item.getLocation());
                                                    dialog.dismiss();
                                                }
                                            }
                                    );
                                    dialog.show();

                                } catch (NullPointerException e) {

                                }


                            }
                        });


                    } catch (NullPointerException e) {
                        dialog.dismiss();
                        showSnackBar(getActivity(), mContext.getString(R.string.data_loading));

                    }


                    ll_apply_filter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            FlagFilter = true;

                            filteryourPatnerArrayList = new ArrayList<>();


                            for (ModelPartnerTravelAgentList wp : yourPatnerArrayList) {
                                if(Flag_agent_Name && Flag_agent_Location && Flag_Meeting){
                                    if (wp.getCall_status().equalsIgnoreCase(sp_spinner_status_call_value.trim()) && wp.getCompany_name().equalsIgnoreCase(sp_spinner_comp_name.trim()) && wp.getContact_person().equalsIgnoreCase(sp_spinner_exicutive_name.trim()) && wp.getLocation().equalsIgnoreCase(sp_spinner_locations.trim())) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(Flag_agent_Name && !Flag_agent_Location && !Flag_Meeting){
                                    if (wp.getCompany_name().equalsIgnoreCase(sp_spinner_comp_name.trim()) && wp.getContact_person().equalsIgnoreCase(sp_spinner_exicutive_name.trim())) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(!Flag_agent_Name && Flag_agent_Location && !Flag_Meeting){
                                    if (wp.getLocation().equalsIgnoreCase(sp_spinner_locations.trim())) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(!Flag_agent_Name && !Flag_agent_Location && Flag_Meeting){
                                    if (wp.getCall_status().equalsIgnoreCase(sp_spinner_status_call_value.trim())) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(Flag_agent_Name && Flag_agent_Location && !Flag_Meeting){
                                    if (wp.getCompany_name().equalsIgnoreCase(sp_spinner_comp_name.trim()) && wp.getContact_person().equalsIgnoreCase(sp_spinner_exicutive_name.trim()) && wp.getLocation().equalsIgnoreCase(sp_spinner_locations.trim())) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(Flag_agent_Name && !Flag_agent_Location && !Flag_Meeting){
                                    if (wp.getCall_status().equalsIgnoreCase(sp_spinner_status_call_value.trim()) && wp.getCompany_name().equalsIgnoreCase(sp_spinner_comp_name.trim()) && wp.getContact_person().equalsIgnoreCase(sp_spinner_exicutive_name.trim()) ) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(!Flag_agent_Name && Flag_agent_Location && Flag_Meeting){
                                    if (wp.getCall_status().equalsIgnoreCase(sp_spinner_status_call_value.trim())&& wp.getLocation().equalsIgnoreCase(sp_spinner_locations.trim()) ) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }else if(Flag_agent_Name && !Flag_agent_Location && Flag_Meeting){
                                    if (wp.getCall_status().equalsIgnoreCase(sp_spinner_status_call_value.trim()) && wp.getCompany_name().equalsIgnoreCase(sp_spinner_comp_name.trim()) && wp.getContact_person().equalsIgnoreCase(sp_spinner_exicutive_name.trim())) {
                                        filteryourPatnerArrayList.add(wp);
                                    }
                                }

                            }


                            callPartnarList(filteryourPatnerArrayList);
                            dialog.dismiss();


                        }
                    });


                    ll_clear_filter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FlagFilter = false;
                            Flag_Meeting=false;
                            Flag_agent_Location=false;
                            Flag_agent_Name=true;
                            callPartnarList(yourPatnerArrayList);
                            dialog.dismiss();

                        }
                    });
                } else {
                    showSnackBar(getActivity(), mContext.getString(R.string.data_loading));
                }


            }
        });

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menus, MenuInflater inflater) {
        try {
            inflater.inflate(R.menu.search_menu, menus);
            mSearch = menus.findItem(R.id.action_search);
            mSearchView = (SearchView) mSearch.getActionView();
            mSearchView.setPadding(0, 8, 0, 8);
            mSearchView.setMaxWidth(Integer.MAX_VALUE);
            View searchplate = (View) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchplate.setBackgroundResource(R.drawable.custombg_white_border);
            //searchplate.setMinimumHeight(40);
            //searchplate.setPadding(0,5,0,5);

        } catch (Exception e) {

        }

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (FlagTabSelect) {
                    searchText = s;
                    adapterFollowUpList.getFilter().filter(searchText);
                } else {
                    searchText = s;
                    adapterYourPatnerList.getFilter().filter(searchText);
                }

                return true;
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                tv_follow_up.setText(mContext.getText(R.string.ta_follow_ups) + "(" + sizeofFollowupList + ")");
                tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(" + sizeofPartnerupList + ")");
                return false;
            }
        });

        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                tv_follow_up.setText(mContext.getText(R.string.ta_follow_ups) + "(" + sizeofFollowupList + ")");
                tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(" + sizeofPartnerupList + ")");
            }
        });


        super.onCreateOptionsMenu(menus, inflater);
    }

    public void getFollowupsRequest() {
        customResponseDialog = new CustomResponseDialog(mContext);
        customResponseDialog.showCustomDialog();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = sessionManager.getBaseUrl() + "users/getFollowups?hash_key=" + sessionManager.getHashkey();
        //final String url = sessionManager.getBaseUrl() + "users/sendOTP?contactNo="+ccp.getSelectedCountryCode()+fetMobile.getText().toString()+"&hashkey="+sessionManager.getHashkey();
        Log.e("getFollowups URL--->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("getFollowups Respon--->", jsonString);
                            customResponseDialog.hideCustomeDialog();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("status");
                                message = status.getString("message");

                                if (status.getString("status_code").equals("200")) {
                                    tv_no_record.setVisibility(View.GONE);
                                    rv_follow_ups.setVisibility(View.VISIBLE);

                                    try {
                                        JSONArray travel_agents = object.getJSONArray("travel_agents");
                                        followeUpArrayList = new ArrayList<>();
                                        for (int i = 0; i < travel_agents.length(); i++) {
                                            JSONObject jsonObject1 = travel_agents.getJSONObject(i);
                                            followUPListModel = new FollowUPListModel();
                                            followUPListModel.setAddress(jsonObject1.getString("address"));
                                            followUPListModel.setBusiness(jsonObject1.getString("business"));
                                            followUPListModel.setCall_status(jsonObject1.getString("call_status"));
                                            followUPListModel.setCompany_name(jsonObject1.getString("company_name"));
                                            followUPListModel.setContact_person(jsonObject1.getString("contact_person"));
                                            followUPListModel.setEmail_id(jsonObject1.getString("email_id"));
                                            followUPListModel.setFollow_up(jsonObject1.getString("follow_up"));
                                            followUPListModel.setLatitude(jsonObject1.getString("latitude"));
                                            followUPListModel.setLongitude(jsonObject1.getString("longitude"));
                                            followUPListModel.setLocation(jsonObject1.getString("location"));
                                            followUPListModel.setPhone(jsonObject1.getString("phone"));
                                            followUPListModel.setRemarks(jsonObject1.getString("remarks"));
                                            followUPListModel.setTravel_Agent_id(jsonObject1.getString("travel_Agent_id"));
                                            followeUpArrayList.add(followUPListModel);

                                        }
                                    } catch (NullPointerException e) {
                                        showSnackBar(getActivity(), "Record not found");
                                    }


                                    if (followeUpArrayList.size() > 0) {

                                        tv_follow_up.setText(mContext.getText(R.string.ta_follow_ups) + "(" + followeUpArrayList.size() + ")");
                                        tv_no_record.setVisibility(View.GONE);
                                        rv_follow_ups.setVisibility(View.VISIBLE);
                                        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        adapterFollowUpList = new AdapterFollowUPList(mContext, followeUpArrayList, getActivity(), fragmentManager);
                                        rv_follow_ups.setLayoutManager(manager);
                                        rv_follow_ups.setAdapter(adapterFollowUpList);
                                    } else {
                                        tv_follow_up.setText(mContext.getText(R.string.ta_follow_ups) + "(0)");
                                        showSnackBar(getActivity(), getString(R.string.record_not_found));
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        rv_follow_ups.setVisibility(View.GONE);
                                    }

                                    try {
                                        //getPartnersRequests();
                                    } catch (NullPointerException e) {

                                    }

                                } else {
                                    showSnackBar(getActivity(), message);
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    rv_follow_ups.setVisibility(View.GONE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                            tv_no_record.setVisibility(View.VISIBLE);
                            rv_follow_ups.setVisibility(View.GONE);
                            try {
                                //getPartnersRequests();
                            } catch (NullPointerException e1) {

                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                        tv_no_record.setVisibility(View.VISIBLE);
                        rv_follow_ups.setVisibility(View.GONE);
                    }
                }
        );

        queue.add(getRequest);


    }

    public void getPartnersRequest() {
        customResponseDialog = new CustomResponseDialog(mContext);
        customResponseDialog.showCustomDialog();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = sessionManager.getBaseUrl() + "users/getPartners?hash_key=" + sessionManager.getHashkey();//422a3933b394432abcfdc805abb18249";
        //final String url = sessionManager.getBaseUrl() + "users/sendOTP?contactNo="+ccp.getSelectedCountryCode()+fetMobile.getText().toString()+"&hashkey="+sessionManager.getHashkey();
        Log.e("getPartners URL--->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("getPartners Respon--->", jsonString);

                            FlagFilter = false;
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("status");
                                message = status.getString("message");

                                if (status.getString("status_code").equals("200")) {
                                    tv_no_record.setVisibility(View.GONE);
                                    try {
                                        JSONArray travel_agents = object.getJSONArray("travel_agents");
                                        yourPatnerArrayList = new ArrayList<>();
                                        db.deleteAlltravels();
                                        for (int i = 0; i < travel_agents.length(); i++) {
                                            JSONObject jsonObject1 = travel_agents.getJSONObject(i);
                                            partnerTravelAgentmodel = new ModelPartnerTravelAgentList();
                                            partnerTravelAgentmodel.setAddress(jsonObject1.getString("address"));
                                            partnerTravelAgentmodel.setBusiness(jsonObject1.getString("business"));
                                            partnerTravelAgentmodel.setCall_status(jsonObject1.getString("call_status"));
                                            partnerTravelAgentmodel.setCompany_name(jsonObject1.getString("company_name"));
                                            partnerTravelAgentmodel.setContact_person(jsonObject1.getString("contact_person"));
                                            partnerTravelAgentmodel.setEmail_id(jsonObject1.getString("email_id"));
                                            partnerTravelAgentmodel.setFollow_up(jsonObject1.getString("follow_up"));
                                            partnerTravelAgentmodel.setLatitude(jsonObject1.getString("latitude"));
                                            partnerTravelAgentmodel.setLongitude(jsonObject1.getString("longitude"));
                                            partnerTravelAgentmodel.setLocation(jsonObject1.getString("location"));
                                            partnerTravelAgentmodel.setPhone(jsonObject1.getString("phone"));
                                            partnerTravelAgentmodel.setRemarks(jsonObject1.getString("remarks"));
                                            partnerTravelAgentmodel.setTravel_Agent_id(jsonObject1.getString("travel_Agent_id"));
                                            yourPatnerArrayList.add(partnerTravelAgentmodel);

                                        }

                                        new LoadData().execute();

                                       /* try {
                                            Runnable runnable = new Runnable() {
                                                @Override
                                                public void run() {
                                                    for (int i = 0; yourPatnerArrayList.size() > i; i++) {
                                                        db.settravelData(yourPatnerArrayList.get(i).getTravel_Agent_id(), yourPatnerArrayList.get(i).getContact_person(), yourPatnerArrayList.get(i).getCompany_name(),yourPatnerArrayList.get(i).getCall_status());
                                                    }
                                                }
                                            };


                                           // AsyncTask.execute(runnable);
                                        } catch (SecurityException | NullPointerException e) {
                                            Log.e("@@@@@@@@@@@@@@","Inside Exception--->");
                                            for (int i = 0; yourPatnerArrayList.size() > i; i++) {
                                                db.settravelData(yourPatnerArrayList.get(i).getTravel_Agent_id(), yourPatnerArrayList.get(i).getContact_person(), yourPatnerArrayList.get(i).getCompany_name(),yourPatnerArrayList.get(i).getCall_status());
                                            }
                                        }
*/

                                    } catch (NullPointerException e) {
                                        showSnackBar(getActivity(), "Record not found");
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        rv_your_partner.setVisibility(View.GONE);
                                    }


                                    callPartnarList(yourPatnerArrayList);


                                } else {
                                    showSnackBar(getActivity(), message);
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    rv_your_partner.setVisibility(View.GONE);
                                }
                                customResponseDialog.hideCustomeDialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                customResponseDialog.hideCustomeDialog();
                            }
                        } catch (Exception e) {
                            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                            tv_no_record.setVisibility(View.VISIBLE);
                            rv_your_partner.setVisibility(View.GONE);
                            customResponseDialog.hideCustomeDialog();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                        tv_no_record.setVisibility(View.VISIBLE);
                        rv_your_partner.setVisibility(View.GONE);
                    }
                }
        );

        queue.add(getRequest);


    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (int i = 0; yourPatnerArrayList.size() > i; i++) {
                    db.settravelData(yourPatnerArrayList.get(i).getTravel_Agent_id(), yourPatnerArrayList.get(i).getContact_person(), yourPatnerArrayList.get(i).getCompany_name(), yourPatnerArrayList.get(i).getCall_status(), yourPatnerArrayList.get(i).getLocation());
                }
            } catch (SecurityException | NullPointerException e) {
                Log.e("@@@@@@@@@@@@@@", "Inside Exception--->");
                for (int i = 0; yourPatnerArrayList.size() > i; i++) {
                    db.settravelData(yourPatnerArrayList.get(i).getTravel_Agent_id(), yourPatnerArrayList.get(i).getContact_person(), yourPatnerArrayList.get(i).getCompany_name(), yourPatnerArrayList.get(i).getCall_status(), yourPatnerArrayList.get(i).getLocation());
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("@@@@@@@@@@@@@@", "Inside onPostExecute--->");
            iv_filter.setVisibility(View.VISIBLE);
            filterFlag = true;
        }
    }

    public void callPartnarList(ArrayList<ModelPartnerTravelAgentList> PatnerArrayList) {
        if (PatnerArrayList.size() > 0) {
            tv_no_record.setVisibility(View.GONE);
            rv_your_partner.setVisibility(View.VISIBLE);
            tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(" + PatnerArrayList.size() + ")");
            GridLayoutManager manager2 = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager2 = getActivity().getSupportFragmentManager();
            adapterYourPatnerList = new AdapterYourPartnerList(mContext, PatnerArrayList, getActivity(), fragmentManager2);
            rv_your_partner.setLayoutManager(manager2);
            rv_your_partner.setAdapter(adapterYourPatnerList);
        } else {
            tv_your_partner.setText(mContext.getText(R.string.your_partner) + "(0)");
            showSnackBar(getActivity(), getString(R.string.record_not_found));
            tv_no_record.setVisibility(View.VISIBLE);
            rv_your_partner.setVisibility(View.GONE);
        }

    }


    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

    }


    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }


    @Override
    public void onResume() {
        super.onResume();

        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        try {
            FlagFollowupSelect = true;
            FlagPartnerSelect = true;

            if (FlagTabSelect) {
                getFollowupsRequest();
                iv_filter.setVisibility(View.INVISIBLE);
                ll_follow_ups.setBackgroundResource(R.drawable.custom_tab_one_fill);
                ll_your_partner.setBackgroundResource(R.drawable.custom_tab_sec_blank);

                vv_followup.setBackgroundColor(Color.parseColor("#D97D54"));
                vv_partner.setBackgroundColor(Color.parseColor("#E3E7E8"));

                tv_follow_up.setTextColor(Color.parseColor("#324755"));
                tv_your_partner.setTextColor(Color.parseColor("#929292"));

                tv_follow_up.setTypeface(typeBold);
                tv_your_partner.setTypeface(typeMedium);

                rv_follow_ups.setVisibility(View.VISIBLE);
                rv_your_partner.setVisibility(View.GONE);
                FlagFollowupSelect = false;
            } else {
                getPartnersRequest();
                iv_filter.setVisibility(View.INVISIBLE);
                ll_follow_ups.setBackgroundResource(R.drawable.custom_tab_sec_blank);
                ll_your_partner.setBackgroundResource(R.drawable.custom_tab_one_fill);

                vv_followup.setBackgroundColor(Color.parseColor("#E3E7E8"));
                vv_partner.setBackgroundColor(Color.parseColor("#D97D54"));

                tv_follow_up.setTextColor(Color.parseColor("#929292"));
                tv_your_partner.setTextColor(Color.parseColor("#324755"));

                tv_follow_up.setTypeface(typeMedium);
                tv_your_partner.setTypeface(typeBold);

                rv_follow_ups.setVisibility(View.GONE);
                rv_your_partner.setVisibility(View.VISIBLE);
                FlagPartnerSelect = false;
            }
        } catch (NullPointerException e) {

        }


        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);

                    return true;
                }
                return false;
            }
        });
    }


}
