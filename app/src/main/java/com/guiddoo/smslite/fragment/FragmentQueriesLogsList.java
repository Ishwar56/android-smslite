package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.adapter.AdapterQueryFollowUPList;
import com.guiddoo.smslite.adapter.AdapterQueryList;
import com.guiddoo.smslite.adapter.AdapterQueryLogsList;
import com.guiddoo.smslite.model.FollowUPListModel;
import com.guiddoo.smslite.model.QueryLogsListModel;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentQueriesLogsList extends Fragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.rv_query)
    RecyclerView rv_query;

    @BindView(R.id.tv_no_record)
    TextView tv_no_record;

    private Context mContext;
    private String responce,resultObj,message,Query_ID;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false;
    private ArrayList<QueryLogsListModel> queryLogsArrayList;
    private AdapterQueryLogsList adapterQueryList;
    private SessionManager sessionManager;
    private GridLayoutManager manager;
    private FragmentManager fragmentManager;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_query_logs_list, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        tv_title.setText(getText(R.string.query_log));
        Query_ID = getArguments().getString("ID");
        getQueryLogRequests();

         manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
         fragmentManager = getActivity().getSupportFragmentManager();

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


        return view;
    }



    public void getQueryLogRequests() {
        customResponseDialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = sessionManager.getBaseUrl() + "users/getquerylogs?hash_key="+sessionManager.getHashkey()+"&query_id="+Query_ID;
        //final String url = sessionManager.getBaseUrl() + "users/sendOTP?contactNo="+ccp.getSelectedCountryCode()+fetMobile.getText().toString()+"&hashkey="+sessionManager.getHashkey();
        Log.e("Querylogs URL--->",url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("Querylogs Respon--->",jsonString);
                            customResponseDialog.hideCustomeDialog();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("data");
                                JSONObject status = object.getJSONObject("status");
                                message = status.getString("message");

                                if(status.getString("status_code").equals("200")){

                                    tv_no_record.setVisibility(View.GONE);
                                    rv_query.setVisibility(View.VISIBLE);

                                    try{
                                        JSONArray travel_agents = object.getJSONArray("logs");

                                        queryLogsArrayList = new ArrayList<>();
                                        for(int i= 0;i<travel_agents.length();i++){
                                            JSONObject jsonObject1 = travel_agents.getJSONObject(i);
                                            QueryLogsListModel listModel = new QueryLogsListModel();
                                            listModel.setCreated_by(jsonObject1.getString("created_by"));
                                            listModel.setLog_title(jsonObject1.getString("log_title"));
                                            queryLogsArrayList.add(listModel);

                                        }
                                    }catch (NullPointerException e){
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        rv_query.setVisibility(View.GONE);
                                        showSnackBar(getActivity(), getString(R.string.record_not_found));
                                    }
                                    if(queryLogsArrayList.size()>0){
                                        adapterQueryList = new AdapterQueryLogsList(mContext, queryLogsArrayList, getActivity(),fragmentManager);
                                        rv_query.setLayoutManager(manager);
                                        rv_query.setAdapter(adapterQueryList);
                                    }else{
                                        tv_no_record.setVisibility(View.VISIBLE);
                                        rv_query.setVisibility(View.GONE);
                                        showSnackBar(getActivity(), getString(R.string.record_not_found));
                                    }

                                }else{
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    rv_query.setVisibility(View.GONE);
                                    showSnackBar(getActivity(), message);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }catch (Exception e){
                            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
                    }
                }
        );

        queue.add(getRequest);


    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }

    }






    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }


    @Override
    public void onResume() {
        super.onResume();

        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    getFragmentManager().popBackStack();
                    return true;
                }
                return false;
            }
        });
    }


}
