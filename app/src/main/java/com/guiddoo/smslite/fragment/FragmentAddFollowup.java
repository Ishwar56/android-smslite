package com.guiddoo.smslite.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.guiddoo.smslite.DatabaseHandler.SQLiteHandler;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.adapter.AdapterCountry;
import com.guiddoo.smslite.adapter.AdapterCurrency;
import com.guiddoo.smslite.adapter.AdapterDestination;
import com.guiddoo.smslite.adapter.AdapterExecutive;
import com.guiddoo.smslite.adapter.AdapterStatus;
import com.guiddoo.smslite.adapter.AdapterStatusDetails;
import com.guiddoo.smslite.datamodel.JsonParser;
import com.guiddoo.smslite.model.ModelCountriesList;
import com.guiddoo.smslite.model.ModelDestinationsList;
import com.guiddoo.smslite.model.ModelExecutivesList;
import com.guiddoo.smslite.model.ModelOCStatusList;
import com.guiddoo.smslite.model.ModelSalesAgentsList;
import com.guiddoo.smslite.model.ModelStatusList;
import com.guiddoo.smslite.model.ModelSupplierList;
import com.guiddoo.smslite.model.ModelTravelAgentList;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;
import com.guiddoo.smslite.utils.TaskNotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class FragmentAddFollowup extends Fragment implements TaskNotifier {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_back)
    ImageView tv_back;

    @BindView(R.id.scroll)
    NestedScrollView scroll;

    @BindView(R.id.tv_query_code)
    FormEditText tv_query_code;

    @BindView(R.id.fetAmount)
    FormEditText fetAmount;

    @BindView(R.id.fetExpected_Amount)
    FormEditText fetExpected_Amount;

    @BindView(R.id.fetMarkUp)
    FormEditText fetMarkUp;

    @BindView(R.id.fetRemark)
    FormEditText fetRemark;

    @BindView(R.id.fetAddQuery)
    FormEditText fetAddQuery;

    @BindView(R.id.tv_Submit)
    TextView tv_Submit;

    @BindView(R.id.fetDate)
    FormEditText fetDate;

    @BindView(R.id.fetTime)
    FormEditText fetTime;

    @BindView(R.id.sp_spinner_followup_type)
    Spinner sp_spinner_followup_type;

    @BindView(R.id.ll_supplier_Dialog)
    LinearLayout ll_supplier_Dialog;

    @BindView(R.id.tv_supplier_Name)
    TextView tv_supplier_Name;

    @BindView(R.id.sp_spinner_Currency)
    Spinner sp_spinner_Currency;

    @BindView(R.id.sp_spinner_Status)
    Spinner sp_spinner_Status;

    @BindView(R.id.sp_spinner_detailed_Status)
    Spinner sp_spinner_detailed_Status;

    @BindView(R.id.cb_confirmation_Guarantee)
    CheckBox cb_confirmation_Guarantee;

    @BindView(R.id.cb_potential_Closure)
    CheckBox cb_potential_Closure;


    private Context mContext;
    private SessionManager sessionManager;
    private String Query_ID,Booking_ID,Booking_Code,Executive_Name,Supplier_Name,sp_spinner_Country_id,sp_spinner_City_id,sp_spinner_supplier_id,sp_spinner_sales_Agent_id,sp_spinner_travel_Agent_id,sp_spinner_Executive_id,sp_spinner_Executive_Code="",sp_spinner_Executive_name,
            HTML_QURY,Travel_Date,Current_Date,sp_spinner_Followuptype_value,sp_spinner_Currency_value,sp_spinner_Status_value,sp_spinner_details_Status_ID,sp_spinner_details_Status_value,responce,resultObj;
    private CustomResponseDialog customResponseDialog;
    private boolean doubleBackToExitPressedOnce = false,Flag_potential_Closure=false,Flagconfirmation_guarantee=false;
    private Calendar myFromCalendar;
    private DatePickerDialog.OnDateSetListener Fromdate;
    private SQLiteHandler db;

    private ArrayList<ModelSupplierList> arraySupplierList= new ArrayList<>();
    private ArrayList<String>arrayCurrencyList =new ArrayList<>();
    private ArrayList<String>arrayFollowupTypeList =new ArrayList<>();
    private ArrayList<ModelStatusList> arrayModelStatusList= new ArrayList<>();
    private ArrayList<ModelOCStatusList> arrayModelStatusDetailsList= new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_followup, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        customResponseDialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        db=new SQLiteHandler(mContext);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        Booking_Code= getArguments().getString("Booking_Code");
        Query_ID= getArguments().getString("Query_ID");
        Executive_Name= getArguments().getString("Executive_Name");
        Supplier_Name= getArguments().getString("Supplier_Name");
        tv_query_code.setText(Booking_Code);
        tv_title.setText(getText(R.string.add_followup));
        myFromCalendar = Calendar.getInstance();

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


        Log.e("Query_ID--->",Query_ID);
        Log.e("Booking_Code--->",Booking_Code);
        Log.e("Executive_Name--->",Executive_Name);
        Log.e("Supplier_Name--->",Supplier_Name);

        arraySupplierList=db.getsuppliersArray();
        if(arraySupplierList.size()>0&&arraySupplierList!=null){
           // AdapterSupplier adapterstate = new AdapterSupplier(getActivity(), R.layout.spiner_area_list, R.id.title, arraySupplierList);
           // sp_spinner_supplier_Name.setAdapter(adapterstate);
            tv_supplier_Name.setText(arraySupplierList.get(0).getSupplier_name());
            sp_spinner_supplier_id=arraySupplierList.get(0).getSupplier_id();
            Supplier_Name=arraySupplierList.get(0).getSupplier_name();

        }




        cb_potential_Closure.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Flag_potential_Closure=true;
                }else{
                    Flag_potential_Closure=false;
                }
            }
        });

        cb_confirmation_Guarantee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Flagconfirmation_guarantee=true;
                }else{
                    Flagconfirmation_guarantee=false;
                }
            }
        });

        fetAmount.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                fetExpected_Amount.setText(s);
            }
        });


        fetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*new DatePickerDialog(mContext, Fromdate, myFromCalendar
                        .get(Calendar.YEAR), myFromCalendar.get(Calendar.MONTH),
                        myFromCalendar.get(Calendar.DAY_OF_MONTH)).show();*/

                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate, myFromCalendar
                        .get(Calendar.YEAR), myFromCalendar.get(Calendar.MONTH),
                        myFromCalendar.get(Calendar.DAY_OF_MONTH));
                mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDate.show();


            }
        });

        Fromdate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis() - 1000);
                myFromCalendar.set(Calendar.YEAR, year);
                myFromCalendar.set(Calendar.MONTH, monthOfYear);
                myFromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateFromDate();
            }
        };


        fetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String period;
                        int hour = 0;
                        String minute;
                        if (selectedMinute >= 0 && selectedMinute < 10)
                            minute = "0" + selectedMinute;
                        else
                            minute = String.valueOf(selectedMinute);
                        if (selectedHour >= 0 && selectedHour < 12)
                            period = "AM";
                        else
                            period = "PM";
                        if (period.equals("AM")) {
                            if (selectedHour == 0)
                                hour = 12;
                            else if (selectedHour >= 1 && selectedHour < 12)
                                hour = selectedHour;
                        } else if (period.equals("PM")) {
                            if (selectedHour == 12)
                                hour = 12;
                            else if (selectedHour >= 13 && selectedHour <= 23)
                                hour = selectedHour - 12;
                        }
                        String time = hour + ":" + minute + " " + period;
                        fetTime.setText(time);
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });




        ll_supplier_Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(getActivity(), "Search Supplier Name...",
                        "Search Supplier Name...?", null, arraySupplierList,
                        new SearchResultListener<ModelSupplierList>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelSupplierList item, int position
                            ) {
                                //sp_spinner_supplier_id=arraySupplierList.get(position).getSupplier_id();
                                sp_spinner_supplier_id=item.getSupplier_id();
                                tv_supplier_Name.setText(item.getSupplier_name());
                                Supplier_Name=item.getSupplier_name();
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });

        arrayFollowupTypeList.add("Email");
        arrayFollowupTypeList.add("Call");
        if(arrayFollowupTypeList.size()>0&&arrayFollowupTypeList!=null){
            AdapterCurrency adapterstate = new AdapterCurrency(getActivity(), R.layout.spiner_area_list, R.id.title, arrayFollowupTypeList);
            sp_spinner_followup_type.setAdapter(adapterstate);
        }

        sp_spinner_followup_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayFollowupTypeList.size()>0&&arrayFollowupTypeList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_Followuptype_value=arrayFollowupTypeList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        arrayCurrencyList.add("USD");
        arrayCurrencyList.add("SGD");
        arrayCurrencyList.add("AED");
        arrayCurrencyList.add("INR");
        arrayCurrencyList.add("HKD");
        arrayCurrencyList.add("CNY");
        arrayCurrencyList.add("EGP");
        arrayCurrencyList.add("EUR");
        arrayCurrencyList.add("MYR");
        arrayCurrencyList.add("THB");
        arrayCurrencyList.add("TRY");
        if(arrayCurrencyList.size()>0&&arrayCurrencyList!=null){
            AdapterCurrency adapterstate = new AdapterCurrency(getActivity(), R.layout.spiner_area_list, R.id.title, arrayCurrencyList);
            sp_spinner_Currency.setAdapter(adapterstate);
        }

        sp_spinner_Currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayCurrencyList.size()>0&&arrayCurrencyList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_Currency_value=arrayCurrencyList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayModelStatusList=db.getStatusArray();
        if(arrayModelStatusList.size()>0&&arrayModelStatusList!=null){
            AdapterStatus adapterstate = new AdapterStatus(getActivity(), R.layout.spiner_area_list, R.id.title, arrayModelStatusList);
            sp_spinner_Status.setAdapter(adapterstate);
        }

        sp_spinner_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayModelStatusList.size()>0&&arrayModelStatusList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_Status_value=arrayModelStatusList.get(position).getStatus();
                    arrayModelStatusDetailsList = db.getStatusDetailsArray(sp_spinner_Status_value);
                    if(arrayModelStatusDetailsList.size()>0&&arrayModelStatusDetailsList!=null){
                        AdapterStatusDetails adapterstate = new AdapterStatusDetails(getActivity(), R.layout.spiner_area_list, R.id.title, arrayModelStatusDetailsList);
                        sp_spinner_detailed_Status.setAdapter(adapterstate);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        sp_spinner_detailed_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayModelStatusDetailsList.size()>0&&arrayModelStatusDetailsList!=null){
                    // Toast.makeText(getActivity(),  arrayDestinationList.get(position).getDestination_id(), Toast.LENGTH_SHORT).show();
                    sp_spinner_details_Status_value=arrayModelStatusDetailsList.get(position).getDetailed_status();
                    sp_spinner_details_Status_ID=arrayModelStatusDetailsList.get(position).getDetailed_status_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {tv_query_code,fetAmount,fetExpected_Amount,fetMarkUp,fetRemark,fetAddQuery};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }

                if (allValid) {

                    SpannableStringBuilder text = (SpannableStringBuilder) fetAddQuery.getText();
                    HTML_QURY = Html.toHtml(text);
                    Log.e("HTML_QURY--->",HTML_QURY);

                    if(fetAmount.getText().toString().equalsIgnoreCase("0")){
                        showSnackBar(getActivity(),getString(R.string.check_amt));
                    }else{
                        getRequestAddQuery();
                        //showSnackBar(getActivity(),"Add followup success");
                    }

                }

            }
        });


        return view;
    }




    private void updateFromDate() {
        // String myFormat = "dd/MM/yy";
        String myFormat = "yyyy-MM-dd";
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        fetDate.setText(sdf.format(myFromCalendar.getTime()).replace("/", ""));
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }

    }

    public void getRequestAddQuery() {

        JsonParser jsonParser = new JsonParser();
        JSONObject customerDetails = new JSONObject();
        try {

            customerDetails.put("query_id", Query_ID);
            customerDetails.put("booking_code", Booking_Code);
            customerDetails.put("currency", sp_spinner_Currency_value);
            customerDetails.put("amount", new BigDecimal(fetAmount.getText().toString()));
            customerDetails.put("executive_name", Executive_Name);
            customerDetails.put("suppliername", Supplier_Name);
            customerDetails.put("remarks", fetRemark.getText().toString());
            customerDetails.put("query", HTML_QURY);
            customerDetails.put("followup_type",sp_spinner_Followuptype_value);
            customerDetails.put("status",sp_spinner_Status_value);
            customerDetails.put("detailed_status_id", sp_spinner_details_Status_ID);
            customerDetails.put("detailed_status_name", sp_spinner_details_Status_value);
            customerDetails.put("markup", new BigDecimal(fetMarkUp.getText().toString()));
            customerDetails.put("next_followup_date", fetDate.getText().toString());
            customerDetails.put("potential_closure", Flag_potential_Closure);
            customerDetails.put("confirmation_guarantee", Flagconfirmation_guarantee);//
            customerDetails.put("expected_amount", fetExpected_Amount.getText().toString());



        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("All details", String.valueOf(customerDetails.toString()));
        jsonParser.getJsonLoginRequest(mContext, this, customerDetails, "users/addfollowup");

    }

    @Override
    public void onSuccess(String message) {
        Log.e("", "Add Query Responce--->" + message);

        try {
            JSONObject jsonObject = new JSONObject(message);
            String msg = jsonObject.getString("message");

            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();

            } else {
                showSnackBar(getActivity(), msg);
            }

        } catch (JSONException e) {
            showSnackBar(getActivity(), getString(R.string.something_went_wrong));
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String message) {
        showSnackBar(getActivity(), getString(R.string.something_went_wrong));
    }



    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }

    @Override
    public void onResume() {
        super.onResume();

        try{
            ((ActivityHomeScreen)getActivity()).closeSubMenusFab();
        }catch (Exception e){
            Log.e("Inside exception","Bottom bar Up");
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }


}
