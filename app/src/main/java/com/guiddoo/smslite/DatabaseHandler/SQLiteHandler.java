package com.guiddoo.smslite.DatabaseHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.guiddoo.smslite.model.ModelDestinationsList;
import com.guiddoo.smslite.model.ModelExecutivesList;
import com.guiddoo.smslite.model.ModelCountriesList;
import com.guiddoo.smslite.model.ModelOCStatusList;
import com.guiddoo.smslite.model.ModelSalesAgentsList;
import com.guiddoo.smslite.model.ModelStatusList;
import com.guiddoo.smslite.model.ModelSupplierList;
import com.guiddoo.smslite.model.ModelTravelAgentList;
import com.guiddoo.smslite.model.ModelTravelAgentLocationList;
import com.guiddoo.smslite.model.QueryListModel;
import com.guiddoo.smslite.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Ishwar on 01-09-2018.
 */

public class SQLiteHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLiteHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "SMSLite";
    private SessionManager session;

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        session = new SessionManager(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            String countries_table,sales_agents_table,statuses_table,suppliers_table,travel_table,query_table;

            countries_table = "CREATE TABLE countries (country_id TEXT NOT NULL ,country_name TEXT NOT NULL ,destinations Text NOT NULL,executives Text NOT NULL)";
            db.execSQL(countries_table);
            Log.d(TAG, "countries table is created");

            sales_agents_table = "CREATE TABLE sales_agents (user_code TEXT NOT NULL ,user_id TEXT NOT NULL ,user_name Text NOT NULL)";
            db.execSQL(sales_agents_table);
            Log.d(TAG, "sales_agents table is created");

            statuses_table = "CREATE TABLE statuses (status TEXT NOT NULL ,detailed_statuses TEXT NOT NULL)";
            db.execSQL(statuses_table);
            Log.d(TAG, "statuses table is created");

            suppliers_table = "CREATE TABLE suppliers (supplier_id TEXT NOT NULL ,supplier_name TEXT NOT NULL)";
            db.execSQL(suppliers_table);
            Log.d(TAG, "suppliers table is created");

            travel_table = "CREATE TABLE travel (travel_id TEXT NOT NULL ,travel_name TEXT NOT NULL ,com_name TEXT NOT NULL,call_status TEXT NOT NULL,Location Text)";
            db.execSQL(travel_table);
            Log.d(TAG, "travel table is created");

            query_table = "CREATE TABLE querys (query_id TEXT NOT NULL ,amount TEXT NOT NULL ,booking_code TEXT NOT NULL,confirmation_guarantee TEXT NOT NULL,currency TEXT NOT NULL,destination TEXT NOT NULL,detailed_status TEXT NOT NULL" +
                    ",executive_name TEXT NOT NULL,markup TEXT NOT NULL,status TEXT NOT NULL,supplier TEXT NOT NULL,travel_agent TEXT NOT NULL,travel_agent_phone TEXT NOT NULL)";
            db.execSQL(query_table);
            Log.d(TAG, "query table is created");
        }catch (Exception e1){

        }


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        session.setLogin(false);
        db.execSQL("DROP TABLE IF EXISTS countries");
        db.execSQL("DROP TABLE IF EXISTS sales_agents");
        db.execSQL("DROP TABLE IF EXISTS statuses");
        db.execSQL("DROP TABLE IF EXISTS suppliers");
        db.execSQL("DROP TABLE IF EXISTS travel");
        onCreate(db);
    }



    /*---------------------countries-----------------------------------------*/

//            countries_table = "CREATE TABLE countries (country_id TEXT NOT NULL ,country_name TEXT NOT NULL ,destinations Text NOT NULL,executives Text NOT NULL)";
    public boolean setCountriesData(String country_id,String country_name, String destinations,String executives){
        try{
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!country_id.isEmpty()&&!country_name.isEmpty()){
                ContentValues values = new ContentValues();
                values.put("country_id",country_id);
                values.put("country_name",country_name);
                values.put("destinations",destinations);
                values.put("executives",executives);
                long id = db.insert("countries", null,values);
                db.close();
                values.clear();
                Log.e(TAG, "Insert Data in countries table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        }catch (Exception e1){
            return false;
        }

    }


    public ArrayList<ModelCountriesList> getCountriesArray(){
        try{
            String selectQuery = "SELECT * FROM countries"; //WHERE country_id='"+country_id+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelCountriesList> result = new ArrayList<ModelCountriesList>();
            ArrayList<ModelDestinationsList> modelDestinationsList = new ArrayList<>();
            ArrayList<ModelExecutivesList> modelExecutivesList = new ArrayList<>();
            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                ModelCountriesList listModel = new ModelCountriesList();
                listModel.setCountry_id(c.getString(c.getColumnIndex("country_id")));
               listModel.setCountry_name(c.getString(c.getColumnIndex("country_name")));



                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }

    public ArrayList<ModelDestinationsList> getCountriesDestinationArray(String country_id){
        try{
            String selectQuery = "SELECT * FROM countries WHERE country_id='"+country_id+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);


            ArrayList<ModelDestinationsList> modelDestinationsList = new ArrayList<>();
            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){

                try{
                    JSONArray Destinationsarray = new JSONArray(c.getString(c.getColumnIndex("destinations")));
                    for(i = 0;i<Destinationsarray.length();i++){
                        ModelDestinationsList destinationsList = new ModelDestinationsList();
                        JSONObject jsonObject = Destinationsarray.getJSONObject(i);
                        destinationsList.setDestination_id(jsonObject.getString("destination_id"));
                        destinationsList.setDestination_name(jsonObject.getString("destination"));
                        modelDestinationsList.add(destinationsList);
                    }
                }catch (NullPointerException | JSONException e){
                }

                c.moveToNext();
            }

            return modelDestinationsList;
        }catch (Exception e1){
            return null;
        }


    }

    public ArrayList<ModelExecutivesList> getCountriesExecutiveArray(String country_id){
        try{
            String selectQuery = "SELECT * FROM countries WHERE country_id='"+country_id+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelExecutivesList> modelExecutivesList = new ArrayList<>();
            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                try{
                    JSONArray Executivesarray = new JSONArray(c.getString(c.getColumnIndex("executives")));
                    for(i = 0;i<Executivesarray.length();i++){
                        ModelExecutivesList executivesList = new ModelExecutivesList();
                        JSONObject jsonObject = Executivesarray.getJSONObject(i);
                        executivesList.setUser_code(jsonObject.getString("user_code"));
                        executivesList.setUser_id(jsonObject.getString("user_id"));
                        executivesList.setUser_name(jsonObject.getString("user_name"));
                        modelExecutivesList.add(executivesList);
                    }
                }catch (NullPointerException | JSONException e){
                }

                c.moveToNext();
            }

            return modelExecutivesList;
        }catch (Exception e1){
            return null;
        }


    }




    public boolean deleteAllCountries()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e(TAG, "Remove countries table ");
        return db.delete("countries", null, null) > 0;
    }

    /*---------------------sales_agents-----------------------------------------*/

    //            sales_agents_table = "CREATE TABLE sales_agents (user_code TEXT NOT NULL ,user_id TEXT NOT NULL ,user_name Text NOT NULL)";

    public boolean setSalesAgentsData(String user_code,String user_id, String user_name){
        try{
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!user_code.isEmpty()&&!user_id.isEmpty()){
                ContentValues values = new ContentValues();
                values.put("user_code",user_code);
                values.put("user_id",user_id);
                values.put("user_name",user_name);
                long id = db.insert("sales_agents", null,values);
                db.close();
                values.clear();
                Log.e(TAG, "Insert Data in sales_agents table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        }catch (Exception e1){
            return false;
        }

    }


    public ArrayList<ModelSalesAgentsList> getSalesAgentsArray(){
        try{
            String selectQuery = "SELECT * FROM sales_agents"; //WHERE country_id='"+country_id+"'"
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelSalesAgentsList> result = new ArrayList<ModelSalesAgentsList>();

            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                ModelSalesAgentsList listModel = new ModelSalesAgentsList();
                listModel.setUser_code(c.getString(c.getColumnIndex("user_code")));
                listModel.setUser_id(c.getString(c.getColumnIndex("user_id")));
                listModel.setUser_name(c.getString(c.getColumnIndex("user_name")));
                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }

    public boolean deleteAllSalesAgents()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e(TAG, "Remove sales_agents table ");
        return db.delete("sales_agents", null, null) > 0;
    }



    /*---------------------Status-----------------------------------------*/
//            statuses_table = "CREATE TABLE statuses (status TEXT NOT NULL ,detailed_statuses TEXT NOT NULL)";
    public boolean setStatusData(String status,String detailed_statuses){
        try{
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!status.isEmpty()&&!detailed_statuses.isEmpty()){
                ContentValues values = new ContentValues();
                values.put("status",status);
                values.put("detailed_statuses",detailed_statuses);
                long id = db.insert("statuses", null,values);
                db.close();
                values.clear();
                Log.e(TAG, "Insert Data in statuses table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        }catch (Exception e1){
            return false;
        }

    }


    public ArrayList<ModelStatusList> getStatusArray(){// Closed,Open
        try{
            String selectQuery = "SELECT * FROM statuses";// WHERE status='"+status+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelStatusList> result = new ArrayList<ModelStatusList>();
            ArrayList<ModelOCStatusList> modelDestinationsList = new ArrayList<>();
            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                ModelStatusList listModel = new ModelStatusList();
                listModel.setStatus(c.getString(c.getColumnIndex("status")));

                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }
    public ArrayList<ModelOCStatusList> getStatusDetailsArray(String status){// Closed,Open
        try{
            String selectQuery = "SELECT * FROM statuses WHERE status='"+status+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelOCStatusList> modelDestinationsList = new ArrayList<>();
            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){

                try{
                    JSONArray Destinationsarray = new JSONArray(c.getString(c.getColumnIndex("detailed_statuses")));
                    for(i = 0;i<Destinationsarray.length();i++){
                        ModelOCStatusList destinationsList = new ModelOCStatusList();
                        JSONObject jsonObject = Destinationsarray.getJSONObject(i);
                        destinationsList.setDetailed_status_id(jsonObject.getString("detailed_status_id"));
                        destinationsList.setDetailed_status(jsonObject.getString("detailed_status"));
                        modelDestinationsList.add(destinationsList);
                    }

                }catch (NullPointerException | JSONException e){

                }

                c.moveToNext();
            }

            return modelDestinationsList;
        }catch (Exception e1){
            return null;
        }


    }


    public boolean deleteAllStatus()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e(TAG, "Remove statuses table ");
        return db.delete("statuses", null, null) > 0;
    }


    /*---------------------suppliers-----------------------------------------*/
//            suppliers_table = "CREATE TABLE suppliers (supplier_id TEXT NOT NULL ,supplier_name TEXT NOT NULL)";

    public boolean setsuppliersData(String supplier_id,String supplier_name){
        try{
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!supplier_id.isEmpty()&&!supplier_name.isEmpty()){
                ContentValues values = new ContentValues();
                values.put("supplier_id",supplier_id);
                values.put("supplier_name",supplier_name);
                long id = db.insert("suppliers", null,values);
                db.close();
                values.clear();
                Log.e(TAG, "Insert Data in suppliers table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        }catch (Exception e1){
            return false;
        }

    }


    public ArrayList<ModelSupplierList> getsuppliersArray(){
        try{
            String selectQuery = "SELECT * FROM suppliers"; //WHERE country_id='"+country_id+"'"
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelSupplierList> result = new ArrayList<ModelSupplierList>();

            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                ModelSupplierList listModel = new ModelSupplierList();
                listModel.setSupplier_id(c.getString(c.getColumnIndex("supplier_id")));
                listModel.setSupplier_name(c.getString(c.getColumnIndex("supplier_name")));
                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }

    public boolean deleteAllSuppliers()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e(TAG, "Remove suppliers table ");
        return db.delete("suppliers", null, null) > 0;
    }

    /*---------------------Travel-----------------------------------------*/

//            travel_table = "CREATE TABLE travel (travel_id TEXT NOT NULL ,travel_name TEXT NOT NULL,com_name TEXT NOT NULL)";

    /*                                      partnerTravelAgentmodel.setAddress(jsonObject1.getString("address"));
                                            partnerTravelAgentmodel.setBusiness(jsonObject1.getString("business"));
                                            partnerTravelAgentmodel.setCall_status(jsonObject1.getString("call_status"));
                                            partnerTravelAgentmodel.setCompany_name(jsonObject1.getString("company_name"));
                                            partnerTravelAgentmodel.setContact_person(jsonObject1.getString("contact_person"));
                                            partnerTravelAgentmodel.setEmail_id(jsonObject1.getString("email_id"));
                                            partnerTravelAgentmodel.setFollow_up(jsonObject1.getString("follow_up"));
                                            partnerTravelAgentmodel.setLatitude(jsonObject1.getString("latitude"));
                                            partnerTravelAgentmodel.setLongitude(jsonObject1.getString("longitude"));
                                            partnerTravelAgentmodel.setLocation(jsonObject1.getString("location"));
                                            partnerTravelAgentmodel.setPhone(jsonObject1.getString("phone"));
                                            partnerTravelAgentmodel.setRemarks(jsonObject1.getString("remarks"));
                                            partnerTravelAgentmodel.setTravel_Agent_id(jsonObject1.getString("travel_Agent_id")); */

    public boolean settravelData(String travel_id,String travel_name,String compony_name,String call_status,String Location){
        try{
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!travel_id.isEmpty()&&!travel_name.isEmpty()){
                ContentValues values = new ContentValues();
                values.put("travel_id",travel_id);
                values.put("travel_name",travel_name);
                values.put("com_name",compony_name);
                values.put("call_status",call_status);
                values.put("Location",Location);
                long id = db.insert("travel", null,values);
                db.close();
                values.clear();
                Log.e(TAG, "Insert Data in travel table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        }catch (Exception e1){
            return false;
        }

    }


    public ArrayList<ModelTravelAgentList> getTravelArray(){
        try{
            String selectQuery = "SELECT * FROM travel"; //WHERE country_id='"+country_id+"'"
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelTravelAgentList> result = new ArrayList<ModelTravelAgentList>();

            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                ModelTravelAgentList listModel = new ModelTravelAgentList();
                listModel.setTravel_id(c.getString(c.getColumnIndex("travel_id")));
                listModel.setTravel_name(c.getString(c.getColumnIndex("travel_name")));
                listModel.setComp_name(c.getString(c.getColumnIndex("com_name")));
                listModel.setCall_status(c.getString(c.getColumnIndex("call_status")));
                listModel.setLocation(c.getString(c.getColumnIndex("Location")));
                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }



    public ArrayList<ModelTravelAgentLocationList> getTravelLocation(){
        try{
            String selectQuery = "SELECT distinct Location FROM travel"; //WHERE country_id='"+country_id+"'"
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<ModelTravelAgentLocationList> result = new ArrayList<ModelTravelAgentLocationList>();

            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                ModelTravelAgentLocationList listModel = new ModelTravelAgentLocationList();
                listModel.setLocation(c.getString(c.getColumnIndex("Location")));
                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }

    public boolean deleteAlltravels()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e(TAG, "Remove travel table ");
        return db.delete("travel", null, null) > 0;
    }


    /*---------------------Query-----------------------------------------*/

/*query_table = "CREATE TABLE querys (query_id TEXT NOT NULL ,amount TEXT NOT NULL ,booking_code TEXT NOT NULL,
confirmation_guarantee TEXT NOT NULL,currency TEXT NOT NULL,destination TEXT NOT NULL,detailed_status TEXT NOT NULL" +
 ",executive_name TEXT NOT NULL,markup TEXT NOT NULL,markup TEXT NOT NULL,status TEXT NOT NULL,supplier TEXT NOT NULL,
 travel_agent TEXT NOT NULL,travel_agent_phone TEXT NOT NULL)";
           */

    public boolean setQueryData(String query_id,String amount,String booking_code,
                                String confirmation_guarantee,String currency,String destination,
                                String detailed_status,String executive_name,String markup,
                                String status,String supplier,String travel_agent,String travel_agent_phone){
        try{
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!query_id.isEmpty()&&!amount.isEmpty()&&!booking_code.isEmpty()){
                ContentValues values = new ContentValues();
                values.put("query_id",query_id);
                values.put("amount",amount);
                values.put("booking_code",booking_code);
                values.put("confirmation_guarantee",confirmation_guarantee);
                values.put("currency",currency);
                values.put("destination",destination);
                values.put("detailed_status",detailed_status);
                values.put("executive_name",executive_name);
                values.put("markup",markup);
                values.put("status",status);
                values.put("supplier",supplier);
                values.put("travel_agent",travel_agent);
                values.put("travel_agent_phone",travel_agent_phone);

                long id = db.insert("querys", null,values);
                db.close();
                values.clear();
                Log.e(TAG, "Insert Data in querys table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        }catch (Exception e1){
            return false;
        }

    }

    /*Filter query list*/

    public ArrayList<QueryListModel>getQueryStatusArray(String Status,String Detail_Status,String Destiation,boolean Flag_Status,boolean Flag_Details_Status,boolean Flag_City){
        try{
            String selectQuery="";
            if(Flag_Status && !Flag_Details_Status && !Flag_City){
                 selectQuery = "SELECT * FROM querys WHERE status= '"+Status+"'  ";
            }else if(!Flag_Status && Flag_Details_Status && !Flag_City){
                 selectQuery = "SELECT * FROM querys WHERE  detailed_status = '"+Detail_Status+"'";
            }else if(!Flag_Status && !Flag_Details_Status && Flag_City){
                 selectQuery = "SELECT * FROM querys WHERE destination = '"+Destiation+"' ";
            }else if(Flag_Status && Flag_Details_Status && !Flag_City){
                selectQuery = "SELECT * FROM querys WHERE status= '"+Status+"' AND detailed_status = '"+Detail_Status+"' ";
            }else if(!Flag_Status && Flag_Details_Status && Flag_City){
                selectQuery = "SELECT * FROM querys WHERE detailed_status = '"+Detail_Status+"' AND destination = '"+Destiation+"' ";
            }else if(Flag_Status && !Flag_Details_Status && Flag_City){
                selectQuery = "SELECT * FROM querys WHERE status= '"+Status+"' AND destination = '"+Destiation+"' ";
            }else if(Flag_Status && Flag_Details_Status && Flag_City){
                 selectQuery = "SELECT * FROM querys WHERE status= '"+Status+"' AND detailed_status = '"+Detail_Status+"' AND destination = '"+Destiation+"' ";
            }

            Log.e("Filter Query",selectQuery);


            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            ArrayList<QueryListModel> result = new ArrayList<QueryListModel>();

            c.moveToFirst();
            for(int i = 0; i < c.getCount(); i++){
                QueryListModel listModel = new QueryListModel();

                listModel.setQuery_id(c.getString(c.getColumnIndex("query_id")));
                listModel.setAmount(c.getString(c.getColumnIndex("amount")));
                listModel.setBooking_code(c.getString(c.getColumnIndex("booking_code")));

                listModel.setConfirmation_guarantee(c.getString(c.getColumnIndex("confirmation_guarantee")));
                listModel.setCurrency(c.getString(c.getColumnIndex("currency")));
                listModel.setDestination(c.getString(c.getColumnIndex("destination")));

                listModel.setDetailed_status(c.getString(c.getColumnIndex("detailed_status")));
                listModel.setExecutive_name(c.getString(c.getColumnIndex("executive_name")));
                listModel.setMarkup(c.getString(c.getColumnIndex("markup")));

                listModel.setStatus(c.getString(c.getColumnIndex("status")));
                listModel.setSupplier(c.getString(c.getColumnIndex("supplier")));
                listModel.setTravel_agent(c.getString(c.getColumnIndex("travel_agent")));
                listModel.setTravel_agent_phone(c.getString(c.getColumnIndex("travel_agent_phone")));

                result.add(listModel);
                c.moveToNext();
            }

            return result;
        }catch (Exception e1){
            return null;
        }


    }

    public boolean deleteAllQueryData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e(TAG, "Remove Query table ");
        return db.delete("querys", null, null) > 0;
    }


}
