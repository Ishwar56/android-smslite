package com.guiddoo.smslite.datamodel;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoo.smslite.R;
import com.guiddoo.smslite.application.excursion;
import com.guiddoo.smslite.utils.CustomResponseDialog;
import com.guiddoo.smslite.utils.SessionManager;
import com.guiddoo.smslite.utils.TaskNotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class JsonParser {

    static CustomResponseDialog pDialog;
    static Context context;
    String TAG_SUCCESS = "JSON_PARSER_SUCCESS";
    String TAG_ERROR = "JSON_PARSER_ERROR";
    Map<String, String> params;
    JSONObject paramsJSONObject;
    String body = null;
    Activity activity;
    private String TAG = "JSON Parser";
    private SessionManager sessionManager;


    public static void showDialogue(Context context) {
        pDialog = new CustomResponseDialog(context);
        pDialog.showCustomDialog();
    }

    public static void dissmissDialogue() {
        pDialog.hideCustomeDialog();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void getJsonUsingStringRequest(final Context context1, final TaskNotifier notifier, JSONObject jsonObject, Map<String, String> params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            params = params1;
            paramsJSONObject = jsonObject;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            //String url = sessionManager.getBaseUrl() + action;
            String url = "http://wayrtoo.azurewebsites.net/wayrtooservice/Wayrtoo_Service.svc/products/activities/" + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "Header==>" + params + "Jsonobject==>" + paramsJSONObject + "params url==>" + url);

            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },

                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                //get status code here
                                Log.e(TAG_ERROR, "Error==>" + error + "");

                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                //get response body and parse with appropriate encoding
                                if (error.networkResponse.data != null) {
                                    try {
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");


                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                //do stuff with the body...
                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }
                    })

            {
                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }

                @Override
                public String getBodyContentType() {
                    return "application/x-www-form-urlencoded; charset=UTF-8";
                }

                /* (non-Javadoc)
                 * @see com.android.volley.toolbox.StringRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
                 */
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    // since we don't know which of the two underlying network vehicles
                    // will Volley use, we have to handle and store session cookies manually
                    return super.parseNetworkResponse(response);
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();

                    if (headers == null
                            || headers.equals(Collections.emptyMap())) {
                        headers = new HashMap<String, String>();
                       /* headers.put("Content-Type", "application/json");
                        headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");*/
                        headers = params;
                    }

                    return headers;
                }

            };

            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);

        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));

                //   notifier.onError(obj + "");
            } catch (JSONException e1) {

            }
        }
    }

    public void getWebViewStringRequest(final Context context1, final TaskNotifier notifier, Map<String, String> params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            params = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);

            String url = sessionManager.getBaseUrl() + "products/activities/details?tour_id=" + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "params==>" + params + "params url==>" + url);

            StringRequest strRequest = new StringRequest(Request.Method.GET, url,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e(TAG_SUCCESS, response + "");

                            notifier.onSuccess(response);
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                //get status code here
                                Log.e(TAG_ERROR, "Error==>" + error + "");

                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                //get response body and parse with appropriate encoding
                                if (error.networkResponse.data != null) {
                                    try {
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                //do stuff with the body...
                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }
                    })

            {
                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }

                @Override
                public String getBodyContentType() {
                    return "application/x-www-form-urlencoded; charset=UTF-8";
                }

                /* (non-Javadoc)
                 * @see com.android.volley.toolbox.StringRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
                 */
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    // since we don't know which of the two underlying network vehicles
                    // will Volley use, we have to handle and store session cookies manually

                    return super.parseNetworkResponse(response);
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();

                    if (headers == null
                            || headers.equals(Collections.emptyMap())) {
                        headers = new HashMap<String, String>();
                       /* headers.put("Content-Type", "application/json");
                        headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");*/
                        headers = params;
                    }

                    return headers;
                }

            };

            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);

        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));

                //   notifier.onError(obj + "");
            } catch (JSONException e1) {

            }
        }
    }

    public void getJsonUsingJSONObjectRequest(final Context context1, final TaskNotifier notifier, JSONObject params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            paramsJSONObject = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            String url = sessionManager.getBaseUrl() + "products/activities/" + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "JsonObject==>" + paramsJSONObject + "params url==>" + url);
            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {


                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                dissmissDialogue();
                                Log.e(TAG_ERROR, "Error==>" + error + "");
                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                if (error.networkResponse.data != null) {
                                    try {
                                        dissmissDialogue();
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }

                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("X-Authorization",sessionManager.getHashkey());
                    return headers;
                }
            };
            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));
            } catch (JSONException e1) {

            }
        }
    }

    public void getStatusRequest(final Context context1, final TaskNotifier notifier, JSONObject params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            paramsJSONObject = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            String url = sessionManager.getBaseUrl() + "products/" + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "JsonObject==>" + paramsJSONObject + "params url==>" + url);
            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                dissmissDialogue();
                                Log.e(TAG_ERROR, "Error==>" + error + "");
                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                if (error.networkResponse.data != null) {
                                    try {
                                        dissmissDialogue();
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Something went wrong, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Something went wrong, Please try after sometime");
                                dissmissDialogue();
                            }
                        }

                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("X-Authorization",sessionManager.getHashkey());
                    return headers;
                }
            };
            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));
            } catch (JSONException e1) {
                dissmissDialogue();
            }
        }
    }

    public void getStatusRequests(final Context context1, final TaskNotifier notifier, JSONObject params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            paramsJSONObject = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            String url = sessionManager.getBaseUrl() + "" + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "JsonObject==>" + paramsJSONObject + "params url==>" + url);
            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                dissmissDialogue();
                                Log.e(TAG_ERROR, "Error==>" + error + "");
                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                if (error.networkResponse.data != null) {
                                    try {
                                        dissmissDialogue();
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }

                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("X-Authorization",sessionManager.getHashkey());
                    return headers;
                }
            };
            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));
            } catch (JSONException e1) {
                dissmissDialogue();
            }
        }
    }

    public void getJsonEditTripName(final Context context1, final TaskNotifier notifier, JSONObject params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            paramsJSONObject = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            String url = sessionManager.getBaseUrl() + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "JsonObject==>" + paramsJSONObject + "params url==>" + url);
            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                Log.e(TAG_ERROR, "Error==>" + error + "");
                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                if (error.networkResponse.data != null) {
                                    try {
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }

                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("hash_key", sessionManager.getHashkey());
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));
            } catch (JSONException e1) {

            }
        }
    }


    public void getJsonLoginRequest(final Context context1, final TaskNotifier notifier, JSONObject params1, String action) {
        sessionManager = new SessionManager(context1);
        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            paramsJSONObject = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            String url = sessionManager.getBaseUrl() + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "JsonObject==>" + paramsJSONObject + "params url==>" + url);
            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                Log.e(TAG_ERROR, "Error==>" + error + "");
                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                if (error.networkResponse.data != null) {
                                    try {
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }

                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("X-Authorization",sessionManager.getHashkey());
                    return headers;
                }
            };
            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));
            } catch (JSONException e1) {

            }
        }
    }


    public void getJsonLoginRequests(final Context context1, final TaskNotifier notifier, JSONObject params1, String action) {

        if (sessionManager.isNetworkAvailable()) {
            context = context1;
            paramsJSONObject = params1;
            sessionManager = new SessionManager(context);
            showDialogue(context);
            String url = sessionManager.getBaseUrl() + action;
            trustAllCertificates();

            Log.e("Jason Parser Url", "JsonObject==>" + paramsJSONObject + "params url==>" + url);
            JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG_SUCCESS, response + "");
                            notifier.onSuccess(response.toString());
                            dissmissDialogue();
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                Log.e(TAG_ERROR, "Error==>" + error + "");
                                String statusCode = String.valueOf(error.networkResponse.statusCode);
                                if (error.networkResponse.data != null) {
                                    try {
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        Log.e(TAG_ERROR, body + "");

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                        dissmissDialogue();
                                    }
                                }

                                dissmissDialogue();
                                notifier.onError(body + "");
                                // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                                dissmissDialogue();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                JsonParser.showToast(context, "Failed to connect server, Please try after sometime");
                                dissmissDialogue();
                            }
                        }

                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    // headers.put("X-Authorization","0D1067102F935B4CC31E082BD45014D469E35268");
                    return headers;
                }
            };
            strRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20*3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.add(strRequest);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj.put("developerMessage", context1.getResources().getString(R.string.internet_conn));
                showToast(context1, context1.getString(R.string.internet_conn));
            } catch (JSONException e1) {

            }
        }
    }

    public void getJsonUsingStringRequestUsingGET(Context context1,
                                                  final TaskNotifier notifier, String action) {
        context = context1;
        showDialogue(context);
        //queue = Volley.newRequestQueue(context);
        //String url = sessionManager.getBaseUrl() + action;
        String url = action;
        trustAllCertificates();


        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG_SUCCESS, response + "");

                        notifier.onSuccess(response);
                        dissmissDialogue();

                    }
                },
                new Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            //get status code here
                            String statusCode = String.valueOf(error.networkResponse.statusCode);
                            //get response body and parse with appropriate encoding
                            if (error.networkResponse.data != null) {
                                try {
                                    body = new String(error.networkResponse.data, "UTF-8");
                                    Log.e(TAG_ERROR, body + "");

                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }

                            //do stuff with the body...
                            notifier.onError(body + "");
                            // Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                            dissmissDialogue();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            dissmissDialogue();

                        }
                    }


                })

        {
            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /* (non-Javadoc)
             * @see com.android.volley.toolbox.StringRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
             */
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                // since we don't know which of the two underlying network vehicles
                // will Volley use, we have to handle and store session cookies manually

                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = super.getHeaders();

                if (headers == null
                        || headers.equals(Collections.emptyMap())) {
                    headers = new HashMap<String, String>();
                }

                return headers;
            }


        };
        strRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        excursion.getInstance().getRequestQueue().add(strRequest);

    }

    private void trustAllCertificates() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception e) {
        }
    }


}