package com.guiddoo.smslite.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityViewSingleMapDirection;
import com.guiddoo.smslite.fragment.FragmentMeetingFollowUp;
import com.guiddoo.smslite.model.FollowUPListModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.guiddoo.smslite.fragment.FragmentMeeting.sizeofFollowupList;


/**
 * Created by Ishwar on 26/12/2017.
 */

public class AdapterFollowUPList extends RecyclerView.Adapter<AdapterFollowUPList.MyViewHolder> implements Filterable {
    private List<FollowUPListModel> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private ArrayList<FollowUPListModel> arraylist;

    public AdapterFollowUPList(Context mContext, List<FollowUPListModel> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
        this.arraylist = new ArrayList<FollowUPListModel>();
        this.arraylist.addAll(liabraryList);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                liabraryList = new ArrayList<>();
                String charString = charText.toString();

                if (charString.isEmpty()) {
                    liabraryList = arraylist;
                } else {
                    for (FollowUPListModel wp : arraylist) {
                        if (wp.getPhone().toLowerCase(Locale.getDefault()).contains(charText)||wp.getFollow_up().toLowerCase(Locale.getDefault()).contains(charText)
                                || wp.getCompany_name().toLowerCase(Locale.getDefault()).contains(charText)|| wp.getContact_person().toLowerCase(Locale.getDefault()).contains(charText) || wp.getRemarks().toLowerCase(Locale.getDefault()).contains(charText)) {
                            liabraryList.add(wp);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = liabraryList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                liabraryList = (ArrayList<FollowUPListModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_company_name,tv_contact_person,tv_contact_call,tv_call_status,tv_map;
        private ImageView tv_patner_info_edit,iv_call;
        private LinearLayout ll_call;
        public MyViewHolder(View view) {
            super(view);

            tv_map = (TextView) view.findViewById(R.id.tv_map);
            iv_call = (ImageView)view.findViewById(R.id.iv_call);
            tv_contact_call = (TextView)view.findViewById(R.id.tv_contact_call);
            tv_contact_person = (TextView)view.findViewById(R.id.tv_contact_person);
            tv_patner_info_edit = (ImageView) view.findViewById(R.id.tv_patner_info_edit);
            ll_call = (LinearLayout) view.findViewById(R.id.ll_call);
            tv_company_name = (TextView)view.findViewById(R.id.tv_company_name);
            tv_call_status = (TextView)view.findViewById(R.id.tv_call_status);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_follow_up_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_company_name.setText(liabraryList.get(position).getCompany_name());
        holder.tv_contact_person.setText(liabraryList.get(position).getContact_person());

        try{
            if(liabraryList.get(position).getPhone().equalsIgnoreCase("")|| liabraryList.get(position).getPhone()==null){
                holder.ll_call.setVisibility(View.GONE);
            }else if(liabraryList.get(position).getPhone().length()>15){
                holder.ll_call.setVisibility(View.VISIBLE);
                holder.tv_contact_call.setText(liabraryList.get(position).getPhone().split(",")[0]);
            }else{
                holder.ll_call.setVisibility(View.VISIBLE);
                holder.tv_contact_call.setText(liabraryList.get(position).getPhone());
            }
        }catch (NullPointerException|SecurityException e){
            holder.ll_call.setVisibility(View.VISIBLE);
            holder.tv_contact_call.setText(liabraryList.get(position).getPhone());
        }

        if(liabraryList.get(position).getFollow_up().equalsIgnoreCase("Followup Tomorrow")){
            holder.tv_call_status.setTextColor(Color.parseColor("#87BCBF"));
            holder.tv_call_status.setText(liabraryList.get(position).getFollow_up());
        }else  if(liabraryList.get(position).getFollow_up().equalsIgnoreCase("Followup Today")){
            holder.tv_call_status.setText(liabraryList.get(position).getFollow_up());
            holder.tv_call_status.setTextColor(Color.parseColor("#D97D54"));
        }else {
            holder.tv_call_status.setText(liabraryList.get(position).getFollow_up());
            holder.tv_call_status.setTextColor(Color.parseColor("#6E8CA0"));
        }

        holder.tv_patner_info_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentMeetingFollowUp bookReview = new FragmentMeetingFollowUp();
                Bundle bundle=new Bundle();
                bundle.putString("ID", liabraryList.get(position).getTravel_Agent_id());
                bundle.putString("Name", liabraryList.get(position).getContact_person());
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview,true);
            }
        });

        holder.tv_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ActivityViewSingleMapDirection.class);
                intent.putExtra("Lat",liabraryList.get(position).getLatitude());
                intent.putExtra("Log",liabraryList.get(position).getLongitude());
                activity.startActivity(intent);

            }
        });

        holder.ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(liabraryList.get(position).getPhone().length()>6){
                    final Dialog dialogMsg = new Dialog(activity);
                    dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogMsg.setContentView(R.layout.login_dialog);
                    dialogMsg.setCancelable(false);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialogMsg.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogMsg.getWindow().setAttributes(lp);
                    dialogMsg.show();

                    TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
                    TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_text);
                    TextView tv_title = (TextView) dialogMsg.findViewById(R.id.tv_title);
                    TextView tv_sms = (TextView) dialogMsg.findViewById(R.id.tv_msg);
                    TextView tv_no = (TextView)dialogMsg.findViewById(R.id.tv_no);
                    tv_no.setText(liabraryList.get(position).getPhone());

                    cardViewCancel.setText(mContext.getText(R.string.btn_cancel));
                    btn_text.setText(mContext.getText(R.string.call));
                    tv_title.setText(mContext.getText(R.string.contact_us));
                    tv_sms.setText(mContext.getText(R.string.call_worning));
                    //tv_sms.setVisibility(View.GONE);
                    btn_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialogMsg.cancel();
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + liabraryList.get(position).getPhone()));
                            activity.startActivity(callIntent);

                        }
                    });

                    cardViewCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.cancel();
                        }
                    });

                }



            }
        });


    }

    @Override
    public int getItemCount() {
        try {
            sizeofFollowupList = liabraryList.size();
        } catch (Exception e) {
            sizeofFollowupList = 0;
        }

        try{
            return liabraryList.size();
        }catch (NullPointerException e){
            return 0;
        }


    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}