package com.guiddoo.smslite.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.model.ModelDestinationsList;
import com.guiddoo.smslite.model.ModelExecutivesList;

import java.util.List;

public class AdapterExecutive extends ArrayAdapter<ModelExecutivesList> {

        LayoutInflater flater;

        public AdapterExecutive(Activity context, int resouceId, int textviewId, List<ModelExecutivesList> list){

            super(context,resouceId,textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ModelExecutivesList rowItem = getItem(position);

            View rowview = flater.inflate(R.layout.spiner_area_list,null,true);

            TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
            txtTitle.setText(rowItem.getUser_name());

            return rowview;
        }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spiner_area_list,parent, false);
        }
        ModelExecutivesList rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(rowItem.getUser_name());
        return convertView;
    }
    }

