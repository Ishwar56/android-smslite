package com.guiddoo.smslite.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityHomeScreen;
import com.guiddoo.smslite.fragment.FragmentQueries;
import com.guiddoo.smslite.fragment.FragmentQueriesFollowUpList;
import com.guiddoo.smslite.fragment.FragmentQueriesLogsList;
import com.guiddoo.smslite.model.QueryListModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.guiddoo.smslite.fragment.FragmentQueries.sizeofList;


/**
 * Created by Ishwar on 26/12/2017.
 */

public class AdapterQueryList extends RecyclerView.Adapter<AdapterQueryList.MyViewHolder> implements Filterable {
    private List<QueryListModel> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1,listSize=0;
    private FragmentManager fragmentManager;
    private FragmentQueries fragmentQueries;
    private ArrayList<QueryListModel> arraylist;


    public AdapterQueryList(Context mContext, List<QueryListModel> liabraryList, Activity activity, FragmentManager fragmentManager,FragmentQueries fragment) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
        this.arraylist = new ArrayList<QueryListModel>();
        this.arraylist.addAll(liabraryList);
        this.fragmentQueries = fragment;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_compony_name,tv_query_city,tv_query_code,tv_query_amt,tv_status_details,tv_agent_call,tv_status,tv_exicutive_name;
        private ImageView iv_image,iv_call;
        private LinearLayout tv_query_follow_up,tv_query_logs;
        public MyViewHolder(View view) {
            super(view);


            iv_call = (ImageView)view.findViewById(R.id.iv_call);

            tv_compony_name = (TextView)view.findViewById(R.id.tv_compony_name);
            tv_query_code = (TextView)view.findViewById(R.id.tv_query_code);
            tv_query_amt = (TextView)view.findViewById(R.id.tv_query_amt);
            tv_status = (TextView)view.findViewById(R.id.tv_status);
            tv_exicutive_name = (TextView)view.findViewById(R.id.tv_exicutive_name);
            tv_query_follow_up = (LinearLayout)view.findViewById(R.id.tv_query_follow_up);
            tv_query_logs = (LinearLayout)view.findViewById(R.id.tv_query_logs);
            tv_query_city = (TextView)view.findViewById(R.id.tv_query_city);
            tv_agent_call= (TextView)view.findViewById(R.id.tv_agent_call);
            tv_status_details= (TextView)view.findViewById(R.id.tv_status_details);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_query_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_query_code.setText(liabraryList.get(position).getBooking_code());
        holder.tv_query_amt.setText(liabraryList.get(position).getCurrency()+" "+liabraryList.get(position).getAmount());

        holder.tv_query_city.setText(liabraryList.get(position).getDestination());
        holder.tv_compony_name.setText(liabraryList.get(position).getTravel_agent());

        if(liabraryList.get(position).getStatus().equalsIgnoreCase("Open")){
            holder.tv_status.setTextColor(Color.parseColor("#00A86B"));
        }else{
            holder.tv_status.setTextColor(Color.parseColor("#DC2300"));
        }
        holder.tv_status.setText(liabraryList.get(position).getStatus());

        holder.tv_exicutive_name.setText(liabraryList.get(position).getExecutive_name());

        holder.tv_status_details.setText(liabraryList.get(position).getDetailed_status());

        try{
            if(liabraryList.get(position).getTravel_agent_phone().equalsIgnoreCase("")|| liabraryList.get(position).getTravel_agent_phone()==null){
                holder.tv_agent_call.setVisibility(View.GONE);
                holder.iv_call.setVisibility(View.GONE);
            }else if(liabraryList.get(position).getTravel_agent_phone().length()>15){
                holder.tv_agent_call.setVisibility(View.VISIBLE);
                holder.iv_call.setVisibility(View.VISIBLE);
                holder.tv_agent_call.setText(liabraryList.get(position).getTravel_agent_phone().split(",")[0]);
            }else{
                holder.tv_agent_call.setVisibility(View.VISIBLE);
                holder.iv_call.setVisibility(View.VISIBLE);
                holder.tv_agent_call.setText(liabraryList.get(position).getTravel_agent_phone());
            }
        }catch (SecurityException|NullPointerException e){
            holder.tv_agent_call.setText(liabraryList.get(position).getTravel_agent_phone());
        }


        holder.tv_query_follow_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentQueriesFollowUpList bookReview = new FragmentQueriesFollowUpList();
                Bundle bundle=new Bundle();
                bundle.putString("ID", liabraryList.get(position).getQuery_id());
                bundle.putString("Booking_Code", liabraryList.get(position).getBooking_code());
                bundle.putString("Executive_Name", liabraryList.get(position).getExecutive_name());
                bundle.putString("Supplier_Name", liabraryList.get(position).getSupplier());
                bundle.putString("Status", liabraryList.get(position).getStatus());
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview,true);

                try{
                    ((ActivityHomeScreen)activity).closeSubMenusFab();
                }
                catch (Exception e){

                }

            }
        });

        holder.tv_query_logs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentQueriesLogsList bookReview = new FragmentQueriesLogsList();
                Bundle bundle=new Bundle();
                bundle.putString("ID", liabraryList.get(position).getQuery_id());
                // bundle.putString("Book_Name", liabraryList.get(position).getBookName());
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview,true);//
                try{
                    ((ActivityHomeScreen)activity).closeSubMenusFab();
                }
                catch (Exception e){

                }
            }
        });

        holder.tv_agent_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(liabraryList.get(position).getTravel_agent_phone().length()>6){
                    final Dialog dialogMsg = new Dialog(activity);
                    dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogMsg.setContentView(R.layout.login_dialog);
                    dialogMsg.setCancelable(false);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialogMsg.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogMsg.getWindow().setAttributes(lp);
                    dialogMsg.show();

                    TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
                    TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_text);
                    TextView tv_title = (TextView) dialogMsg.findViewById(R.id.tv_title);
                    TextView tv_sms = (TextView) dialogMsg.findViewById(R.id.tv_msg);
                    TextView tv_no = (TextView)dialogMsg.findViewById(R.id.tv_no);
                    tv_no.setText(liabraryList.get(position).getTravel_agent_phone());
                    cardViewCancel.setText(mContext.getText(R.string.btn_cancel));
                    btn_text.setText(mContext.getText(R.string.call));
                    tv_title.setText(mContext.getText(R.string.contact_us));
                    tv_sms.setText(mContext.getText(R.string.call_worning));
                   // tv_sms.setVisibility(View.GONE);
                    btn_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialogMsg.cancel();
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + liabraryList.get(position).getTravel_agent_phone()));
                            activity.startActivity(callIntent);
                        }
                    });

                    cardViewCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.cancel();
                        }
                    });
                }

            }
        });


    }




    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                liabraryList = new ArrayList<>();
                String charString = charText.toString();

                if (charString.isEmpty()) {
                    liabraryList = arraylist;
                   // sizeofList=liabraryList.size();
                    //setListCount(liabraryList.size());
                } else {
                    for (QueryListModel wp : arraylist) {
                        if (wp.getExecutive_name().toLowerCase(Locale.getDefault()).contains(charText)||wp.getBooking_code().toLowerCase(Locale.getDefault()).contains(charText)
                        || wp.getStatus().toLowerCase(Locale.getDefault()).contains(charText) || wp.getTravel_agent().toLowerCase(Locale.getDefault()).contains(charText)
                        ||wp.getAmount().toLowerCase(Locale.getDefault()).contains(charText) || wp.getDestination().toLowerCase(Locale.getDefault()).contains(charText)
                                ||wp.getSupplier().toLowerCase(Locale.getDefault()).contains(charText) || wp.getDetailed_status().toLowerCase(Locale.getDefault()).contains(charText)) {
                            liabraryList.add(wp);
                        }
                    }
                    try{
                        //listSize=liabraryList.size();
                       // sizeofList=liabraryList.size();
                        //setListCount(liabraryList.size());
                       // FragmentQueries queries = new FragmentQueries();
                       // queries.setListCountss(listSize);
                    }catch (NullPointerException e){

                    }


                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = liabraryList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                liabraryList = (ArrayList<QueryListModel>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }

    public void setListCount(int size){
        try{
            ((FragmentQueries) fragmentQueries).setListCounts(size);
        }catch (NullPointerException e){
            fragmentQueries.setListCounts(size);
        }

    }


    @Override
    public int getItemCount() {
        /*try {
            if (num * 5 > liabraryList.size()) {
                return liabraryList.size();
            } else {
                return num * 5;
            }

        } catch (Exception e) {
            return 0;
        }*/
        sizeofList=liabraryList.size();
        try{
            return liabraryList.size();
        }catch (NullPointerException e){
            return 0;
        }

    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}