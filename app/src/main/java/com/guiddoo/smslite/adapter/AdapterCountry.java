package com.guiddoo.smslite.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.model.AreaModel;
import com.guiddoo.smslite.model.ModelCountriesList;
import com.guiddoo.smslite.model.ModelDestinationsList;

import java.util.ArrayList;
import java.util.List;

public class AdapterCountry extends ArrayAdapter<ModelCountriesList> {

    LayoutInflater flater;

    public AdapterCountry(Activity context, int resouceId, int textviewId, List<ModelCountriesList> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ModelCountriesList rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.spiner_area_list,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
        txtTitle.setText(rowItem.getCountry_name());

        return rowview;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spiner_area_list,parent, false);
        }
        ModelCountriesList rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(rowItem.getCountry_name());
        return convertView;
    }
}
