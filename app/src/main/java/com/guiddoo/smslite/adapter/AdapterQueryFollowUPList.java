package com.guiddoo.smslite.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.fragment.FragmentQueriesFollowUpList;
import com.guiddoo.smslite.fragment.FragmentQueryFollowUp;
import com.guiddoo.smslite.model.FollowUPListModel;
import com.guiddoo.smslite.model.QueryFollowupListModel;

import java.util.List;


/**
 * Created by Ishwar on 26/12/2017.
 */

public class AdapterQueryFollowUPList extends RecyclerView.Adapter<AdapterQueryFollowUPList.MyViewHolder> {
    private List<QueryFollowupListModel> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterQueryFollowUPList(Context mContext, List<QueryFollowupListModel> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_user_name,tv_date,tv_query_amt,tv_query_type,tv_query_follow_up,tv_query_logs;
        private LinearLayout ll_click;
        public MyViewHolder(View view) {
            super(view);

            ll_click = (LinearLayout) view.findViewById(R.id.ll_click);
            tv_user_name = (TextView)view.findViewById(R.id.tv_user_name);
            tv_date = (TextView)view.findViewById(R.id.tv_date);
            tv_query_amt = (TextView)view.findViewById(R.id.tv_query_amt);
            tv_query_type = (TextView)view.findViewById(R.id.tv_query_type);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_query_follow_up_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_user_name.setText(liabraryList.get(position).getExecutive_name());
        holder.tv_date.setText(liabraryList.get(position).getDate());
        holder.tv_query_type.setText(liabraryList.get(position).getType());
        holder.tv_query_amt.setText(liabraryList.get(position).getSales_amount());

       /* holder.ll_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentQueryFollowUp bookReview = new FragmentQueryFollowUp();
                Bundle bundle=new Bundle();
                //bundle.putString("Book_ID", liabraryList.get(position).getBookId());
               // bundle.putString("Book_Name", liabraryList.get(position).getBookName());
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview,true);
            }
        });*/


    }

    @Override
    public int getItemCount() {
        /*try {
            if (num * 5 > liabraryList.size()) {
                return liabraryList.size();
            } else {
                return num * 5;
            }

        } catch (Exception e) {
            return 0;
        }*/
        return liabraryList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}