package com.guiddoo.smslite.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.activity.ActivityViewSingleMapDirection;
import com.guiddoo.smslite.fragment.FragmentAddTravelAgent;
import com.guiddoo.smslite.fragment.FragmentMeetingFollowUp;
import com.guiddoo.smslite.model.ModelPartnerTravelAgentList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.guiddoo.smslite.fragment.FragmentMeeting.sizeofFollowupList;
import static com.guiddoo.smslite.fragment.FragmentMeeting.sizeofPartnerupList;


/**
 * Created by Ishwar on 26/12/2017.
 */

public class AdapterYourPartnerList extends RecyclerView.Adapter<AdapterYourPartnerList.MyViewHolder> implements Filterable {
    private List<ModelPartnerTravelAgentList> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private ArrayList<ModelPartnerTravelAgentList> arraylist;

    public AdapterYourPartnerList(Context mContext, List<ModelPartnerTravelAgentList> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
        this.arraylist = new ArrayList<ModelPartnerTravelAgentList>();
        this.arraylist.addAll(liabraryList);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                liabraryList = new ArrayList<>();
                String charString = charText.toString();

                if (charString.isEmpty()) {
                    liabraryList = arraylist;
                } else {
                    for (ModelPartnerTravelAgentList wp : arraylist) {
                        if (wp.getCall_status().toLowerCase(Locale.getDefault()).contains(charText)||wp.getPhone().toLowerCase(Locale.getDefault()).contains(charText)||wp.getFollow_up().toLowerCase(Locale.getDefault()).contains(charText)
                                || wp.getAddress().toLowerCase(Locale.getDefault()).contains(charText) || wp.getLocation().toLowerCase(Locale.getDefault()).contains(charText)|| wp.getCompany_name().toLowerCase(Locale.getDefault()).contains(charText) || wp.getRemarks().toLowerCase(Locale.getDefault()).contains(charText)) {
                            liabraryList.add(wp);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = liabraryList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                liabraryList = (ArrayList<ModelPartnerTravelAgentList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name,tv_contact_call,tv_company_name,tv_location,tv_call_status,tv_patner_info,tv_patner_info_edit,tv_map;
        private ImageView iv_call;
        private LinearLayout ll_call,ll_information,ll_edit,ll_call_status;
        public MyViewHolder(View view) {
            super(view);

            tv_map = (TextView) view.findViewById(R.id.tv_map);
            ll_call = (LinearLayout) view.findViewById(R.id.ll_call);
            ll_information = (LinearLayout) view.findViewById(R.id.ll_information);
            ll_edit = (LinearLayout) view.findViewById(R.id.ll_edit);
            iv_call = (ImageView)view.findViewById(R.id.iv_call);
            tv_patner_info = (TextView) view.findViewById(R.id.tv_patner_info);
            tv_patner_info_edit = (TextView) view.findViewById(R.id.tv_patner_info_edit);
            tv_contact_call = (TextView)view.findViewById(R.id.tv_contact_call);
            tv_company_name = (TextView)view.findViewById(R.id.tv_company_name);
            tv_location = (TextView)view.findViewById(R.id.tv_location);
            tv_call_status = (TextView)view.findViewById(R.id.tv_call_status);
            ll_call_status = (LinearLayout) view.findViewById(R.id.ll_call_status);

            /*click = (LinearLayout) view.findViewById(R.id.ll_content_click);
            tv_name = (TextView)view.findViewById(R.id.book_name);
            */

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_your_patner_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_company_name.setText(liabraryList.get(position).getCompany_name());


        try {
            if(liabraryList.get(position).getAddress().equalsIgnoreCase("")|| liabraryList.get(position).getAddress()==null){
                holder.tv_location.setVisibility(View.GONE);
            }else{
                holder.tv_location.setText(liabraryList.get(position).getAddress());
                holder.tv_location.setVisibility(View.VISIBLE);
            }
        }catch (SecurityException|NullPointerException e){
            holder.tv_location.setText(liabraryList.get(position).getAddress());
        }

        try{
            if(liabraryList.get(position).getPhone().equalsIgnoreCase("")|| liabraryList.get(position).getPhone()==null){
                holder.ll_call.setVisibility(View.GONE);
            }else if(liabraryList.get(position).getPhone().length()>15){
                holder.ll_call.setVisibility(View.VISIBLE);
                holder.tv_contact_call.setText(liabraryList.get(position).getPhone().split(",")[0]);
            }else{
                holder.ll_call.setVisibility(View.VISIBLE);
                holder.tv_contact_call.setText(liabraryList.get(position).getPhone());
            }
        }catch (SecurityException|NullPointerException e){
            holder.tv_contact_call.setText(liabraryList.get(position).getPhone());
        }

        if(liabraryList.get(position).getCall_status().equalsIgnoreCase("")|| liabraryList.get(position).getCall_status()==null){
            holder.ll_call_status.setVisibility(View.GONE);
        }else if(liabraryList.get(position).getCall_status().equalsIgnoreCase("Cold")){
            holder.ll_call_status.setVisibility(View.VISIBLE);
            holder.tv_call_status.setTextColor(Color.parseColor("#00A86B"));
            holder.tv_call_status.setText(liabraryList.get(position).getCall_status());
        }else  if(liabraryList.get(position).getCall_status().equalsIgnoreCase("Hot")){
            holder.ll_call_status.setVisibility(View.VISIBLE);
            holder.tv_call_status.setText(liabraryList.get(position).getCall_status());
            holder.tv_call_status.setTextColor(Color.parseColor("#CC0200"));
        }else {
            holder.ll_call_status.setVisibility(View.VISIBLE);
            holder.tv_call_status.setText(liabraryList.get(position).getCall_status());
            holder.tv_call_status.setTextColor(Color.parseColor("#39A3E8"));
        }


        holder.tv_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ActivityViewSingleMapDirection.class);
                intent.putExtra("Lat",liabraryList.get(position).getLatitude());
                intent.putExtra("Log",liabraryList.get(position).getLongitude());
                activity.startActivity(intent);
            }
        });

        holder.ll_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentAddTravelAgent bookReview = new FragmentAddTravelAgent();
                Bundle bundle=new Bundle();
                bundle.putString("Flow", "Info");
                bundle.putString("Position", String.valueOf(position));
                bundle.putSerializable("arraylist", (Serializable) liabraryList);
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview,true);
            }
        });

        holder.ll_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentMeetingFollowUp bookReview = new FragmentMeetingFollowUp();
                Bundle bundle=new Bundle();
                bundle.putString("ID", liabraryList.get(position).getTravel_Agent_id());
                bundle.putString("Name", liabraryList.get(position).getContact_person());
                bookReview.setArguments(bundle);
                changeFragment(R.id.fragment_container, bookReview,true);
            }
        });

        holder.ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(liabraryList.get(position).getPhone().length()>6){
                    final Dialog dialogMsg = new Dialog(activity);
                    dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogMsg.setContentView(R.layout.login_dialog);
                    dialogMsg.setCancelable(false);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialogMsg.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogMsg.getWindow().setAttributes(lp);
                    dialogMsg.show();

                    TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
                    TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_text);
                    TextView tv_title = (TextView) dialogMsg.findViewById(R.id.tv_title);
                    TextView tv_sms = (TextView) dialogMsg.findViewById(R.id.tv_msg);
                    TextView tv_no = (TextView)dialogMsg.findViewById(R.id.tv_no);
                    tv_no.setText(liabraryList.get(position).getPhone());
                    cardViewCancel.setText(mContext.getText(R.string.btn_cancel));
                    btn_text.setText(mContext.getText(R.string.call));
                    tv_title.setText(mContext.getText(R.string.contact_us));
                    tv_sms.setText(mContext.getText(R.string.call_worning));

                    btn_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialogMsg.cancel();
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + liabraryList.get(position).getPhone()));
                            activity.startActivity(callIntent);

                        }
                    });

                    cardViewCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.cancel();
                        }
                    });
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        try {
            sizeofPartnerupList = liabraryList.size();
        } catch (Exception e) {
            sizeofPartnerupList = 0;
        }

        try{
            return liabraryList.size();
        }catch (NullPointerException e){
            return 0;
        }
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}