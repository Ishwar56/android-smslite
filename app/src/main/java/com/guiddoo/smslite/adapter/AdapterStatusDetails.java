package com.guiddoo.smslite.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.model.ModelOCStatusList;
import com.guiddoo.smslite.model.ModelStatusList;

import java.util.List;

public class AdapterStatusDetails extends ArrayAdapter<ModelOCStatusList> {

        LayoutInflater flater;

        public AdapterStatusDetails(Activity context, int resouceId, int textviewId, List<ModelOCStatusList> list){

            super(context,resouceId,textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ModelOCStatusList rowItem = getItem(position);

            View rowview = flater.inflate(R.layout.spiner_area_list,null,true);

            TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
            txtTitle.setText(rowItem.getDetailed_status());

            return rowview;
        }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spiner_area_list,parent, false);
        }
        ModelOCStatusList rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(rowItem.getDetailed_status());
        return convertView;
    }
    }

