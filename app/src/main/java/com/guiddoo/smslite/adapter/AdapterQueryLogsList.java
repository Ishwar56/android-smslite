package com.guiddoo.smslite.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guiddoo.smslite.R;
import com.guiddoo.smslite.fragment.FragmentQueryFollowUp;
import com.guiddoo.smslite.model.FollowUPListModel;
import com.guiddoo.smslite.model.QueryLogsListModel;

import java.util.List;


/**
 * Created by Ishwar on 26/12/2017.
 */

public class AdapterQueryLogsList extends RecyclerView.Adapter<AdapterQueryLogsList.MyViewHolder> {
    private List<QueryLogsListModel> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterQueryLogsList(Context mContext, List<QueryLogsListModel> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_logs_title,tv_created_by;
        private LinearLayout ll_click;
        public MyViewHolder(View view) {
            super(view);

            ll_click = (LinearLayout) view.findViewById(R.id.ll_click);
            tv_logs_title = (TextView)view.findViewById(R.id.tv_logs_title);
            tv_created_by = (TextView)view.findViewById(R.id.tv_created_by);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_query_logs_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_logs_title.setText(liabraryList.get(position).getLog_title());
        holder.tv_created_by.setText("Created by : "+liabraryList.get(position).getCreated_by());



    }

    @Override
    public int getItemCount() {

        return liabraryList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}